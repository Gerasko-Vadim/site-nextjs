import { useRouter } from 'next/router'
import React from "react"
import { DocItem } from '../../components'
import { files, filesKgList } from '../../files/files'
import { kg } from '../../locales/kg'
import { ru } from '../../locales/ru'
import styles from "./docs.module.css"

export const DocsWrapper = (): JSX.Element => {
    const router = useRouter();
    const t = router.locale === 'ru' ? ru : kg;

    return (
        <div className={styles.wrapper}>

            {
                router.locale == "ru" ?
                    files.map((item, index) => <DocItem key={index} pathname={item.path} name={item.name} />)
                    :
                    filesKgList.map((item, index) => <DocItem key={index} pathname={item.path} name={item.name} />)
            }

        </div>

    )
}