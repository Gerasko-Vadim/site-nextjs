import classNames from 'classnames'
import React from "react"
import { Container } from '../../components/Container/Container'
import GoogleSvg from "./google.svg"
import AppStoreSvg from "./appStore.svg"
import InstagramSvg from "./instagram.svg"
import InstagramSvg2 from "./instagram.svg"
import TelegramSvg from "./telegram.svg"
import FBSvg from "./fb.svg"
import OkSvg from "./ok.svg"
import TwitSvg from "./twit.svg"
import VkSvg from "./vk.svg"
import YouTubeSvg from "./youtube.svg"
import TiktokSvg from "./tiktok.svg"
import WpSvg from "./wp.svg"
import TgSvg from "./tg.svg"
import styles from "./Footer.module.css"
import Link from 'next/link'

import { useRouter } from 'next/router'
import { ru } from '../../locales/ru'
import { kg } from '../../locales/kg'

// "footer-partner": {
//     "column1": {
//         "title": "Общая информация",
//             "1": "Как это работает"
//     },
//     "column2": {
//         "1": "Бизнесу выгодно",
//             "2": "Стать партнером"
//     },
//     "column3": {
//         "calls": "Для звонков по Кыргызстану",
//             "bottomText": "\"ОсОО «МКК «Келечек». Запись об учетной регистрации НБКР №510 от 06.08.2021\""
//     }
// },
// <Nav.Link className={styles.a} href="#trustUs">{t['header-partner'].link1}</Nav.Link>
// <Nav.Link className={styles.a} href="#HowItWork">{t['header-partner'].link2}</Nav.Link>
// <Nav.Link className={styles.a} href="#HelpBusiness">{t['header-partner'].link3}</Nav.Link>
// <Nav.Link className={styles.a} href="#BusinessProfitable">{t['header-partner'].link4}</Nav.Link>
// <Nav.Link className={styles.a} href="#ApplicationForm">{t['header-partner'].link5}</Nav.Link>


export const Footer = (): JSX.Element => {

    const router = useRouter();
    const { pathname } = router;
    const t = router.locale === 'ru' ? ru : kg;
    return (
        <div className={styles.footer}>
            <Container>
                <div className={styles.wrapper}>
                    <div className={styles.links}>
                        {
                            pathname == "/business" ?
                                <>
                                    <span>{t['footer-partner'].column1.title}</span>
                                    <a href="#HowItWork">{t['footer-partner'].column1.line1}</a>
                                </>
                                :
                                <>
                                    <span>{t['footer-client'].column1.title}</span>
                                    <a href="#about">{t['footer-client'].column1.line1}</a>
                                    <a href="#howToRepay">{t['footer-client'].column1.line2}</a>
                                    <Link href={`/${router.locale}/docs`}>
                                        {t['footer-client'].column1.line3}
                                    </Link>
                                </>
                        }


                    </div>
                    <div className={styles.links}>
                        <span>cash2u</span>
                        {
                            pathname == "/business" ?
                                <>
                                    <a href="#BusinessProfitable">{t['footer-partner'].column2.line1}</a>
                                    <a href="#ApplicationForm">{t['footer-partner'].column2.line2}</a>
                                </>
                                :
                                <>
                                    <a href="#questions">{t['footer-client'].column2.line1}</a>
                                    <a href="#howToRepay">{t['footer-client'].column2.line2}</a>
                                </>
                        }

                    </div>
                    <div className={styles.links}>
                        <span>Контакты</span>
                        <a>{t['footer-client'].column3.calls}</a>
                        <div className={styles.phones}>
                            <p>0772 55 33 33</p>
                            <p>0552 55 33 33</p>
                            <p>0502 55 33 33</p>
                        </div>

                    </div>
                    <div className={styles.downloadBlockWrapper}>
                        <div className={styles.downloadBlock}>
                            <a href="https://play.google.com/store/apps/details?id=kg.cash2u.client"
                            title="Google"
                                target="_blank">
                                <GoogleSvg />
                            </a>

                        </div>
                        <div className={styles.icons}>
                            <a target="_blank"
                            title="Facebook"
                                href="https://www.facebook.com/Cash2u-102189698862906/"
                            >
                                <FBSvg />
                            </a>
                            <a target="_blank"
                            title="Instagram"
                                href=" https://www.instagram.com/cash2u.kg/"
                            >
                                <InstagramSvg />
                            </a>
                            <a target="_blank"
                            title="Ok"
                                href="https://ok.ru/profile/582735885535"
                            >
                                <OkSvg />
                            </a>
                            <a target="_blank"
                                title="VK"
                                href="https://vk.com/cash2u"
                            >
                                <VkSvg />
                            </a>
                            {/* <TwitSvg /> */}
                            <a target="_blank"
                      
                            title="TikTok"
                                href="https://www.tiktok.com/@cash2u.kg"
                            >
                                <TiktokSvg />
                            </a>

                            <a target="_blank"
                            title="YouTube"
                                href="https://www.youtube.com/channel/UCYd4ljGJODrg1mCLNLl85aA"
                            >
                                <YouTubeSvg />
                            </a>
                            <a title='WhatsApp'
                                href="https://wa.me/+996552553333"
                            >
                                <WpSvg />
                            </a>
                            <a title='Telegram'
                                href="tg://resolve?domain=@KelechekMKKbot"
                            >
                                <TgSvg />
                            </a>
                        </div>
                    </div>
                </div>
                <div className={styles.icons2}>
                    <a target="_blank"
                        href="https://www.facebook.com/Cash2u-102189698862906/"
                        title="Facebook"
                    >
                        <FBSvg />
                    </a>
                    <a target="_blank"
                    title="Instagram"
                        href=" https://www.instagram.com/cash2u.kg/"
                    >
                        <InstagramSvg2 />
                    </a>
                    <a target="_blank"
                    title="Ok"
                        href="https://ok.ru/profile/582735885535"
                    >
                        <OkSvg />
                    </a>
                    <a target="_blank"
                    title="Vk"
                        href="https://vk.com/cash2u"
                    >
                        <VkSvg />
                    </a>
                    {/* <TwitSvg /> */}
                    <a target="_blank"
                    title="TikTok"
                        href="https://www.tiktok.com/@cash2u.kg"
                    >
                        <TiktokSvg />
                    </a>

                    <a target="_blank"
                    title="YouTube"
                        href="https://www.youtube.com/channel/UCYd4ljGJODrg1mCLNLl85aA"
                    >
                        <YouTubeSvg />
                    </a>
                    <a target="_blank"
                    title="WhatsApp"
                        href="https://wa.me/+996552553333"
                    >
                        <WpSvg />
                    </a>
                    <a target="_blank"
                    title='Telegram'
                        href="tg://resolve?domain=@KelechekMKKbot"
                    >
                        <TgSvg />
                    </a>
                </div>

                <div className={styles.bottomText}>
                    <span>{t['footer-client'].column3.bottomText}</span>
                </div>

            </Container>

        </div>
    )
}