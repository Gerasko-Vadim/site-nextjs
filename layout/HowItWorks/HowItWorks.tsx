import { useRouter } from 'next/router'
import React, { useEffect, useState } from "react"
import { Htag } from '../../components'
import { Container } from '../../components/Container/Container'
import { kg } from '../../locales/kg'
import { ru } from '../../locales/ru'
import styles from "./HowItWork.module.css"



const howItWork1 = "/business/howItWork1.png"
const howItWork2 = "/business/howItWork2.png"
const howItWork3 = "/business/howItWork3.png"
const howItWork4 = "/business/howItWork4.png"

// "block3": {
//     "title": "Как это работает?",
//         "b1": "Клиент выбирает товар или услугу в магазине Партнёра",
//             "b2": "Оплачивает покупку в рассрочку на кассе QR- кодом в приложении cash2u",
//                 "b3": "Еженедельное зачисление денег за проданный товар на счет Партнера",
//                     "b4": "Клиент возвращает сумму покупки компании cash2u равными частями в течении 3 месяцев"
// },

export const HowItWork = () => {

    const router = useRouter();
    const t = router.locale === 'ru' ? ru : kg;


    return (
        <div className={styles.wrapper} id="HowItWork">
            <Container>
                <div className={styles.wrapper}>
                    <Htag size='h1' className={styles.h1}>{t['partner-page'].block3.title}</Htag>

                    <div className={styles.slider}>
                        <div className={styles.card}>
                            <div className={styles.blockPhone}>
                                <img alt="" src={howItWork1} />
                            </div>
                            <div className={styles.answer}>
                                <div className={styles.answerCount}>1</div>
                                <div className={styles.text}>
                                    <span>
                                        {t['partner-page'].block3.b1}
                                    </span>
                                </div>
                            </div>

                        </div>
                        <div className={styles.card}>
                            <div className={styles.blockPhone}>
                                <img alt="" src={howItWork2} />
                            </div>
                            <div className={styles.answer}>
                                <div className={styles.answerCount}>2</div>
                                <div className={styles.text}>
                                    <span>
                                        {t['partner-page'].block3.b2}
                                    </span>
                                </div>
                            </div>


                        </div>
                        <div className={styles.card}>
                            <div className={styles.blockPhone}>
                                <img alt="" src={howItWork3} />
                            </div>
                            <div className={styles.answer}>
                                <div className={styles.answerCount}>3</div>
                                <div className={styles.text}>
                                    <span>
                                        {t['partner-page'].block3.b3}
                                    </span>
                                </div>
                            </div>


                        </div>
                        <div className={styles.card}>
                            <div className={styles.blockPhone}>
                                <img alt="" src={howItWork4} />
                            </div>
                            <div className={styles.answer}>
                                <div className={styles.answerCount}>4</div>
                                <div className={styles.text}>
                                    <span>
                                        {t['partner-page'].block3.b4}
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Container>
        </div>
    )
}