import { useRouter } from 'next/router'
import React from "react"
import { Htag } from '../../components'
import { Container } from '../../components/Container/Container'
import { kg } from '../../locales/kg'
import { ru } from '../../locales/ru'
import styles from "./HelpBusiness.module.css"

// "block4": {
//     "title": "Мы помогаем Вашему бизнесу расти",
//         "1": "Увеличение среднего чека",
//             "2": "раза увеличение ежедневной выручки",
//                 "3": "Привлечение новых клиентов"
// },

export const HelpBusiness = () => {
    const router = useRouter();
    const t = router.locale === 'ru' ? ru : kg;


    return (
        <div className={styles.wrapper} id="HelpBusiness">
            <Container>
                <div className={styles.wrapperTitle}>
                    <Htag size='h1'>{t['partner-page'].block4.title}</Htag>
                </div>
                <div className={styles.content}>
                    <div className={styles.card}>
                        <span className={styles.title}>
                            45%
                        </span>
                        <span className={styles.subtitle}>{t['partner-page'].block4.b1}</span>
                    </div>
                    <div className={styles.card}>
                        <span className={styles.title}>
                            2-2,5
                        </span>
                        <span className={styles.subtitle}>{t['partner-page'].block4.b2}</span>
                    </div>
                    <div className={styles.card}>
                        <span className={styles.title}>
                            100 - 200%
                        </span>
                        <span className={styles.subtitle}>{t['partner-page'].block4.b3}</span>
                    </div>
                </div>
            </Container>
        </div>
    )
}