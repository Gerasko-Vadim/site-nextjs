import { useRouter } from 'next/router'
import Raect from "react"
import { Htag } from '../../components'
import { Container } from '../../components/Container/Container'
import { kg } from '../../locales/kg'
import { ru } from '../../locales/ru'
import styles from "./Condition.module.css"

const money = "/money.svg"
const calendary = "/calendary.svg"
const clock = "/clock.svg"
const file = "/file.svg"
const phone = "/phone.svg"
const people = "/people.svg"





export const Condition = (): JSX.Element => {

    const router = useRouter();
    const t = router.locale === 'ru' ? ru : kg;

    return (
        <div className={styles.condition} id="condition">
            <Container>
                <div className={styles.blockTitle}>
                    <Htag size='h1' className={styles.title}>
                       {t.block2.title}
                    </Htag>
                </div>
                <div className={styles.wrapper}>
                    <div className={styles.card}>
                        <img alt="icon" src={money} />
                        <span>{t.block2.card1}</span>
                    </div>
                    <div className={styles.card}>
                        <img alt="icon" src={calendary} />
                        <span>{t.block2.card2}</span>
                    </div>
                    <div className={styles.card}>
                        <img alt="icon" src={clock} />
                        <span>{t.block2.card3}</span>
                    </div>
                    <div className={styles.card}>
                        <img alt="icon" src={file} />
                        <span>{t.block2.card4}</span>
                    </div>
                    <div className={styles.card}>
                        <img alt="icon" src={phone} />
                        <span>{t.block2.card5}</span>
                    </div>
                    <div className={styles.card}>
                        <img alt="icon" src={people} />
                        <span>{t.block2.card6}</span>
                        <span></span>
                    </div>
                </div>
            </Container>
        </div>
    )
}