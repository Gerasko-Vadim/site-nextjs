import React, { useEffect, useRef, useState } from "react"
import styles from "./WelcomeBlockBusiness.module.css"
import Link from 'next/link'

import { Container } from '../../components/Container/Container'
import classNames from 'classnames'
import { Htag } from '../../components';
import PhoneSvg from "./phone1.svg"
import MobileSvg from "./mobile.svg"
import { Button } from "../../components/Button/Button"
import { useRouter } from 'next/router';
import { ru } from '../../locales/ru';
import { kg } from '../../locales/kg';




export function WelcomeBlockBusiness() {
    const router = useRouter();
    const t = router.locale === 'ru' ? ru : kg;

    return (
        <div className={styles.wrapper}>
            <Container>
                <div className={styles.wrapperContent}>
                    <div className={styles.content}>
                        <Htag className={styles.title} size={'h1'}>
                            {t['partner-page'].block1.title}
                        </Htag>
                        <span className={styles.subtitle}>
                            {t['partner-page'].block1.subtitle}
                        </span>
                        <div>
                            <Link href="#ApplicationForm">
                                <Button 
                                color='yellow'
                                className={styles.btn}
                                >
                                    <span className={styles.first}></span>
                                    <span className={styles.second}></span>
                                    <span className={styles.third}></span>
                                    <span className={styles.fourth}></span>
                                    {t['partner-page'].block1.button}
                                    </Button>
                            </Link>
                        </div>
                    </div>
                    <div className={styles.blockImage}>
                        <PhoneSvg className={styles.phone} />
                    </div>
                </div>
            </Container>
            <div className={styles.mobile}>
                <div className={styles.topBlock}>
                    <MobileSvg className={styles.phone} />
                </div>
                <div className={styles.blockContent}>
                    <Container>
                        <div className={styles.content}>
                            <Htag className={styles.title} size={'h1'}>
                                {t['partner-page'].block1.title}
                            </Htag>
                            <span className={styles.subtitle}>
                                {t['partner-page'].block1.subtitle}
                            </span>
                            <div>
                                <Link href="#ApplicationForm">
                                    <Button
                                        className={styles.btn}
                                        color='yellow'>
                                        <span className={styles.first}></span>
                                        <span className={styles.second}></span>
                                        <span className={styles.third}></span>
                                        <span className={styles.fourth}></span>
                                        {t['partner-page'].block1.button}
                                    </Button>
                                </Link>
                            </div>
                        </div>
                    </Container>
                </div>

            </div>
        </div>
    );
}
