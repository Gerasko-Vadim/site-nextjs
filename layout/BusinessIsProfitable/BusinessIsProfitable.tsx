import { useRouter } from 'next/router'
import React from "react"
import { Htag } from '../../components'
import { Container } from '../../components/Container/Container'
import { kg } from '../../locales/kg'
import { ru } from '../../locales/ru'
import styles from "./BusinessIsProfitable.module.css"


// "block5": {
//     "title": "Бизнесу выгодно с cash2u",
//         "b1": "Маркетинговые инструменты для привлечения новых клиентов",
//             "b2": "Зачисление выручки на р/счет, кошелек, карту Партнера",
//                 "b3": "Получение конкурентного преимущества",
//                     "b4": "Простота и удобность приема оплаты за товары и услуги",
//                         "b5": "МКК берет на себя все риски по возврату денег от клиента",
//                             "b6": "Расширение географии ваших продаж",
//                                 "b7": "Доступ к личному кабинету Партнера",
//                                     "b8": "Долгосрочное сотрудничество и доступ к новой базе клиентов"
// },

export const BusinessIsProfitable = () => {
    const router = useRouter();
    const t = router.locale === 'ru' ? ru : kg;
    return (
        <div className={styles.wrapper}>
            <Container>
                <div className={styles.wrapperTitle} id="BusinessProfitable">
                    <Htag size='h1'>{t['partner-page'].block5.title}</Htag>
                </div>
                <div className={styles.content}>
                    <div className={styles.card}>
                        <img alt="icon" src="/business/BusinessIsProfitable/1.png" />
                        <span>
                            {t['partner-page'].block5.b1}
                        </span>
                    </div>
                    <div className={styles.card}>
                        <img alt="icon" src="/business/BusinessIsProfitable/2.png" />
                        <span>
                            {t['partner-page'].block5.b2}
                        </span>
                    </div>
                    <div className={styles.card}>
                        <img alt="icon" src="/business/BusinessIsProfitable/3.png" />
                        <span>
                            {t['partner-page'].block5.b3}
                        </span>
                    </div>
                    <div className={styles.card}>
                        <img alt="icon" src="/business/BusinessIsProfitable/4.png" />
                        <span>
                            {t['partner-page'].block5.b4}
                        </span>
                    </div>
                    <div className={styles.card}>
                        <img alt="icon" src="/business/BusinessIsProfitable/5.png" />
                        <span>
                            {t['partner-page'].block5.b5}
                        </span>
                    </div>
                    <div className={styles.card}>
                        <img alt="icon" src="/business/BusinessIsProfitable/6.png" />
                        <span>
                            {t['partner-page'].block5.b6}
                        </span>
                    </div>
                    <div className={styles.card}>
                        <img alt="icon" src="/business/BusinessIsProfitable/7.png" />
                        <span>
                            {t['partner-page'].block5.b7}
                        </span>
                    </div>
                    <div className={styles.card}>
                        <img alt="icon" src="/business/BusinessIsProfitable/8.png" />
                            <span>
                            {t['partner-page'].block5.b8}
                            </span>
                    </div>
                </div>

            </Container>
        </div>
    )
}