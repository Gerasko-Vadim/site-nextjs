import React, { useEffect } from "react"
import { Footer } from './Footer/Footer'
import { Header } from './Header/Header'
import ReactGA from 'react-ga';

import { LayoutProps } from './layout.props'
import { useRouter } from 'next/router';

ReactGA.initialize('UA-212130805-1');

export const Layout = ({ children }: LayoutProps): JSX.Element => {
    const {pathname, query} = useRouter()
    
    useEffect(() => {
        ReactGA.pageview(pathname + query?.agent);
        const isReferal = query?.agent || ''
        if (!Array.isArray(isReferal) && isReferal !=='') {
            sessionStorage.setItem('referal', isReferal)
        }
        else {
        }
    }, [pathname]);

    return (
        <>
            <Header />
            <div>
                {children}
            </div>
            <Footer />
        </>

    )
}