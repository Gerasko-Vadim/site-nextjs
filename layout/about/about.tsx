import { useRouter } from 'next/router'
import React from "react"
import { Htag } from '../../components'
import { Button } from '../../components/Button/Button'
import { Container } from '../../components/Container/Container'
import { kg } from '../../locales/kg'
import { ru } from '../../locales/ru'
import { DownloadAppTrigger } from '../../utils/analytics'


import styles from "./about.module.css"

const qrcodeImg = "/QR Code.png"
// "block8": {
//     "title": "О приложении",
//         "subtitle": "Мобильный офис который всегда с вами\n- все сферы услуг для рассрочки онлайн 24/7\n- оплата телефоном без лишних деталей\n- различные акции и бонусы от партнеров",
//             "button": "Скачать приложение",
//                 "bottomBlock": "Подробнее о том как пользоваться приложением и что в нем можно делать"
// },

export const About = (): JSX.Element => {
    const router = useRouter();
    const t = router.locale === 'ru' ? ru : kg;
    return (
        <div className={styles.about} id="about">
            <Container>
                <div className={styles.wrapper}>
                    <div className={styles.content}>
                        <div className={styles.blockTitle}>
                        <Htag size="h1" className={styles.title}>
                           {t.block8.title}
                        </Htag>
                        </div>
                        <div className={styles.text}>
                            <span className={styles.text}>{t.block8.subtitle}</span>
                            <ul>
                                <li> {t.block8.l1}</li>
                                <li> {t.block8.l2}</li>
                                <li> {t.block8.l3}</li>
                               
                            </ul>
                            
                        </div>
                        <div className={styles.bottomBlock}>
                            <div className={styles.qr}>
                                <span className={styles.qrText}>{t.block8.qrcode}</span>
                                <img 
                                    alt="qrcode"
                                    src={qrcodeImg}
                                  />
                            </div>
                            <a href="https://play.google.com/store/apps/details?id=kg.cash2u.client" target="_blank">
                                <Button
                                    className={styles.btn}
                                    onClick={() => DownloadAppTrigger()}
                                >
                                    <span className={styles.first}></span>
                                    <span className={styles.second}></span>
                                    <span className={styles.third}></span>
                                    <span className={styles.fourth}></span>
                                    {t.block8.button}
                                </Button>
                            </a>
                        </div>
                    </div>
                    <div className={styles.video}>

                    <video
                            className={styles.player}
                            controls
                            src='./2.mp4'
                        />

                    </div>
                </div>
            </Container>
        </div>
    )
}