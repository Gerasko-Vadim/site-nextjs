import classNames from 'classnames'
import { useRouter } from 'next/router'
import React from "react"
import { Htag } from '../../components'
import { Container } from '../../components/Container/Container'
import Slick from '../../components/slick/slick'
import { kg } from '../../locales/kg'
import { ru } from '../../locales/ru'
import styles from "./TrustUs.module.css"



export const TrustUs = () => {
    const router = useRouter();
    const t = router.locale === 'ru' ? ru : kg;

    const arrBrends = new Array(6)
        .fill(<div></div>)

    return (
        <div className={styles.wrapper} id="trustUs">
            <Container>
                <div className={styles.wrapperTitle}>
                    <Htag size='h1'>{t['partner-page'].block2.title}</Htag>
                </div>
               
                   <div className={styles.slider}>
                    {
                        arrBrends.map((item, index) => {
                            return(
                                <div key={index} style={{width:"240px"}} className={classNames(styles.brendsItem, styles[`brendsItem${index}`])}></div>
                            )
                        })
                    }
                    </div>

                    {/* <div className={styles.brendsItem}> </div>
                    <div className={styles.brendsItem}> </div>
                    <div className={styles.brendsItem}> </div>
                    <div className={styles.brendsItem}> </div>
                    <div className={styles.brendsItem}> </div> */}
            
            </Container>
        </div>
    )
}