import React from "react"
import { Button } from '../../components/Button/Button'
import { Container } from '../../components/Container/Container'
import { GetAloanTrigger } from '../../utils/analytics'
import PhoneMobile from "./iPhoneMobile.svg"
import styles from "./GreetBlock.module.css"

import { useRouter } from 'next/router'
import { ru } from '../../locales/ru'
import { kg } from '../../locales/kg'

const female = "/female.png"
const female760 = "/female760.png"
const female360 = "/female360.png"


export const GreetBlock = (): JSX.Element => {
    const router = useRouter();
    const t = router.locale === 'ru' ? ru : kg;

    return (
        <>
            <div className={styles.greetBlock}>
                <div className={styles.wrapper}>
                    <div className={styles.leftBlock}></div>
                    <div className={styles.wrapperContent}>
                        <div className={styles.blockContent}>
                            <span className={styles.title}>
                                {t.block1.title}
                            </span>
                            <span className={styles.subtitle}> {t.block1.subtitle}</span>
                            <a href="https://play.google.com/store/apps/details?id=kg.cash2u.client" target="_blank">
                                <Button
                                    className={styles.btn}
                                    onClick={() => GetAloanTrigger()}
                                    color='brich'
                                >
                                    <span className={styles.first}></span>
                                    <span className={styles.second}></span>
                                    <span className={styles.third}></span>
                                    <span className={styles.fourth}></span>
                                    {t.block1.button}
                                </Button>
                            </a>
                        </div>
                    </div>
                </div>
                <div className={styles.rightBlock}>
                    <img alt="female" className={styles.female} src={female} />
                    <img alt="frmale" className={styles.female760} src={female760} />
                </div>
            </div>


            <div className={styles.mobile}>
                <div className={styles.content}>
                    <div className={styles.blockText}>
                        <span className={styles.title}>
                            {t.block1.title}
                        </span>
                        <span className={styles.subtitle}> {t.block1.subtitle}</span>
                    </div>
                    <img alt="female" src={female360} />
                </div>
                <a href="https://play.google.com/store/apps/details?id=kg.cash2u.client" target="_blank">
                    <button className={styles.btn}>{t.block1.button}</button>
                </a>
            </div>
        </>
    )
}



{/* <Container>
    <div className={styles.greetBlock}>
        <div className={styles.blockContent}>
            <span className={styles.title}>
                {t.block1.title}
            </span>
            <span className={styles.subtitle}> {t.block1.subtitle}</span>
            <a href="https://play.google.com/store/apps/details?id=kg.cash2u.client" target="_blank">
                <Button
                    className={styles.btn}
                    onClick={() => GetAloanTrigger()}
                    color='brich'
                >
                    {t.block1.button}
                </Button>
            </a>
        </div>


        <div className={styles.rightBlock}>
            <div className={styles.brichBlock}></div>
            <img src={female} className={styles.phone} />
        </div>

    </div>

    <div className={styles.mobile}>
        <div className={styles.buttonBlock}>
            <PhoneMobile className={styles.phone} />
        </div>

        <div className={styles.blockContent}>
            <span className={styles.title}>
                Купи сейчас
                — <br />
                плати потом!
            </span>
            <span className={styles.subtitle}>Рассрочка без процентов<br /> и переплаты</span>
            <a href="https://play.google.com/store/apps/details?id=kg.cash2u.client" target="_blank">
                <Button
                    className={styles.btn}
                    onClick={() => GetAloanTrigger()}
                    color='brich'
                >
                    Получить кредит
                </Button>
            </a>

        </div>
    </div>
</Container> */}