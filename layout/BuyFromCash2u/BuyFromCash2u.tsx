import { useRouter } from 'next/router'
import React from "react"
import { Htag } from '../../components'
import { Container } from '../../components/Container/Container'
import { kg } from '../../locales/kg'
import { ru } from '../../locales/ru'
import styles from "./BuyFromCash2u.module.css"


export const BuyfromCash2u = (): JSX.Element => {

    const router = useRouter();
    const t = router.locale === 'ru' ? ru : kg;
    return (
        <div className={styles.BuyfromCash2u} id="BuyfromCash2u">
            <Container>
                <div className={styles.titleBlock}>
                    <Htag size="h1" className={styles.title}>
                        {t.block5.title}
                    </Htag>
                </div>
                <div className={styles.tags}>
                    <div className={styles.tag}>
                        {t.block5.category.b1}
                    </div>
                    <div className={styles.tag}>
                        {t.block5.category.b2}
                    </div>
                    <div className={styles.tag}>
                        {t.block5.category.b3}
                    </div>
                    <div className={styles.tag}>
                        {t.block5.category.b4}
                    </div>
                    <div className={styles.tag}>
                        {t.block5.category.b5}
                    </div>
                    <div className={styles.tag}>
                        {t.block5.category.b6}
                    </div>
                </div>

                <div className={styles.wrapper}>
                    <div className={styles.card}>
                        <img alt="img" src="/brends/1.png" />
                    </div>
                    <div className={styles.card}>
                        <img alt="img" src="/brends/2.png" />
                    </div>
                    <div className={styles.card}>
                        <img alt="img" src="/brends/3.png" />
                    </div>
                    <div className={styles.card}>
                        <img alt="img" src="/brends/4.png" />
                    </div>
                    <div className={styles.card}>
                        <img alt="img" src="/brends/5.png" />
                    </div>
                    <div className={styles.card}>
                        <img alt="img" src="/brends/6.png" />
                    </div>
                    <div className={styles.card}>
                        <img alt="img" src="/brends/7.png" />
                    </div>
                    <div className={styles.card}>
                        <img alt="img" src="/brends/8.png" />
                    </div>
                    <div className={styles.card}>
                        <img alt="img" src="/brends/9.png" />
                    </div>
                    <div className={styles.card}>
                        <img alt="img" src="/brends/10.png" />
                    </div>
                </div>

            </Container>
        </div>
    )
}