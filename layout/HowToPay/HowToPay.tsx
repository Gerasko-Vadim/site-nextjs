import { useRouter } from 'next/router'
import React from "react"
import { Htag } from '../../components'
import { Container } from '../../components/Container/Container'
import { kg } from '../../locales/kg'
import { ru } from '../../locales/ru'
import styles from "./HowToPay.module.css"

import JacketSvg from "./jacket.svg"
import SneakersSvg from "./sneakers.svg"



export const HowToPay = () => {
    const router = useRouter();
    const t = router.locale === 'ru' ? ru : kg;
    return (
        <div className={styles.wrapper}>
            <Container>
                <Htag size="h1" className={styles.title}>{t.block4.title}</Htag>
                <div className={styles.wrapperContent}>
                    <div className={styles.blockMonth}>
                        <div className={styles.month}>
                            <span className={styles.span}>
                                {t.block4.payments}
                            </span>
                        </div>
                        <div></div>
                        <div className={styles.month}>
                            <span className={styles.spanMonth}>МАЙ</span>
                            <span className={styles.span}>1550 сом</span>
                        </div>
                        <div className={styles.month}>
                            <span className={styles.spanMonth}>ИЮНЬ</span>
                            <span className={styles.span}>1550 сом</span>
                        </div>
                        <div className={styles.month}>
                            <span className={styles.spanMonth}>ИЮЛЬ</span>
                            <span className={styles.span}>3333 сом</span>
                        </div>
                        <div className={styles.month}>
                            <span className={styles.spanMonth}>АВГУСТ</span>
                            <span className={styles.span}>3333 сом</span>
                        </div>
                        <div className={styles.month}>
                            <span className={styles.spanMonth}>СЕНТЯБРЬ</span>
                            <span className={styles.span}>1783 сом</span>
                        </div>
                    </div>
                    <div className={styles.blockJacket}>
                        <div className={styles.blockExample}>
                            <JacketSvg className={styles.jaket} />
                            <div className={styles.exampleContent}>
                                <span className={styles.exampleTitle}>Куртка</span>
                                <span className={styles.exampleTotal}>4650 сом</span>
                            </div>
                        </div>
                        <div className={styles.jacketContent}>
                            <span className={styles.span}>1550 сом</span>
                            <span className={styles.span}>1550 сом</span>
                            <span className={styles.span}>1550 сом</span>
                        </div>
                    </div>
                    <div className={styles.blockSneacker}>
                        <div className={styles.exampleSneakers}>
                            <SneakersSvg className={styles.sneakers} />
                            <div className={styles.exampleContent}>
                                <span className={styles.exampleTitle}>Кроссовки</span>
                                <span className={styles.exampleTotal}>5350 сом</span>
                            </div>
                        </div>
                        <div className={styles.sneakersContent}>
                            <span className={styles.span}>1783 сом</span>
                            <span className={styles.span}>1783 сом</span>
                            <span className={styles.span}>1783 сом</span>
                        </div>
                    </div>
                    <div className={styles.blockRemainder}>
                        <div className={styles.blockTitle}>
                            <span className={styles.span}>{t.block4.remainder}</span>
                        </div>
                        <div className={styles.remainderContent}>
                            <span className={styles.span}>5350 сом</span>
                            <span className={styles.span}>1550 сом</span>
                            <span className={styles.span}>4883 сом</span>
                            <span className={styles.span}>8217 сом</span>
                            <span className={styles.span}>10000 сом</span>
                        </div>
                    </div>
                    <div className={styles.totalAmount}>
                        <span> {t.block4.total}</span>
                    </div>
                </div>


                <div className={styles.contentMobile}>
                    <div className={styles.mobileTitle}>
                        <span className={styles.mobileT}>{t.block4.payments}</span>
                        <div>
                            <span className={styles.exampleTitle}>Куртка</span>
                            <span className={styles.exampleTotal}>4650 сом</span>
                        </div>
                        <div>
                            <span className={styles.exampleTitle}>Кроссовки</span>
                            <span className={styles.exampleTotal}>5350 сом</span>
                        </div>
            
                        <span className={styles.mobileT}>{t.block4.remainder}</span>
                    </div>
                    <div className={styles.mobileContent}>
                        <div className={styles.mobileMonth}>
                            <div className={styles.mobileMonth2}>
                                <span className={styles.spanMonth}>МАЙ</span>
                                <span className={styles.span}>1550с</span>
                            </div>
                            <div className={styles.mobileMonth2}>
                                <span className={styles.spanMonth}>ИЮНЬ</span>
                                <span className={styles.span}>1550с</span>
                            </div>
                            <div className={styles.mobileMonth2}>
                                <span className={styles.spanMonth}>ИЮЛЬ</span>
                                <span className={styles.span}>1550с</span>
                            </div>
                            <div className={styles.mobileMonth2}>
                                <span className={styles.spanMonth}>АВГУСТ</span>
                                <span className={styles.span}>1550с</span>
                            </div>
                            <div className={styles.mobileMonth2}>
                                <span className={styles.spanMonth}>СЕНТЯБРЬ</span>
                                <span className={styles.span}>1550с</span>
                            </div>
                        </div>
                        <div className={styles.mobileJaket}>
                            <div className={styles.mobileJaketContent}>
                                <div> <span className={styles.span}>1550 </span></div>
                                <div> <span className={styles.span}>1550 </span></div>
                                <div> <span className={styles.span}>1550 </span></div>
                            </div>
                        </div>
                        <div className={styles.mobileSneacker}>
                            <div className={styles.mobileSneakersContent}>
                                <div><span className={styles.span}>1783 </span></div>
                                <div><span className={styles.span}>1783 </span></div>
                                <div><span className={styles.span}>1783 </span></div>
                            </div>
                        </div>
                        <div className={styles.mobileRemainder}>
                            <div className={styles.mobileRemainderContent}>
                                <span className={styles.span2}>5350 </span>
                                <span className={styles.span2}>5350 </span>
                                <span className={styles.span2}>5350 </span>
                                <span className={styles.span2}>5350 </span>
                                <span className={styles.span2}>5350 </span>
                            </div>
                        </div>
                    </div>
                    <div className={styles.mobileTotal}>
                        <span>{t.block4.total}</span>
                    </div>
                </div>

            </Container>
        </div>
    )
}