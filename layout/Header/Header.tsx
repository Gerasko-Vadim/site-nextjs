import React, { useEffect, useRef, useState } from "react"
import styles from "./Header.module.css"
import LogoSvg from "./cash2u.svg"
import { Container } from '../../components/Container/Container'
import { SwitchLang } from '../../components/switchLang/switchLang'
import { Nav, Navbar } from 'react-bootstrap'
import Link from 'next/link'
import classNames from 'classnames'
import { useRouter } from 'next/router'

import { ru } from "../../locales/ru"
import { kg } from "../../locales/kg"
import link from 'next/link'



export function Header() {
    const router = useRouter();
    const t = router.locale === 'ru' ? ru : kg;
    const { pathname } = router;
    return (
        <div className={styles.wrapperHeader}>
            <Container>
                <Navbar expand="lg"
                    style={{
                        minHeight: "100px",
                    }}
                >
                    <Navbar.Brand style={{ cursor: 'pointer' }}><Link href="/" ><LogoSvg /></Link></Navbar.Brand>
                    <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                    <Navbar.Collapse id="responsive-navbar-nav" className="nav-content">
                        <Nav className={classNames('m-auto', styles.links)} >
                            {
                                pathname == "/business" ?
                                    <>
                                        <Nav.Link className={styles.a} href="#trustUs">{t['header-partner'].link1}</Nav.Link>
                                        <Nav.Link className={styles.a} href="#HowItWork">{t['header-partner'].link2}</Nav.Link>
                                        <Nav.Link className={styles.a} href="#HelpBusiness">{t['header-partner'].link3}</Nav.Link>
                                        <Nav.Link className={styles.a} href="#BusinessProfitable">{t['header-partner'].link4}</Nav.Link>
                                        <Nav.Link className={styles.a} href="#ApplicationForm">{t['header-partner'].link5}</Nav.Link>
                                    </>
                                    :
                                    <>
                                        <Nav.Link className={styles.a} href="#condition">{t['header-client'].link1}</Nav.Link>
                                        <Nav.Link className={styles.a} href="#BuyfromCash2u">{t['header-client'].link2}</Nav.Link>
                                        <Nav.Link className={styles.a} href="#howToRepay">{t['header-client'].link3}</Nav.Link>
                                        <Nav.Link className={styles.a} href="#about">{t['header-client'].link4}</Nav.Link>
                                        <Nav.Link className={styles.a} href="#howToRepay">{t['header-client'].link5}</Nav.Link>
                                        <Link href="/business" locale={router.locale} >{t['header-client'].link6}</Link>
                                    </>
                            }

                        </Nav>
                        <Nav>
                            <SwitchLang />
                        </Nav>
                    </Navbar.Collapse>
                </Navbar>
            </Container>
        </div>


    );


}

export async function getStaticProps(context) {
    return {
        props: {
            context,
        },
    };
}





{/* <div className={styles.header}>
    <LogoSvg />
    <div className={styles.links}>
        <a>Условия</a>
        <a>Покупки</a>
        <a>Как оформить?</a>
        <a>О Приложении</a>
        <a>Для бизнеса</a>
        <a>Как погасить?</a>
    </div>
    <SwitchLang />
</div> */}


{/* <LogoSvg/>

                <nav className="Nav">
                    <a href="/">Home</a>
                    <a href="/">Articles</a>
                    <a href="/">About</a>
            
                </nav>
            <div {...handlers} style={{
                float: "right",
                position: "fixed",
                width: "33%",
                height: "100%",
                border: "2px dashed gray"
            }} />
            <Sidebar pageWrapId="__next" outerContainerId={"__next"} /> */}
