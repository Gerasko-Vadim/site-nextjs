import { useRouter } from 'next/router'
import React from "react"
import { Htag } from '../../components'
import { Container } from '../../components/Container/Container'
import { kg } from '../../locales/kg'
import { ru } from '../../locales/ru'
import styles from "./HowToRepay.module.css"



export const HowToRepay = (): JSX.Element => {

    const router = useRouter();
    const t = router.locale === 'ru' ? ru : kg;
    return (
        <div className={styles.howToRepay} id="howToRepay">
            <Container>
                <div className={styles.titleBlock}>
                    <Htag size="h1" className={styles.title}>
                        {t.block9.title}
                    </Htag>
                </div>

                <div className={styles.wrapper}>
                    <div className={styles.card}>
                        <div className={styles.blockImg1}>
                            <img alt="img" className={styles.terminal} src="/terminal.png" />
                        </div>
                        <div className={styles.blockContent}>
                            <span>{t.block9.cards.c1.title}</span>
                            <ul>
                                <li>{t.block9.cards.c1.li1}</li>
                                <li>{t.block9.cards.c1.li2}</li>
                            </ul>
                        </div>
                    </div>
                    <div className={styles.card}>
                        <div className={styles.blockImg2}>
                            <img alt="img" className={styles.phone} src="/cardPhone.png" />
                        </div>
                        <div className={styles.blockContent}>
                            <span>{t.block9.cards.c2.title}</span>
                            <ul>
                                <li>{t.block9.cards.c2.li1}</li>
                                <li>{t.block9.cards.c2.li2}</li>
                                <li>{t.block9.cards.c2.li3}</li>
                            </ul>
                        </div>
                    </div>
                    <div className={styles.card}>
                        <div className={styles.blockImg}>
                            <img alt="img" className={styles.bank} src="/bank.png" />
                        </div>
                        <div className={styles.blockContent}>
                            <span>{t.block9.cards.c3.title}</span>
                            <ul>
                                <li>{t.block9.cards.c3.li1}</li>
                            </ul>
                        </div>
                    </div>
                    <div className={styles.card}>
                        <div className={styles.blockImgCards}>
                            <img alt="img" className={styles.bank} src="/bank-cards.png" />
                        </div>
                        <div className={styles.blockContent}>
                            <span>{t.block9.cards.c4.title}</span>
                            <ul>
                                <li>{t.block9.cards.c4.li1}</li>
                                <li>{t.block9.cards.c4.li2}</li>
                                <li>{t.block9.cards.c4.li3}</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </Container>
        </div>
    )
}


