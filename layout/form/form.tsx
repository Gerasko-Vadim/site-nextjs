import Raect, { useState } from "react"
import { Form } from 'react-bootstrap'
import { Htag } from '../../components'
import { Container } from '../../components/Container/Container'
import styles from "./form.module.css"
import ReactGA from 'react-ga'
import { useRouter } from 'next/router'
import { ru } from '../../locales/ru'
import { kg } from '../../locales/kg'


// "block10": {
//     "title": "Заполнив анкету вы сможете:",
//         "subtitle": "Получать уведомления о специальных акциях и бонусах только для пользователей приложения «cash2u». Поэтому если Вам интересно скорее заполняй анкету и получай доступ к лучшим акция и бонусам",
//             "formTitle": "Узнать о акциях и бонусах сейчас!",
//                 "placeholder": "Электронная почта",
//                     "checkbox": "Выражаю согласие на обработку персональных данных и подтверждаю,что ознакомлен с Политикой обработки персональных данных",
//                         "button": "Отправить"
// },

export const FormBlock = (): JSX.Element => {

    const router = useRouter();
    const t = router.locale === 'ru' ? ru : kg;

    const [inputValue, setInputValue] = useState('')
    const search = () => {
        ReactGA.event({
            category: 'Send_email',
            action: `Send email from ${inputValue}`,
        });
    }

    return (
        <div className={styles.formBlock}>
            <Container>
                <div>
                    <Htag size="h1" color="white" className={styles.title}>
                        {t.block10.title}
                    </Htag>
                    <span className={styles.desc}>{t.block10.subtitle}</span>
                    <form>
                        <div className={styles.form}>
                            <input
                                placeholder={t.block10.placeholder}
                                onChange={(e) => setInputValue(e.target.value)}
                            />
                            {/* <div className={styles.checkboxBlockResponsive}>
                                <input
                                    type="checkbox"
                                    id="check1"
                                    required={true}

                                    className="checkbox" />
                                <label htmlFor="check1" className="check-text">
                                </label>
                                <span>{t.block10.checkbox}</span>
                            </div> */}
                            <button 
                            className={styles.btn}
                            onClick={() => search()}>
                                <span className={styles.first}></span>
                                <span className={styles.second}></span>
                                <span className={styles.third}></span>
                                <span className={styles.fourth}></span>
                                {t.block10.button}
                            </button>
                        </div>
                        {/* <div className={styles.checkboxBlock}>
                            <input
                                type="checkbox"
                                id="check1"
                                required={true}
                                className="checkbox" />
                            <label htmlFor="check1" className="check-text">
                            </label>
                            <span> {t.block10.checkbox}</span>
                        </div> */}
                    </form>
                </div>

            </Container>
        </div>
    )
}
