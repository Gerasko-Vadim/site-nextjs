import { useRouter } from 'next/router'
import React, { useState } from "react"
import InputMask from 'react-input-mask';
import { toast, ToastContainer } from 'react-toastify'
import { Htag } from '../../components'
import { Container } from '../../components/Container/Container'
import { kg } from '../../locales/kg'
import { ru } from '../../locales/ru'
import styles from "./ApplicationForm.module.css"
import 'react-toastify/dist/ReactToastify.css';

// "form": {
//     "title": "Заполните заявку и станьте партнером!",
//         "subtitle": "Общая информация",
//             "input1": "Контактное имя",
//                 "input2": "Мобильный телефон",
//                     "input3": "Электронная почта",
//                         "checkbox": "Выражаю согласие на обработку персональных данных и подтверждаю,что ознакомлен с Политикой обработки персональных данных",
//                             "button": "Отправить"
// }

export const ApplicationForm = () => {
    const [name, setName] = useState('')
    const [phone, setPhone] = useState('')
    const [brand_name, setBrend] = useState('')

    const router = useRouter();
    const t = router.locale === 'ru' ? ru : kg;

    const onSubmit = async (e: any) => {
        e.preventDefault()
        await fetch('https://stage.c2u.io:5005/referrals/create', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            redirect: 'follow',
            body: JSON.stringify({
                name,
                phone,
                brand_name,
                "referral_code": sessionStorage.getItem('referal') || ''
            })
        })
            .then(res => {
                toast.success("Заявка отправлена успешно !!")
                setName('')
                setPhone('')
                setBrend('')
            })
            .catch(err => toast.error("Сообщение не отправлено :("))

    }
    return (
        <div className={styles.wrapper} id="ApplicationForm">
            <ToastContainer
                position="top-right"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
            />
            <Container>
                <div className={styles.content}>
                    <div className={styles.blockText}>
                        <Htag size="h1" color='white' className={styles.title}>
                            {t['partner-page'].form.title}
                        </Htag>
                        <span className={styles.subtitle}>{t['partner-page'].form.subtitle}</span>
                    </div>
                    <div className={styles.blockForm}>
                        <form className={styles.form} onSubmit={onSubmit}>
                            <input
                                placeholder={t['partner-page'].form.input1}
                                required={true}
                                value={name}
                                type="text"
                                onChange={(e) => setName(e.target.value)}
                            />
                            <InputMask
                                mask="+996 (555) 555-555"
                                placeholder={t['partner-page'].form.input2}

                                value={phone}
                                required={true}
                                formatChars={
                                    {
                                        '5': '[0-9]',
                                        'a': '[A-Za-z]',
                                        '*': '[A-Za-z0-9]'
                                    }
                                }

                                onChange={(e) => setPhone(e.target.value)}
                                maskChar="_" />
                            <input
                                placeholder={t['partner-page'].form.input3}
                                required={true}
                        
                                type="text"
                                value={brand_name}
                                onChange={(e) => setBrend(e.target.value)}
                            />
                            <div className={styles.blockCheckbox}>
                                <input
                                    type="checkbox"
                                    required={true} />
                                <>
                                    <span>
                                        {t['partner-page'].form.checkbox}
                                    </span>
                                </>
                            </div>
                            <button
                                className={styles.btn}
                            >
                                <span className={styles.first}></span>
                                <span className={styles.second}></span>
                                <span className={styles.third}></span>
                                <span className={styles.fourth}></span>
                                {t['partner-page'].form.button}
                            </button>
                        </form>
                    </div>
                </div>
            </Container>
        </div>
    )
}