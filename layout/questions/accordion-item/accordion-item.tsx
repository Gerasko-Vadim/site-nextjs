import React, { useState } from "react"
import { Htag } from '../../../components'
import { AccordionItemProps } from './accordion-item.props'
import styles from "./accordion-item.module.css"
import cn from "classnames"



const AccordionItem = ({ title, info }: AccordionItemProps): JSX.Element => {
    const [expanded, setExpanded] = useState(false)
    return (
        <article className={styles.question} onClick={() => setExpanded(!expanded)}>
            <div className={styles.content}>
                <Htag size='h3' className={cn({
                    [styles.onClick]: expanded
                })}>
                    {title}
                </Htag>
                {
                    expanded && <div className={styles.answerBlock}>
                        <p>{info}</p>
                    </div>
                }
            </div>
            <div>
                <span
                    
                    className={cn(styles.btn, {
                        [styles.btnOnClick]: expanded,
                        [styles.btnOffClick]: !expanded

                    })}
                >
                    {/* {expanded ? '-' : '+'} */}
                </span>
            </div>
        </article>

    )
}
export default AccordionItem;


{/* <header >
    <div className={styles.title}>
        <h4 onClick={() => setExpanded(!expanded)} className='question-title'>
            {title}
        </h4>
    </div>

   
</header>
{ expanded && <p className={styles.content}>{info}</p> } */}