import { DetailedHTMLProps, HTMLAttributes, ReactNode } from 'react';

export interface AccordionItemProps extends DetailedHTMLProps<HTMLAttributes<HTMLElement>, HTMLElement> {
    title: any;
    info: ReactNode | string;
}