import { useRouter } from 'next/router'
import React from "react"
import { Htag } from '../../components'
import { Container } from '../../components/Container/Container'
import { kg } from '../../locales/kg'
import { ru } from '../../locales/ru'
import AccordionItem from './accordion-item/accordion-item'
import styles from "./questions.module.css"


export const Questions = (): JSX.Element => {

    const router = useRouter();
    const t = router.locale === 'ru' ? ru : kg;
    return (
        <div className={styles.questions} id="questions">
            <Container>
                <div className={styles.titleBlock}>
                    <Htag size="h1" className={styles.title}>
                        {t.block7.title}
                    </Htag>
                </div>
                <div className={styles.content}>
                    <div>
                        <AccordionItem
                            title={t.block7.questions.q4.title}
                            info={t.block7.questions.q4.answers}
                        />
                        <AccordionItem
                            title={t.block7.questions.q7.title}
                            info={t.block7.questions.q7.answers}
                        />
                        <AccordionItem
                            title={t.block7.questions.q6.title}
                            info={t.block7.questions.q6.answers}
                        />
                    </div>
                    <div>
                        <AccordionItem
                            title={t.block7.questions.q5.title}
                            info={t.block7.questions.q5.answers}
                        />
                        <AccordionItem
                            title={t.block7.questions.q2.title}
                            info={t.block7.questions.q2.answers}
                        />
                        <AccordionItem
                            title={t.block7.questions.q3.title}
                            info={t.block7.questions.q3.answers}
                        />
                    </div>
                </div>
            </Container>
        </div>
    )
}