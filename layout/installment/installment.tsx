import { useRouter } from 'next/router'
import React from 'react'

import { Htag } from '../../components'
import { Container } from '../../components/Container/Container'
import { kg } from '../../locales/kg'
import { ru } from '../../locales/ru'
import styles from "./installment.module.css"

const one = "/1.png"
const two = "/2.png"
const three = "/3.png"


// "block3": {
//     "title": "Бөлүп төлөөнү кантип тариздөө керек?",
//         "card1": "Мобилдик колдонмону орнотуу GooglePlay жана AppStoreдан Cash2u.",
//             "card2": "Толтуруу менен идентификациядан өтүңүз бөлүп төлөө планын алуу үчүн анкета.",
//                 "card3": "Жоопту күтүңүз жана акчаны алыңыз"
// },


export const Installment = (): JSX.Element => {
    const router = useRouter();
    const t = router.locale === 'ru' ? ru : kg;
    return (
        <div className={styles.installment} id="howToRepay">
            <Container>
                <div className={styles.blockTitle}>
                    <Htag size="h1" >
                        {t.block3.title}
                    </Htag>
                </div>
                <div className={styles.wrapper}>
                    <div className={styles.card}>
                        <img alt="one" className={styles.img} src={one} />
                        <div className={styles.text}>
                            <Htag size="h2" className={styles.h1}>
                                {t.block3.subtitle1}
                            </Htag>

                            <span>{t.block3.card1}</span>
                        </div>
                    </div>
                    <div className={styles.card}>
                        <img alt="one" className={styles.img} src={two} />
                        <div className={styles.text}>
                            <Htag size="h2" className={styles.h1}>
                                {t.block3.subtitle2}
                            </Htag>

                            <span>{t.block3.card2}</span>
                        </div>

                    </div>
                    <div className={styles.card}>
                        <img alt="one" className={styles.img} src={three} />
                        <div className={styles.text}>
                            <Htag size="h2" className={styles.h1}>
                                {t.block3.subtitle3}
                            </Htag>
                            <span>{t.block3.card3}</span>
                        </div>
                    </div>
                </div>
            </Container>
        </div>
    )
}




