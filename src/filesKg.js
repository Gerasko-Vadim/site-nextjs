import { consent } from "./view/pages/docs/files-kg/consent-of-the-personal-data-subject";
import { UserInfoKg } from "./view/pages/docs/files-kg/user-info";
import { loanAgreement } from "./view/pages/docs/files-kg/loan-agreement";
import { oferta } from "./view/pages/docs/files-kg/oferta";
import { ConfirmationForUser } from "./view/pages/docs/files-kg/text-for-confirmation-user";
import { privacyPolicy } from './view/pages/docs/files-kg/privacy-policy';





export const filesKgList = [
    {
        name: "КАРДАР ҮЧҮН МААЛЫМАТ",
        path: "info-user",
        file: UserInfoKg
    },
    {
        name: "1-тиркеме",
        path: "rights-and-expenses",
        file: ConfirmationForUser
    },
    {
        name: "Коомдук сунуш",
        path: "oferta",
        file: oferta
    },
    {
        name: "Жеке маалыматтар субъектинин анын жеке маалыматтарын чогултууга жана иштетүүгө макулдугу",
        path: "consent-of-the-personal-data-subject",
        file: consent
    },
    {
        name: "Мамлекеттик насыя келишими",
        path: "loan-agreement",
        file: loanAgreement
    },
    // {
    //     name: "Реквизиты МКК Келечек для МП",
    //     path: "requistes",
    //     file: requisites
    // },
    {
        name: "Мобилдик колдонмонун коопсуздугу жана купуялык саясаты",
        path: "privacy-policy",
        file: privacyPolicy
    },
    // {
    //     name:"Паспорт кредитного продукта «Онлайн»",
    //     path: "passport-credit-doc",
    //     file: passportCreditDoc
    // }
]