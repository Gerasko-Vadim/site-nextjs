import logo from './logo.svg';
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import { useLocation, } from 'react-router';
import ReactGA from 'react-ga';
import Main from './view/pages/main';
import Footer from "../src/view/components/footer"
import Header from './view/components/header';
import Business from './view/pages/business';
import Docs from './view/pages/docs';
import Doc from './view/pages/doc';
import PrivacyPolicy from './view/pages/privacyPolicy';
import { useEffect } from 'react';
import Routes from './routes';



const App = ()=> {



  return (
    <div className="App">

      <Router>
        <Header />
        <Routes/>
        <Footer />
      </Router>

    </div>
  );
}

export default App;
