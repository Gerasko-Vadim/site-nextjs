
const initialState = {
    isClient: true
}

export const HandleChanheHeader = (state = initialState, action)=>{
    switch(action.type){
        case "SET_PARTNER":
            return{
                ...state,
                isClient:false
            }
        case "SET_CLIENT":
            return{
                ...state,
                isClient: true
            }
        default:
            return state
    }
}