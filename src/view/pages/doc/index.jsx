import React, { useEffect, useState } from "react"
import { useLocation } from "react-router"
import renderHTML from 'react-render-html';
import "./style.scss"
import { oferta } from "../docs/files/oferta";
import { files } from "../../../files";
import { UserInfo } from "../docs/files/info-users";
import { filesKgList } from '../../../filesKg';


const Doc = () => {
    const [file, setFile] = useState()
    const { pathname } = useLocation()

    useEffect(() => {
        window.scrollTo(0, 0)

        if (pathname.split('/').includes('kg')) {
            setFile(filesKgList.find((file) => file.path === pathname.split('/').pop()))
        }
        else {
            setFile(files.find((file) => file.path === pathname.split('/').pop()))
        }
    }, [])


    return (
        <div className="doc-page">
            <div className="doc-page__content">
                {
                    file &&
                    renderHTML(file.file)
                }

            </div>
        </div>
    )
}
export default Doc;