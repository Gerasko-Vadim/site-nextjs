
export const passportCreditDoc = `
<html>

<head>
<meta http-equiv=Content-Type content="text/html; charset=utf-8">
<meta name=Generator content="Microsoft Word 15 (filtered)">
<style>
<!--
 /* Font Definitions */
 @font-face
	{font-family:"Cambria Math";
	panose-1:2 4 5 3 5 4 6 3 2 4;}
@font-face
	{font-family:Calibri;
	panose-1:2 15 5 2 2 2 4 3 2 4;}
@font-face
	{font-family:Georgia;
	panose-1:2 4 5 2 5 4 5 2 3 3;}
@font-face
	{font-family:"Noto Sans Symbols";}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin-top:0in;
	margin-right:0in;
	margin-bottom:8.0pt;
	margin-left:0in;
	line-height:107%;
	font-size:11.0pt;
	font-family:"Calibri",sans-serif;}
.MsoChpDefault
	{font-family:"Calibri",sans-serif;}
.MsoPapDefault
	{margin-bottom:8.0pt;
	line-height:107%;}
@page WordSection1
	{size:8.5in 11.0in;
	margin:56.7pt 42.5pt 56.7pt 85.05pt;}
div.WordSection1
	{page:WordSection1;}
 /* List Definitions */
 ol
	{margin-bottom:0in;}
ul
	{margin-bottom:0in;}
-->
</style>

</head>

<body lang=EN-US style='word-wrap:break-word'>

<div class=WordSection1>

<p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
line-height:normal'><b><span lang=RU style='font-size:12.0pt;font-family:"Times New Roman",serif;
color:black'> Согласие субъекта персональных данных </span></b></p>

<p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
line-height:normal'><b><span lang=RU style='font-size:12.0pt;font-family:"Times New Roman",serif;
color:black'>на сбор и обработку его персональных данных</span></b></p>

<p class=MsoNormal style='text-align:justify'><b><span lang=RU
style='font-size:12.0pt;line-height:107%;font-family:"Times New Roman",serif'>&nbsp;</span></b></p>

<p class=MsoNormal style='margin-bottom:0in;text-align:justify'><span lang=RU
style='font-size:12.0pt;line-height:107%;    word-wrap: break-word; font-family:"Times New Roman",serif'>Я,
_______________________________________________________________________________________________</span></p>

<p class=MsoNormal style='text-align:justify'><span lang=RU style='font-size:
12.0pt;line-height:107%;font-family:"Times New Roman",serif'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
(фамилия, имя, отчество субъекта)</span></p>

<p class=MsoNormal style='text-align:justify'><span lang=RU style='font-size:
12.0pt;line-height:107%; word-wrap: break-word; font-family:"Times New Roman",serif'>ПИН, присвоенный в
Кыргызской Республике: _________________________________________________________</span></p>

<p class=MsoNormal style='margin-bottom:0in;text-align:justify'><span lang=RU
style='font-size:12.0pt;line-height:107%;font-family:"Times New Roman",serif'>Документ,
удостоверяющий личность: _________________ серия ___________ № ____________________________</span></p>

<p class=MsoNormal style='text-align:justify'><span lang=RU style='font-size:
12.0pt;line-height:107%;font-family:"Times New Roman",serif'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
(вид документа)</span></p>

<p class=MsoNormal style='margin-bottom:0in;text-align:justify'><span lang=RU
style='font-size:12.0pt;line-height:107%;    word-wrap: break-word;font-family:"Times New Roman",serif'>выдан:
____________________________________________________________________________________________</span></p>

<p class=MsoNormal style='text-align:justify'><span lang=RU style='font-size:
12.0pt;line-height:107%;font-family:"Times New Roman",serif'>&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
(дата выдачи, наименование органа выдачи)</span></p>

<p class=MsoNormal style='text-align:justify'><span lang=RU style='font-size:
12.0pt;line-height:107%;word-wrap: break-word; font-family:"Times New Roman",serif'>Адрес фактического
проживания: _____________________________________________________________________</span></p>

<p class=MsoNormal style='text-align:justify'><span lang=RU style='font-size:
12.0pt;line-height:107%;    word-wrap: break-word; font-family:"Times New Roman",serif' >__________________________________________________________________________________________________</span></p>

<p class=MsoNormal style='text-align:justify'><span lang=RU style='font-size:
12.0pt;line-height:107%;word-wrap: break-word; font-family:"Times New Roman",serif'word-wrap: break-word;>Адрес места
прописки: ______________________________________________________________________________</span></p>

<p class=MsoNormal style='text-align:justify'><span lang=RU style='font-size:
12.0pt;line-height:107%;font-family:"Times New Roman",serif'>___________________________________________________________________________________________________</span></p>

<p class=MsoNormal style='text-align:justify'><span lang=RU style='font-size:
12.0pt;line-height:107%;font-family:"Times New Roman",serif'>&nbsp;</span></p>

<p class=MsoNormal style='text-align:justify'><span lang=RU style='font-size:
12.0pt;line-height:107%;word-wrap: break-word; font-family:"Times New Roman",serif'>Контактный
телефон: __________________________ эл.почта ______________________________________________</span></p>

<p class=MsoNormal style='text-align:justify'><span lang=RU style='font-size:
12.0pt;line-height:107%;font-family:"Times New Roman",serif'>&nbsp;</span></p>

<p class=MsoNormal style='text-align:justify'><span lang=RU style='font-size:
12.0pt;line-height:107%;font-family:"Times New Roman",serif'>даю согласие ОсОО
«МКК «Келечек» (далее «МКК») действующему на на основании Свидетельства об
учетной регистрации от 6 августа 2021 года за № 510</span><span lang=RU
style='font-size:12.0pt;line-height:107%;font-family:"Times New Roman",serif'>,
</span><span lang=RU
style='font-size:12.0pt;line-height:107%;font-family:"Times New Roman",serif'> выданной Национальным банком Кыргызской Республики
</span><span
lang=RU style='font-size:12.0pt;line-height:107%;font-family:"Times New Roman",serif'>:</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:0in;
margin-left:.5in;text-align:justify;text-indent:-.25in;border:none'><span
lang=RU style='font-size:12.0pt;line-height:107%;font-family:"Noto Sans Symbols";
color:black'>✔<span style='font:7.0pt "Times New Roman"'> </span></span><span
lang=RU style='font-size:12.0pt;line-height:107%;font-family:"Times New Roman",serif;
color:black'>на обработку моих персональных данных (сбор, систематизация,
накопление, запись, хранение, актуализации (обновление изменение), группировка,
блокирование, уничтожение персональных данных) автоматическими средствами или
без таковых;</span></p>

<p class=MsoNormal style='margin-left:.5in;text-align:justify;text-indent:-.25in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Noto Sans Symbols";color:black'>✔<span style='font:7.0pt "Times New Roman"'> </span></span><span
lang=RU style='font-size:12.0pt;line-height:107%;font-family:"Times New Roman",serif;
color:black'>передачу моих персональных данных третьим лицам в соответствии с
Законом Кыргызской Республики «Об информации персонального характера» и
международными договорами, трансграничную передачу моих персональных данных
держателям, находящимся под юрисдикцией других государств,</span></p>

<p class=MsoNormal style='text-align:justify'><span lang=RU style='font-size:
12.0pt;line-height:107%;font-family:"Times New Roman",serif'>в связи с чем:</span></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>1.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Настоящим даю своё согласие на
обработку МКК моих персональных данных включая, но не ограничиваясь, получение
от моего имени и/или от любых третьих лиц, в том числе:</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:0in;
margin-left:49.65pt;text-align:justify;text-indent:-21.3pt;border:none'><span
lang=RU style='font-size:12.0pt;line-height:107%;font-family:"Times New Roman",serif;
color:black'>-<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Государственная налоговая
служба при Правительстве Кыргызской Республики;</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:0in;
margin-left:49.65pt;text-align:justify;text-indent:-21.3pt;border:none'><span
lang=RU style='font-size:12.0pt;line-height:107%;font-family:"Times New Roman",serif;
color:black'>-<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Социальный Фонд Кыргызской
Республики;</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:0in;
margin-left:49.65pt;text-align:justify;text-indent:-21.3pt;border:none'><span
lang=RU style='font-size:12.0pt;line-height:107%;font-family:"Times New Roman",serif;
color:black'>-<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Государственная
регистрационная служба при Правительстве Кыргызской Республики;</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:0in;
margin-left:49.65pt;text-align:justify;text-indent:-21.3pt;border:none'><span
lang=RU style='font-size:12.0pt;line-height:107%;font-family:"Times New Roman",serif;
color:black'>-<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>ГУ «Унаа» при Государственной
регистрационной службе при Правительстве Кыргызской Республики;</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:0in;
margin-left:49.65pt;text-align:justify;text-indent:-21.3pt;border:none'><a
name="_heading=h.gjdgxs"></a><span lang=RU style='font-size:12.0pt;line-height:
107%;font-family:"Times New Roman",serif;color:black'>-<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Департамент кадастра и регистрации
прав на недвижимое имущество при государственной регистрационной службе при
Правительстве Кыргызской Республики;</span></p>

<p class=MsoNormal style='margin-bottom:0in;text-align:justify;border:none'><span
lang=RU style='font-size:12.0pt;line-height:107%;font-family:"Times New Roman",serif;
color:black'>а также от любых иных третьих лиц, действующих в соответствии с
требованиями законодательства Кыргызской Республики, моих персональных данных.</span></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>2.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Настоящее согласие дается
исключительно в целях исполнения Оферты МКК размещенной в разделе «Документы»
Мобильного приложения «Cash2U» и на официальном веб-сайте МКК по адресу:
www.cash2u.kg, а также иных Договоров (далее - «Договор») заключённых между
мною и МКК, и для осуществления МКК своих функций согласно Оферте и Договорам,
согласие распространяется на следующую информацию: фамилия, имя, отчество, год,
месяц, дата и место рождения, адрес прописки и проживания, данные документа удостоверяющего
личность, адрес электронной почты, номер мобильного телефона, данные об
осуществленных операциях через сервис «Cash2U»  (при необходимости), сведения
об образовании, идентификационный номер налогоплательщика, данные о наличии
либо отсутствии судимости и любая иная информация, доступная, либо известная в
любой конкретный момент времени МКК (далее - Персональные данные);</span></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>3.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Настоящее согласие действует с
даты предоставления (подписания простой электронной подписью) мною в форме
электронного документа настоящего согласия и до полного исполнения мною своих
обязательств по Оферте и договорам, заключённым с МКК, а также на срок хранения
данных об оказанных услугах в соответствии с законодательством Кыргызской
Республики.</span></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>4.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Согласие может быть отозвано
досрочно только после полного исполнения обязательств по Оферте и иным
Договорам, заключённым с МКК, путем направления соответствующего письменного
уведомления МКК в произвольной форме, при этом обработка персональных данных
полностью или частично может быть продолжена в соответствии со статьями 5 и 15
Закона Кыргызской Республики «Об информации персонального характера».</span></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>5.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Настоящим даю МКК свое
согласие на осуществления любых действий в отношении моих Персональных данных,
которые необходимы или желаемы для целей исполнения Оферты, договоров и
осуществления МКК функций по предоставлению услуг, указанных в Оферте, а также
разработки МКК новых продуктов и услуг, а также осуществление любых иных
действий с моими Персональными данными с учетом положений Закона Кыргызской
Республики «Об информации персонального характера».</span></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>6.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Я подтверждаю, что вся
вышеизложенная информация достоверная, а также даю согласие и не буду иметь
претензий на получение и/или предоставление сведений обо мне в ОЮЛ «Ассоциация
пользователей кредитной информации» и «Кредитно-информационное бюро «Ишеним»
(по тексту - Кредитное бюро), в том числе я даю свое согласие МКК на обработку
Персональных данных и сведений обо мне членам Комитета по соблюдению Кодекса
поведения, Наблюдателям и другим лицам, присутствующим на заседании
вышеуказанного Комитета при рассмотрении спора. К данным сведениям помимо
Персональных данных также могут относиться следующие сведения: адрес ведения
бизнеса и любые прежние адреса, род и вид деятельности, сведения по кредитам
(вид кредита, сумма кредита, роль субъекта, дата подачи заявления, подробные
сведения об истории выплат, непогашенный остаток, сумма дефолта и общая сумма
долга по данному счету, сведения о поручителях/гарантах, залогодателях и других
лиц, у которых образовались или имеются обязательства перед Поставщиками
информации и другие сведения) полученным мною в МКК, сведения о мерах,
предпринятых в отношении меня.</span></p>

<p class=MsoNormal style='margin-top:12.0pt;margin-right:0in;margin-bottom:
12.0pt;margin-left:0in;text-align:justify;text-indent:0in'><span lang=RU
style='font-size:12.0pt;line-height:107%;font-family:"Times New Roman",serif'>7.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif'>Я подтверждаю достоверность данных,
указанных в настоящей анкете и в  соответствии с требованиями Закона Кыргызской
Республики «Об информации персонального характера» даю согласие на обработку
персональных данных в целях выполнения требований законодательства Кыргызской
Республики в сфере противодействия финансированию террористической деятельности
и легализации (отмыванию) преступных доходов.</span></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>8.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Я подтверждаю, что ознакомлен
(а) с положениями Закона Кыргызской Республики «Об информации персонального
характера», Порядком получения согласия субъекта персональных данных на сбор и
обработку его персональных данных, порядком и формой уведомления субъектов
персональных данных о передаче их персональных данных третьей стороне,
утвержденным постановлением Правительства Кыргызской Республики от 21 ноября
2017 года № 759.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>9.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Настоящее согласие, выраженное
в форме электронного документа, подписанного простой электронной подписью,
ключом которой является логин и пароль для авторизации в приложении «Cash2U»,
распространяется на все персональные данные и является равнозначным
(идентичным) моему согласию на предоставление, обработку, передачу персональных
данных, подписанному собственноручной подписью и предоставленному на бумажном
носителе без каких-либо ограничений/исключений.</span></p>

</div>

</body>

</html>

`