import React from "react"


export const UserInfo = `
<html>

<head>
<meta http-equiv=Content-Type content="text/html; charset=utf-8">
<meta name=Generator content="Microsoft Word 15 (filtered)">
<style>
<!--
 /* Font Definitions */
 @font-face
	{font-family:"Times New Roman",serif;;
	panose-1:2 4 5 3 5 4 6 3 2 4;}
@font-face
	{font-family:"Times New Roman",serif;}
@font-face
	{font-family:"Arial Unicode MS";
	panose-1:2 11 6 4 2 2 2 2 2 4;}
@font-face
	{font-family:"Times New Roman",serif;
	panose-1:2 4 5 2 5 4 5 2 3 3;}
@font-face
	{font-family:"Times New Roman",serif;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin:0in;
	text-align:justify;
	text-indent:28.35pt;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
.MsoChpDefault
	{font-size:12.0pt;}
.MsoPapDefault
	{text-align:justify;
	text-indent:28.35pt;}
@page WordSection1
	{size:595.3pt 841.9pt;
	margin:56.7pt 42.5pt 56.7pt 85.05pt;}
div.WordSection1
	{page:WordSection1;}
 /* List Definitions */
 ol
	{margin-bottom:0in;}
ul
	{margin-bottom:0in;}
-->
</style>

</head>

<body lang=EN-US style='word-wrap:break-word'>

<div class=WordSection1>

<p class=MsoNormal align=center style='text-align:center'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>ИНФОРМАЦИЯ
ДЛЯ КЛИЕНТА</span></p>

<p class=MsoNormal style='text-indent:0in;line-height:114%'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>Для
обеспечения безопасности в процессе проведения операций в рамках
предоставляемых Обществом услуг и защиты персональных данных, Клиент обязан</span><span
lang=RU style='font-size:10.5pt;line-height:114%;font-family:"Times New Roman",serif;
color:black'>:</span></p>

<p class=MsoNormal style='margin-left:.5in;text-indent:-.25in;line-height:114%;
border:none'><span lang=RU style='color:black'>-<span style='font:7.0pt "Times New Roman"'>
</span></span><span lang=RU style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;'>&nbsp;не раскрывать посторонним лицам
свой персональный идентификационный номер (ПИН), пароль, пароль от электронной
почты, иные сведения, которые могут способствовать несанкционированному доступу
при удаленном/дистанционном обслуживании от имени пользователя;&nbsp;</span></p>

<p class=MsoNormal style='margin-left:.5in;text-indent:-.25in;line-height:114%;
border:none'><span lang=RU style='color:black'>-<span style='font:7.0pt "Times New Roman"'>
</span></span><span lang=RU style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;'>&nbsp;периодически менять свои
Авторизационные данные, используемые для входа мобильное приложение;</span></p>

<p class=MsoNormal style='margin-left:.5in;text-indent:-.25in;line-height:114%;
border:none'><span lang=RU style='color:black'>-<span style='font:7.0pt "Times New Roman"'>
</span></span><span lang=RU style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;'>&nbsp;не позволять другим
использовать свой мобильный телефон, через который осуществляются операции;</span></p>


<p class=MsoNormal style='margin-left:.5in;text-indent:-.25in;line-height:114%;
border:none'><span lang=RU>-</span><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>&nbsp;&nbsp;при потере или краже мобильного
телефона, незамедлительно сообщить об этом ОсОО “МКК
Келечек”</span></p>



<p class=MsoNormal style='margin-left:.5in;text-indent:-.25in;line-height:114%;
border:none'><span lang=RU>-</span><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>&nbsp;не отправлять свою личную
информацию, пароль или персональный идентификационный номер через электронную почту, социальные сети и другие средства электронного обмена данными</span></p>



<p class=MsoNormal style='margin-left:.5in;text-indent:-.25in;line-height:114%;
border:none'><span lang=RU>-</span><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>&nbsp;не отправлять свою личную
информацию, пароль или персональный идентификационный номер через электронную почту, социальные сети и другие средства электронного обмена данными</span></p>

<p class=MsoNormal style='margin-left:.5in;text-indent:-.25in;line-height:114%;
border:none'><span lang=RU>-</span><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>&nbsp;незамедлительно сообщить ОсОО “МКК Келечек” при
возникновении любых вопросов относительно безопасности доступа к мобильному
приложению.</span></p>

</div>

</body>

</html>`

