export const requisites = `
<html>
    <head>
        <meta content="text/html; charset=UTF-8" http-equiv="content-type" />
        <style type="text/css">
            ol {
                margin: 0;
                padding: 0;
            }
            table td,
            table th {
                padding: 0;
            }
            .c0 {
                color: #000000;
                font-weight: 400;
                text-decoration: none;
                vertical-align: baseline;
                font-size: 11pt;
                font-family: "Arial";
                font-style: normal;
            }
            .c2 {
                padding-top: 0pt;
                padding-bottom: 0pt;
                line-height: 1.15;
                orphans: 2;
                widows: 2;
                text-align: left;
                height: 11pt;
            }
            .c1 {
                padding-top: 12pt;
                padding-bottom: 12pt;
                line-height: 1.15;
                orphans: 2;
                widows: 2;
                text-align: left;
            }
            .c3 {
                background-color: #ffffff;
                max-width: 451.4pt;
                padding: 72pt 72pt 72pt 72pt;
            }
            .title {
                padding-top: 0pt;
                color: #000000;
                font-size: 26pt;
                padding-bottom: 3pt;
                font-family: "Arial";
                line-height: 1.15;
                page-break-after: avoid;
                orphans: 2;
                widows: 2;
                text-align: left;
            }
            .subtitle {
                padding-top: 0pt;
                color: #666666;
                font-size: 15pt;
                padding-bottom: 16pt;
                font-family: "Arial";
                line-height: 1.15;
                page-break-after: avoid;
                orphans: 2;
                widows: 2;
                text-align: left;
            }
            li {
                color: #000000;
                font-size: 11pt;
                font-family: "Arial";
            }
            p {
                margin: 0;
                color: #000000;
                font-size: 11pt;
                font-family: "Arial";
            }
            h1 {
                padding-top: 20pt;
                color: #000000;
                font-size: 20pt;
                padding-bottom: 6pt;
                font-family: "Arial";
                line-height: 1.15;
                page-break-after: avoid;
                orphans: 2;
                widows: 2;
                text-align: left;
            }
            h2 {
                padding-top: 18pt;
                color: #000000;
                font-size: 16pt;
                padding-bottom: 6pt;
                font-family: "Arial";
                line-height: 1.15;
                page-break-after: avoid;
                orphans: 2;
                widows: 2;
                text-align: left;
            }
            h3 {
                padding-top: 16pt;
                color: #434343;
                font-size: 14pt;
                padding-bottom: 4pt;
                font-family: "Arial";
                line-height: 1.15;
                page-break-after: avoid;
                orphans: 2;
                widows: 2;
                text-align: left;
            }
            h4 {
                padding-top: 14pt;
                color: #666666;
                font-size: 12pt;
                padding-bottom: 4pt;
                font-family: "Arial";
                line-height: 1.15;
                page-break-after: avoid;
                orphans: 2;
                widows: 2;
                text-align: left;
            }
            h5 {
                padding-top: 12pt;
                color: #666666;
                font-size: 11pt;
                padding-bottom: 4pt;
                font-family: "Arial";
                line-height: 1.15;
                page-break-after: avoid;
                orphans: 2;
                widows: 2;
                text-align: left;
            }
            h6 {
                padding-top: 12pt;
                color: #666666;
                font-size: 11pt;
                padding-bottom: 4pt;
                font-family: "Arial";
                line-height: 1.15;
                page-break-after: avoid;
                font-style: italic;
                orphans: 2;
                widows: 2;
                text-align: left;
            }
        </style>
    </head>
    <body class="c3">
        <p class="c1">
            <span class="c0">
                &#1054;&#1089;&#1054;&#1054; &laquo;&#1052;&#1080;&#1082;&#1088;&#1086;&#1082;&#1088;&#1077;&#1076;&#1080;&#1090;&#1085;&#1072;&#1103; &#1082;&#1086;&#1084;&#1087;&#1072;&#1085;&#1080;&#1103;
                &laquo;&#1050;&#1077;&#1083;&#1077;&#1095;&#1077;&#1082;&raquo;&raquo;
            </span>
        </p>
        <p class="c1"><span class="c0">&#1075;.&#1041;&#1080;&#1096;&#1082;&#1077;&#1082;, &#1091;&#1083;. &#1051;&#1100;&#1074;&#1072; &#1058;&#1086;&#1083;&#1089;&#1090;&#1086;&#1075;&#1086;, 126/8</span></p>
        <p class="c1"><span class="c0">&#1048;&#1053;&#1053;: 02707202110036</span></p>
        <p class="c1"><span class="c0">&#1053;&#1086;&#1084;&#1077;&#1088; &#1057;&#1060;&#1050;&#1056;: 102000756740</span></p>
        <p class="c1"><span class="c0">&#1054;&#1050;&#1055;&#1054;: 31158290</span></p>
        <p class="c1"><span class="c0">&#1050;&#1086;&#1076; &#1059;&#1043;&#1053;&#1057;: 002</span></p>
        <p class="c1"><span class="c0">&nbsp;</span></p>
        <p class="c1"><span class="c0">&#1041;&#1072;&#1085;&#1082;&#1086;&#1074;&#1089;&#1082;&#1080;&#1077; &#1088;&#1077;&#1082;&#1074;&#1080;&#1079;&#1080;&#1090;&#1099;:</span></p>
        <p class="c1"><span class="c0">&#1041;&#1072;&#1085;&#1082;: &#1054;&#1040;&#1054; &laquo;&#1040;&#1081;&#1099;&#1083;&#1073;&#1072;&#1085;&#1082;&raquo;</span></p>
        <p class="c1"><span class="c0">&#1088;/&#1089; 1350100020023153</span></p>
        <p class="c1"><span class="c0">&#1041;&#1048;&#1050; 135001</span></p>
        <p class="c2"><span class="c0"></span></p>
    </body>
</html>
`