export const oferta = `
<html>

<head>
<meta http-equiv=Content-Type content="text/html; charset=utf-8">
<meta name=Generator content="Microsoft Word 15 (filtered)">
<style>
<!--
 /* Font Definitions */
 @font-face
	{font-family:"Cambria Math";
	panose-1:2 4 5 3 5 4 6 3 2 4;}
@font-face
	{font-family:Calibri;
	panose-1:2 15 5 2 2 2 4 3 2 4;}
@font-face
	{font-family:Georgia;
	panose-1:2 4 5 2 5 4 5 2 3 3;}
@font-face
	{font-family:"Noto Sans Symbols";}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin-top:0in;
	margin-right:0in;
	margin-bottom:8.0pt;
	margin-left:0in;
	line-height:107%;
	font-size:11.0pt;
	font-family:"Calibri",sans-serif;}
.MsoChpDefault
	{font-family:"Calibri",sans-serif;}
.MsoPapDefault
	{margin-bottom:8.0pt;
	line-height:107%;}
@page WordSection1
	{size:8.5in 11.0in;
	margin:56.7pt 42.5pt 56.7pt 85.05pt;}
div.WordSection1
	{page:WordSection1;}
 /* List Definitions */
 ol
	{margin-bottom:0in;}
ul
	{margin-bottom:0in;}
-->
</style>

</head>

<body lang=EN-US style='word-wrap:break-word'>

<div class=WordSection1>

<p class=MsoNormal align=center style='text-align:center'><b><span lang=RU
style='font-size:12.0pt;line-height:107%;font-family:"Times New Roman",serif;
color:black'>АЙКЫН ОФЕРТА</span></b></p>

<p class=MsoNormal align=center style='text-align:center'><b><span lang=RU
style='font-size:12.0pt;line-height:107%;font-family:"Times New Roman",serif'>“Cash2U” сервисине кошулуу келишимин түзүү жөнүндө</span></b></p>

<p class=MsoNormal style='text-align:justify'><span lang=RU style='font-size:
12.0pt;line-height:107%;font-family:"Times New Roman",serif'>Ушул оферта Кыргыз Республикасынын Жарандык 
кодексинин 398-беренесинин 2-пунктуна ылайык мобилдик тиркеме аркылуу “Cash2U” сервисине кошулуу жөнүндө
 жеке жактарга “МНК “Келечек” ЖЧКнын расмий сунушу болуп саналат.
</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:0in;
margin-left:.25in;text-align:justify;text-indent:-.25in;border:none'><b><span
lang=RU style='font-size:12.0pt;line-height:107%;font-family:"Times New Roman",serif;
color:black'>1.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></b><b><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Келишимдин предмети</span></b></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>1.1.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Кыргыз Республикасынын Улуттук банкы тарабынан берилген 2021-жылдын 6-августундагы №510 
Эсептик каттоо жөнүндө күбөлүктүн негизинде иш жүргүзгөн “МНК “Келечек”  ЖЧКсы (мындан ары ”МНК”) ушул айкын офертада (мындан ары “Оферта”)
 жана Мобилдик тиркемени (мындан ары “МТ”) пайдалануу аркылуу  аларга жөнөкөй электрондук кол тамга коюу жолу менен түзүлгөн башка келишимдерде 
 каралган шарттарда жеке адамдарга (мындан ары “Кардар”) онлайн насыя берүүнү кошуу менен МНКнын кызмат көрсөтүүлөрүнүн айкын сунушун жөнөтөт.</span></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>1.2.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Тараптардын ортосундагы ушул келишим ушул Офертанын кардары
 менен акцеп жолу менен түзүлөт. Мобилдик тиркемеде иденттүүлүк менен каттоодон өтүү боюнча кардар тарабынан
  жасалган аракет Офертанын акцепти деп аталат, ал Офертанын шарттарына толук жана сөзсүз макул болуу катары
   каралат. </span></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>1.3.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>МНК бир тараптуу тартипте  мобилдик тиркеменин бөлүгүндө жана ал күчүнө киргенге чейин 10 күн 
ичинде www cash2ukg дареги боюнча МНКнын сайтына аларды жарыялоо жолу менен Кардарга кабарлап, Офертага өзгөртүүлөр менен толуктоолорду киргизет.
 Офертага өзгөртүүлөр менен толуктоолордун күчүнө кирген учурдан баштап, алар Тараптар үчүн милдеттүү болуп саналат, мында эгерде Кардар мындай 
 өзгөртүүлөр менен толуктоолорго макул болбосо, ал мобилдик тиркемени пайдаланууну токтотууга милдеттенет.</span></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>1.4.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Ушул Офератанын шарттарын кабыл алган Кардар мобилдик тиркеме орнотулган мобилдик түзүлмөгө ээлик кылат
  жана мобилдик тиркемеде каттоо жүргүзүлгөн уюлдук оператордун ээси болуп саналат.</span></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>1.5.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Мобилдик тиркемеде Кардарды каттоодо Кардар ушул Офертанын курамына кирген документтин бардык жоболору, атап айтканда:</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:0in;
margin-left:.5in;text-align:justify;text-indent:-.25in;border:none'><span
lang=RU style='font-size:12.0pt;line-height:107%;font-family:"Times New Roman",serif;
color:black'>-<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Айкын оферта;</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:0in;
margin-left:.5in;text-align:justify;text-indent:-.25in;border:none'><span
lang=RU style='font-size:12.0pt;line-height:107%;font-family:"Times New Roman",serif;
color:black'>-<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif'>Айкын насыя келишим;</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:0in;
margin-left:.5in;text-align:justify;text-indent:-.25in;border:none'><span
lang=RU style='font-size:12.0pt;line-height:107%;font-family:"Times New Roman",serif;
color:black'>-<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Маалыматтарды иштеп чыгууга макулдук;</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:0in;
margin-left:.5in;text-align:justify;text-indent:-.25in;line-height:normal;
border:none'><span lang=RU style='font-size:12.0pt;font-family:"Times New Roman",serif;
color:black'>-<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;font-family:"Times New Roman",serif;
color:black'>Мобилдик тиркеменин “Документтер” бөлүмүндө жана МНК веб-сайтында турган башка документтер 
менен сөзсүз жана толук көлөмдө алып койбостон  таанышкандыгын тастыктайт, бардык милдеттерди кабыл алат 
жана көрсөтүлгөн документтерде камтылган талапты сактоого милдеттенет.</span></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>1.6.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>МТны кардардын пайдаланышы ушул оферта жана “Документтер бөлүмүндө МТда жайгаштырылган
 “Маалыматтарды иштеп чыгууга макулдугунда” көрсөтүлгөн кардардын жеке маалыматтарынын шарттары менен макул болууну билдирет.</span></p>

<p class=MsoNormal style='margin-bottom:0in;text-align:justify;border:none'><span
lang=RU style='font-size:12.0pt;line-height:107%;font-family:"Times New Roman",serif;
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:0in;
margin-left:.25in;text-align:justify;text-indent:-.25in;border:none'><b><span
lang=RU style='font-size:12.0pt;line-height:107%;font-family:"Times New Roman",serif;
color:black'>2.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></b><b><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Кызмат көрсөтүүлөрдүн ыкмалары</span></b></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>2.1.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>МНКнын кызматтары Кардардын мобилдик телефонуна орнотулган              “Cash2U” мобилдик тиркемесин пайдалануу аркылуу берилет.</span></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>2.2.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Кардар “Cash2U” мобилдик тиркемеси орнотулгандан кийин мобилдик тиркемеде МНК тарабынан
 орнотулган эрежелер менен талаптарга ылайык бардык суралган маалыматтарды көрсөтүүгө, сунуш кылынган формаларды толтурууга жана
  иденттештирүүдөн өтүүгө, ошондой эле анын жеке маалыматтарын иштеп чыгууга макулдук берүүгө милдеттүү.</span></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>2.3.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Кызматтарды көрсөтүүнүн шарттары МТнин “Документтер” бөлүмүндө
 жайгаштырылган Кызматтар паспортунда көрсөтүлөт. МТ аркылуу кызматтарды пайдаланууга Кардардын суроо-талабы 
 Кардар бардык документтер менен таанышты, МТда Кардардын генериалдык каттоодо/аутенфикациялоодо электрондук 
 сан ариптик кол тамгасы коюлган  келишимдерди, формаларды жана анкеталарды кошкондо тиешелүү документтерге 
 кол койду дегендикти билдирет.</span></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>2.4.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>МНК Кардардын электрондук арызын карайт жана МНКнын ички жобосуна ылайык 
аны жактырган учурда Кардарга сураган кызмат көрсөтүүнү сунуштайт. Мында МНК Кардар тарабынан берилген маалымат МНКнын 
ички документтеринин нормалары менен талаптарына жана/же Кыргыз Республикасынын мыйзамдарына ылайык келбеген учурда 
Кардарга сураган кызмат көрсөтүүдөн баш тартууга укуктуу.</span></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>2.5.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>“Cash2U” МТси аркылуу Кардар тарабынан жүргүзүлгөн 
бардык аракеттер юридикалык жактан маанилүү деп эсептелет жана МНКга  Кардардын колу коюлуп берилген 
келишимдерге/өтүнмөгө теңдештирилет.</span></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>2.6.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>МТ орнотулган мобилдик телефон жоголгон, аутенттик маалыматтар, логин, 
пароль же башка маанилүү маалымдамалар жоголгон учурда Кардар токтоосуз аккаунтка бөгөт коюу үчүн көрсөтүлгөн жагдайлар
 жөнүндө МНКга билдирүүгө милдеттүү. Кардар МНКга тиешелүү арыз берүү жана жаңы логинди жана/же паролду түзүү жолу менен
  кызмат көрсөтүүлөргө жетүүнү калыбына келтирүүгө укуктуу.</span></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>2.7.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Кардар күндөлүк кызмат көрсөтүүлөр жана МТнин өздүк кабинетинде кызмат көрсөтүүлөр боюнча бардык зарыл маалыматтар менен таанышууга укуктуу.</span></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>2.8.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>МТ аркылуу жөнөтүлгөн бардык арыздар МТ аркылуу МНКга аларды жөнөткөн учурдан тартып далилденди жана бүткөрүлдү (сөзсүз жана кайтарымсыз) деп эсептелет.</span></p>

<p class=MsoNormal style='margin-bottom:0in;text-align:justify;border:none'><span
lang=RU style='font-size:12.0pt;line-height:107%;font-family:"Times New Roman",serif;
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:0in;
margin-left:.25in;text-align:justify;text-indent:-.25in;border:none'><b><span
lang=RU style='font-size:12.0pt;line-height:107%;font-family:"Times New Roman",serif;
color:black'>3.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></b><b><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Тараптардын укуктары жана милдеттери</span></b></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><b><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>3.1.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></b><b><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Кардар укуктуу</span></b></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>3.1.1.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Кардардын ишке жөндөмдүүлүгүнүн чектелбегендигин, 
камкорчулукта, көзөмөлчүлүктө, ошондой эле ден соолугу боюнча кароодо турбаганын, өз укуктарын өз 
алдынча ишке ашыра аларын жана милдеттерин аткара аларын, түзүлгөн келишимдердин жана аны түзүүнүн 
жагдайларынын маңызын баамдоого жолто болуучу ооруулар менен оорубагандыгын ырастап, ушул Офертада 
каралган ыкмалар аркылуу МНК менен келишимдерди түзүүгө.</span></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>3.1.2.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Офертада каралган жана Кыргыз Республикасынын мыйзамдарында тыюу салынбаган бардык операцияларды жүргүзүүгө.</span></p>

<p class=MsoNormal style='margin-bottom:0in;text-align:justify;border:none'><span
lang=RU style='font-size:12.0pt;line-height:107%;font-family:"Times New Roman",serif;
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><b><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>3.2.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></b><b><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Кардар милдеттүү</span></b></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>3.2.1.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Офертанын шарттары, МТнин “Документтер” бөлүгүндө жайгаштырылган МНКнын
 келишимдери жана башка документтери менен өз убагында толук көлөмдө таанышууга жана аларды сактоого.</span></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>3.2.2.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>МНКнын кызматтарын пайдалануу үчүн зарыл болгон жеке
 маалыматтар жөнүндө маалымат берүүгө, алар өзгөргөн учурда жеке маалыматтар жөнүндө берилген маалыматты
 толуктоого.</span></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>3.2.3.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Ушул Офертага жана (же) башка документтерге МНК тарабынан киргизилген өзгөртүүлөргө өз алдынча байкоо жүргүзүүгө.</span></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>3.2.4.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Авторлоштурулган маалыматтарды жоготуу жөнүндө билдирүү МНК 
тарабынан Кардардан алынган учурга чейин жасалган бардык операциялар үчүн жоопкерчилик тартууга.</span></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>3.2.5.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Авторлоштурулган маалыматтарды жоготууну болтурбоо үчүн бардык
 болгон чараларды көрүүгө, үчүнчү жактардын Авторлоштурулган маалыматтарды мыйзамсыз пайдалануусуна жол бербөөгө,
  анын ичинде Кыргыз Республикасынын мыйзамдарында каралган учурларды кошпогондо, Авторлоштурулган маалыматтарды
   үчүнчү жактарга билдирүүгө.</span></p>

<p class=MsoNormal style='margin-bottom:0in;text-align:justify;border:none'><span
lang=RU style='font-size:12.0pt;line-height:107%;font-family:"Times New Roman",serif;
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><b><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>3.3.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></b><b><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>МНК укуктуу</span></b></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>3.3.1.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Кызматтарды көрсөтүү үчүн зарыл болгон маалыматты, анын ичинде жеке мүнөздөгү маалыматты Кардардан суратууга.</span></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>3.3.2.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Кардар берген маалыматты, анын ичинде мамлекеттик органдар менен мекемелерге суроо-талаптарды жөнөтүү жолу менен алууга.</span></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>3.3.3.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>МТга жана МНКнын веб-сайтына тиешелүү маалыматты жайгаштыруу жолу 
менен Кардарга билдирип, МТ аркылуу көрсөтүлүүчү кызматтардын тизмесин кеңейтүүгө, жаңыртууга, толуктоого.</span></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><a name="_heading=h.1fob9te"></a><span lang=RU style='font-size:
12.0pt;line-height:107%;font-family:"Times New Roman",serif;color:black'>3.3.4.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span
lang=RU style='font-size:12.0pt;line-height:107%;font-family:"Times New Roman",serif;
color:black'>Кардар жана/же үчүнчү жактар тарабынан укукка каршы аракеттер шектелген жана/же
 табылган учурда, ошондой эле программалык камсыз кылуу, байланыш ишинде, профилактикалык иштерди
  жүргүзүүдө жана МНКнын ички профилактикалык актыларында жана Кыргыз Республикасынын мыйзамдарында
   белгиленген учурлардан башка учурларда   Кардардын кызмат көрсөтүүлөргө киришүүсүнө бөгөт коюуга.</span></p>

<p class=MsoNormal style='margin-bottom:0in;text-align:justify;border:none'><span
lang=RU style='font-size:12.0pt;line-height:107%;font-family:"Times New Roman",serif;
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><b><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>3.4.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></b><b><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>МНК милдеттүү</span></b></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>3.4.1.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Купуя маалыматты жашыруун  сактоону камсыз кылууга,
 маалыматты Кардардын алдын ала кат жүзүндөгү уруксаты жок жайылтпоого, ошондой эле Оферта жана/же
  Кыргыз Республикасынын мыйзамдары тарабынан белгиленген учурлардан башка Кардардын берген жеке
   маалыматтарын алмашуу, жарыялоо же болбосо башка мүмкүн болгон ыкмалар менен жайылтуу  
    жүргүзбөөгө.</span></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>3.4.2.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Иштиктүү колдонуудагы маалыматты коргоо үчүн пайдаланылган
 тартипке жана Кыргыз Республикасынын мыйзамдарынын талаптарына ылайык Кардардын жеке маалыматтарынын 
 купуялуулугун коргоо үчүн сактык чараларын көрүүгө.</span></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>3.4.3.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Бейукук  же кокус жетүүдөн, талкалоодон, өзгөртүүдөн, бөгөт коюудан, 
көчүрүүдөн, таркатуудан, ошондой эле үчүнчү жактардын башка бейукук аракеттеринен кардардын/зайымчынын жеке маалыматын
 коргоо үчүн зарыл уюштуруучулук жана техникалык чараларды көрүүгө.</span></p>

<p class=MsoNormal style='margin-bottom:0in;text-align:justify;border:none'><span
lang=RU style='font-size:12.0pt;line-height:107%;font-family:"Times New Roman",serif;
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:0in;
margin-left:.25in;text-align:justify;text-indent:-.25in;border:none'><b><span
lang=RU style='font-size:12.0pt;line-height:107%;font-family:"Times New Roman",serif;
color:black'>4.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></b><b><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Тараптардын жоопкерчилиги</span></b></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>4.1.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Оферта боюнча милдеттерди аткарбагандыгы жана талаптагыдай эмес аткаргандыгы
 үчүн тараптар Кыргыз Республикасынын мыйзамдарына ылайык жоопкерчилик тартышат.</span></p>


<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>4.2.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Арызда көрсөтүлгөн мобилдик телефондун номери/электрондук почтанын дареги, 
логин менен паролдун ишенимдүүлүгү үчүн Кардар өз алдынча жоопкерчилик тартат. Көрсөтүлгөн мобилдик телефондун номери/электрондук
 почтанын дареги, логин менен пароль ишенимдүү болбогон учурда, МНКга байланыштуу болбогон шарттар менен чектелген башка учурларда
  (билдирүү уюлдук оператору тарабынан жөнөтүлбөгөндө, интернет провайдер тарабынан жеке адамдын мобилдик телефонуна/электорондук 
    почтасынын дарегине бөгөт коюлганда ж.б) МНК эч кандай жооп бербейт.</span></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>4.3.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Кардар МТнин кызматтарын пайдалануунун жана алар тарабынан ошол операцияларды 
аткаруунун натыйжасында келип чыккан кесепеттердин тобокелдигине өзү жооптуу болот.</span></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>4.4.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>МНК Кардардын бардык жоготуулары, анын ичинде Кардардын офертага жана (же) документтерге киргизилген
 өзгөртүүлөр жана толуктоолор менен офертанын жана документтердин шарттары менен таанышпагандыгы жана өз убагында таанышпагандыгы үчүн жооп бербейт.</span></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>4.5.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>МНК эгерде Кардар тарабынан жеке маалыматтар, логин жана пароль атайылап//атайылабай  берилсе/жоголсо жооп бербейт.</span></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>4.6.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Офертаны аткарбагандыктын жана тиешелүү эмес аткаргандыктын натыйжасында Кардар тарабынан келтирилген зыян МНКнын Кардары тарабынан сөзсүз аткарылууга тийиш.</span></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>4.7.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Тараптар жеңүүгө мүмкүн болгон жагдайлар (форс-мажор) жаралган 
учурда ушул Оферта боюнча милдеттерин айрым же толук аткарбагандыгы үчүн жоопкерчиликтен бошотулат, ага төмөнкүлөр
 кирет: табигый кырсыктар, өрт, суу ташкыны, аскердик аракеттер, ушул келишимде көрсөтүлгөн ишмердүүлүктүн түрүн
  же ушул Оферта боюнча өздөрүнүн милдеттерин Тараптардын аткаруусуна тоскоолдук кылган, башка жагдайлардан 
  улам жаралган Тараптардын акыл-эстен тышкаркы контролдоосуна түз же кыйыр тыюу салган, тараптардын 
  бири аткарууга милдеттүү болгон, мыйзам актыларынын, бийлик жана башкаруу органдарынын актыларынын 
  күчүнө кириши.</span></p>

<p class=MsoNormal style='margin-bottom:0in;text-align:justify;border:none'><span
lang=RU style='font-size:12.0pt;line-height:107%;font-family:"Times New Roman",serif;
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:0in;
margin-left:.25in;text-align:justify;text-indent:-.25in;border:none'><b><span
lang=RU style='font-size:12.0pt;line-height:107%;font-family:"Times New Roman",serif;
color:black'>5.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></b><b><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Талаштарды чечүү</span></b></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>5.1.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Кардар менен МНКнын ортосундагы мамилелерден 
улам келип чыккан талаштар пайда болгон учурда сотко чейин талашты жөнгө салуу тартиби Тараптар
 үчүн милдеттүү болуп саналат. Тараптар МТда жана каттоо кезинде Кардар тарабынан берилген 
 маалыматтарда  көрсөтүлгөн реквизиттер боюнча дооматтарды (талашты ыктыярдуу жөнгө салуу
     жөнүндө кат жүзүндөгү сунуштар) сот органдарына кайрылганга чейин жөнөтүүгө милдеттенишет. 
     Дооматты алган Тарап 15 күндүн ичинде дооматка жүйөлүү жооп берүүгө милдеттүү.</span></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>5.2.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Тараптар макулдашууга жетишпегенде ушул Офертадан жана келишимдерден же аларга байланыштуу пайда болгон талаштар, пикир келишпестиктер же талаптар Кыргыз Республикасынын мыйзамдарына ылайык МНК турган жердеги сот органдарында каралат.</span></p>

<p class=MsoNormal style='margin-bottom:0in;text-align:justify;border:none'><span
lang=RU style='font-size:12.0pt;line-height:107%;font-family:"Times New Roman",serif;
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:0in;
margin-left:.25in;text-align:justify;text-indent:-.25in;border:none'><b><span
lang=RU style='font-size:12.0pt;line-height:107%;font-family:"Times New Roman",serif;
color:black'>6.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></b><b><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Башка шарттар</span></b></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>6.1.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Ушул Келишим ушул Офертанын кардары менен акцептин датасынан тартып түзүлдү деп эсептелет жана ушул Келишим боюнча милдеттерин толук аткарганга чейин колдонулат.</span></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>6.2.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Ушул Оферта аны МНК расмий чакыртып алган учурга чейин колдонулат. Оферта расмий чакыртылып алган учурда бул тууралуу маалымат МНКнын сайтына жана МТга жайгаштырылат.</span></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>6.3.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Ушул офертада каралбаган бардык учурларда, тараптар Кыргыз Республикасынын мыйзамдарын жетекчиликке алат.</span></p>

<p class=MsoNormal style='text-align:justify;border:none'><span lang=RU
style='font-size:12.0pt;line-height:107%;font-family:"Times New Roman",serif;
color:black'>&nbsp;</span></p>

</div>

</body>

</html>

`
