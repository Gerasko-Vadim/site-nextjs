export const consent =`
<html>

<head>
<meta http-equiv=Content-Type content="text/html; charset=utf-8">
<meta name=Generator content="Microsoft Word 15 (filtered)">
<style>
<!--
 /* Font Definitions */
 @font-face
	{font-family:"Cambria Math";
	panose-1:2 4 5 3 5 4 6 3 2 4;}
@font-face
	{font-family:Calibri;
	panose-1:2 15 5 2 2 2 4 3 2 4;}
@font-face
	{font-family:Georgia;
	panose-1:2 4 5 2 5 4 5 2 3 3;}
@font-face
	{font-family:"Noto Sans Symbols";}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin-top:0in;
	margin-right:0in;
	margin-bottom:8.0pt;
	margin-left:0in;
	line-height:107%;
	font-size:11.0pt;
	font-family:"Calibri",sans-serif;}
.MsoChpDefault
	{font-family:"Calibri",sans-serif;}
.MsoPapDefault
	{margin-bottom:8.0pt;
	line-height:107%;}
@page WordSection1
	{size:8.5in 11.0in;
	margin:56.7pt 42.5pt 56.7pt 85.05pt;}
div.WordSection1
	{page:WordSection1;}
 /* List Definitions */
 ol
	{margin-bottom:0in;}
ul
	{margin-bottom:0in;}
-->
</style>

</head>

<body lang=EN-US style='word-wrap:break-word'>

<div class=WordSection1>

<p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
line-height:normal'><b><span lang=RU style='font-size:12.0pt;font-family:"Times New Roman",serif;
color:black'> Жеке маалыматтар субъектисинин анын жеке маалыматтарын </span></b></p>

<p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
line-height:normal'><b><span lang=RU style='font-size:12.0pt;font-family:"Times New Roman",serif;
color:black'>чогултууга жана иштеп чыгууга макулдугу</span></b></p>

<p class=MsoNormal style='text-align:justify'><b><span lang=RU
style='font-size:12.0pt;line-height:107%;font-family:"Times New Roman",serif'>&nbsp;</span></b></p>

<p class=MsoNormal style='margin-bottom:0in;text-align:justify'><span lang=RU
style='font-size:12.0pt;line-height:107%;    word-wrap: break-word; font-family:"Times New Roman",serif'>Мен,
_______________________________________________________________________________________________</span></p>

<p class=MsoNormal style='text-align:justify'><span lang=RU style='font-size:
12.0pt;line-height:107%;font-family:"Times New Roman",serif'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
(субъекттин фамилиясы, аты, атасынын аты)</span></p>

<p class=MsoNormal style='text-align:justify'><span lang=RU style='font-size:
12.0pt;line-height:107%; word-wrap: break-word; font-family:"Times New Roman",serif'>
Кыргыз Республикасында ыйгарылган ЖПН: _________________________________________________________</span></p>

<p class=MsoNormal style='margin-bottom:0in;text-align:justify'><span lang=RU
style='font-size:12.0pt;line-height:107%;font-family:"Times New Roman",serif'>Өздүгүн тастыктаган документ: _________________ серия ___________ № ____________________________</span></p>

<p class=MsoNormal style='text-align:justify'><span lang=RU style='font-size:
12.0pt;line-height:107%;font-family:"Times New Roman",serif'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
 (документтин түрү)</span></p>

<p class=MsoNormal style='margin-bottom:0in;text-align:justify'><span lang=RU
style='font-size:12.0pt;line-height:107%;    word-wrap: break-word;font-family:"Times New Roman",serif'>берилген:
____________________________________________________________________________________________</span></p>

<p class=MsoNormal style='text-align:justify'><span lang=RU style='font-size:
12.0pt;line-height:107%;font-family:"Times New Roman",serif'>&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
(берилген датасы, берген органдын аталышы)</span></p>

<p class=MsoNormal style='text-align:justify'><span lang=RU style='font-size:
12.0pt;line-height:107%;word-wrap: break-word; font-family:"Times New Roman",serif'>Анык жашаган дареги: _____________________________________________________________________</span></p>

<p class=MsoNormal style='text-align:justify'><span lang=RU style='font-size:
12.0pt;line-height:107%;    word-wrap: break-word; font-family:"Times New Roman",serif' >__________________________________________________________________________________________________</span></p>

<p class=MsoNormal style='text-align:justify'><span lang=RU style='font-size:
12.0pt;line-height:107%;word-wrap: break-word; font-family:"Times New Roman",serif'word-wrap: break-word;>Катталган жеринин дареги: ______________________________________________________________________________</span></p>

<p class=MsoNormal style='text-align:justify'><span lang=RU style='font-size:
12.0pt;
    overflow-wrap: break-word;
line-height:107%;font-family:"Times New Roman",serif'>___________________________________________________________________________________________________</span></p>

<p class=MsoNormal style='text-align:justify'><span lang=RU style='font-size:
12.0pt;line-height:107%;font-family:"Times New Roman",serif'>&nbsp;</span></p>

<p class=MsoNormal style='text-align:justify'><span lang=RU style='font-size:
12.0pt;line-height:107%;word-wrap: break-word; font-family:"Times New Roman",serif'>Байланыш телефону: __________________________ эл.почта ______________________________________________</span></p>

<p class=MsoNormal style='text-align:justify'><span lang=RU style='font-size:
12.0pt;line-height:107%;font-family:"Times New Roman",serif'>&nbsp;</span></p>

<p class=MsoNormal style='text-align:justify'><span lang=RU style='font-size:
12.0pt;line-height:107%;font-family:"Times New Roman",serif'>Кыргыз Республикасынын Улуттук банкы 
тарабынан берилген 2021-жылдын 6-августундагы №510 Эсептик каттоо жөнүндө күбөлүктүн негизинде иш 
жүргүзгөн “МНК “Келечек” ЖЧКага (мындан ары – МНК) макулдугумду берем:
</span><span
lang=RU style='font-size:12.0pt;line-height:107%;font-family:"Times New Roman",serif'>:</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:0in;
margin-left:.5in;text-align:justify;text-indent:-.25in;border:none'><span
lang=RU style='font-size:12.0pt;line-height:107%;font-family:"Noto Sans Symbols";
color:black'>✔<span style='font:7.0pt "Times New Roman"'> </span></span><span
lang=RU style='font-size:12.0pt;line-height:107%;font-family:"Times New Roman",serif;
color:black'>менин жеке маалыматтарымды (чогултуу, системалаштыруу, топтоо, жаздыруу, сактоо, 
	актуалдаштыруу ( жаңыртуу, өзгөртүү), топко бөлүү, бөгөт коюу) автоматтык каражаттар менен же аларсыз иштеп чыгууга;</span></p>

<p class=MsoNormal style='margin-left:.5in;text-align:justify;text-indent:-.25in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Noto Sans Symbols";color:black'>✔<span style='font:7.0pt "Times New Roman"'> </span></span><span
lang=RU style='font-size:12.0pt;line-height:107%;font-family:"Times New Roman",serif;
color:black'>менин жеке маалыматтарымды “Жеке мүнөздөгү маалымат жөнүндө” Кыргыз Республикасынын 
Мыйзамына жана эл аралык келишимдерге ылайык үчүнчү жактарга өткөрүп берүүгө, менин жеке маалыматтарымды 
башка өлкөлөрдүн юрисдикциясында туруп кармагандарга транс чек аралык өткөрүп берүүгө,</span></p>

<p class=MsoNormal style='text-align:justify'><span lang=RU style='font-size:
12.0pt;line-height:107%;font-family:"Times New Roman",serif'>ушуга байланыштуу:</span></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>1.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Ушул менен менин жеке маалыматтарымды кошуп,
 бирок чектебестен, МНКга иштеп чыгууга, менин атымдан жана/же бардык үчүнчү жактардын, анын
  ичинде:</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:0in;
margin-left:49.65pt;text-align:justify;text-indent:-21.3pt;border:none'><span
lang=RU style='font-size:12.0pt;line-height:107%;font-family:"Times New Roman",serif;
color:black'>-<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Кыргыз Республикасынын Министрлер Кабинетине караштуу Мамлекеттик салык кызматынын;</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:0in;
margin-left:49.65pt;text-align:justify;text-indent:-21.3pt;border:none'><span
lang=RU style='font-size:12.0pt;line-height:107%;font-family:"Times New Roman",serif;
color:black'>-<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Кыргыз Республикасынын Социалдык Фондунун;
</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:0in;
margin-left:49.65pt;text-align:justify;text-indent:-21.3pt;border:none'><span
lang=RU style='font-size:12.0pt;line-height:107%;font-family:"Times New Roman",serif;
color:black'>-<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Кыргыз Республикасынын Министрлер Кабинетине караштуу Мамлекеттик каттоо кызматынын;
</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:0in;
margin-left:49.65pt;text-align:justify;text-indent:-21.3pt;border:none'><span
lang=RU style='font-size:12.0pt;line-height:107%;font-family:"Times New Roman",serif;
color:black'>-<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Кыргыз Республикасынын Министрлер Кабинетине караштуу Мамлекеттик каттоо кызматынын алдындагы “Унаа” мамлекеттик мекемесинин;
</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:0in;
margin-left:49.65pt;text-align:justify;text-indent:-21.3pt;border:none'><a
name="_heading=h.gjdgxs"></a><span lang=RU style='font-size:12.0pt;line-height:
107%;font-family:"Times New Roman",serif;color:black'>-<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Кыргыз Республикасынын Министрлер Кабинетине караштуу Мамлекеттик каттоо кызматынын алдындагы Кадастр жана кыймылсыз мүлккө укуктарды каттоо департаментинин;</span></p>

<p class=MsoNormal style='margin-bottom:0in;text-align:justify;border:none'><span
lang=RU style='font-size:12.0pt;line-height:107%;font-family:"Times New Roman",serif;
color:black'>ошондой эле менин жеке маалыматтарымды Кыргыз Республикасынын мыйзамдарынын талаптарына ылайык иш жүргүзгөн бардык үчүнчү жактардын алуусуна өзүмдүн макулдугумду берем.</span></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>2.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Ушул макулдук “Cash2U” Мобилдик тиркемесинин “Документтер” 
бөлүмүндө жана www. cash2u.kg, дарегиндеги МНКнын расмий веб-сайтында жайгашкан МНКнын Офертасын, ошондой 
эле менин жана МНКнын ортосунда түзүлгөн башка Келишимдерди (мындан ары – “Келишим”) аткаруу максатында жана 
Оферта менен Келишимге ылайык МНКнын өз функцияларын ишке ашыруусу үчүн гана берилет, макулдук төмөнкүдөй 
маалыматка жайылтылат: фамилиясы, аты, атасынын аты, төрөлгөн жылы, айы, датасы жана жери, катталган жана 
жашаган дареги, өздүгүн тастыктаган документтин маалыматтары, электрондук почтанын дареги, мобилдик телефондун 
номуру, “Cash2U” сервис аркылуу ишке ашырылган операциялар жөнүндө маалыматтар (зарыл болгондо), билими жөнүндө 
маалыматтар, салык төлөөчүнүн идентификациялык номуру, соттолгондугунун бар же жок экендиги тууралуу маалыматтар 
жана МНКнын (мындан ары – Жеке маалыматтар) мезгилдин бардык конкреттүү учурундагы башка жеткиликтүү же болбосо 
белгилүү маалымат.</span></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>3.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Ушул макулдаштык мен тарабынан бул 
документке электрондук формада берилген (жөнөкөй электрондук кол коюлган) датадан  жана  
Оферта жана МНК менен түзүлгөн келишимдер боюнча менин өз милдеттеримди толук аткарганыма 
чейин, ошондой эле Кыргыз Республикасынын мыйзамдарына ылайык көрсөтүлгөн кызматтар жөнүндө 
маалыматтардын сакталыш мөөнөтүнө чейин иштейт.</span></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>4.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Оферта жана МНК менен түзүлгөн башка Келишимдер 
боюнча милдеттер толук аткарылгандан кийин гана, эркин формада МНКнын тиешелүү кат жүзүндөгү 
билдирүүсүн жөнөтүү жолу менен чакыртып алууга макулдук берилиши мүмкүн, мында “Жеке мүнөздөгү 
маалымат жөнүндө” Кыргыз Республикасынын Мыйзамынын 5 жана 15-беренелерине ылайык жеке маалыматтарды 
иштеп чыгуу толук же айрым улантылышы мүмкүн.</span></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>5.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Ушул менен МНКга Офертаны, келишимдерди аткаруу 
максаттары үчүн зарыл же керек болгон менин жеке маалыматтарыма карата бардык аракеттерди ишке
 ашырууга жана Офертада көрсөтүлгөн кызматтарды көрсөтүү боюнча функцияларды МНКнын ишке ашыруусуна, 
 ошондой эле жаңы продукттар менен кызматтарды МНКнын иштеп чыгууларына, ошондой эле  “Жеке мүнөздөгү
  маалымат жөнүндө” Кыргыз Республикасынын Мыйзамынын жоболорун эске алып, менин Жеке маалыматтарым менен 
  бардык башка иш-аракеттерди жүргүзүүгө өзүмдүн макулдугумду берем.</span></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>6.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Мен жогоруда баяндалган бардык маалыматтын анык экендигин 
бышыктайм, ошондой эле “Насыя маалыматын колдонуучулар ассоциациясынын” жана “Ишеним” Насыя-маалымат бюросунун”
 ( текст боюнча – Насыя бюросу) ОЮЛда мен жөнүндө маалыматтарды алууга жана/же берүүгө макулдугумду берем жана 
 доомат келтирбейм, ошону менен катар Жүрүм-турум кодексин сактоо боюнча Комитеттин мүчөлөрүнүн жана талашты 
 кароодо жогоруда көрсөтүлгөн Комитеттин отурумуна катышкан башка адамдардын мен жөнүндө жеке маалыматтар менен
  маалымдарды иштеп чыгуусуна МНКга өз макулдугумду берем. Ушул маалымдарга Жеке маалыматтардан тышкары төмөнкү
   маалыматтар да тиешелүү болушу мүмкүн: МНКда мен тарабынан алынган бизнес жүргүзгөн дарек жана бардык мурдагы
    даректер, иштин түрү, насыялар боюнча маалымдар (насыянын түрү, насыянын суммасы, субъекттин ролу, арыз берген
		 дата, төлөмдөрдүн таржымалы жөнүндө толук маалымдар, жоюлбаган калдык, дефолттун суммасы жана ушул эсеп
		  боюнча карыздын жалпы суммасы, кепилдик берүүчүлөр/гаранттар, күрөө коюучулар жана башка адамдар жөнүндө 
		  маалымдар, аларда маалымат Жөнөтүүчүлөрдүн алдында милдеттери калганбы же барбы жана башка маалымдар), 
		  мага карата көрүлгөн  чаралар жөнүндө маалымдар.</span></p>

<p class=MsoNormal style='margin-top:12.0pt;margin-right:0in;margin-bottom:
12.0pt;margin-left:0in;text-align:justify;text-indent:0in'><span lang=RU
style='font-size:12.0pt;line-height:107%;font-family:"Times New Roman",serif'>7.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif'>Мен ушул анкетада көрсөтүлгөн маалыматтардын анык экендигин бышыктайм жана “Жеке мүнөздөгү маалымат жөнүндө”
 Кыргыз Республикасынын Мыйзамынын талаптарына ылайык террордук иштерди каржылоого жана кылмыштык кирешелерди адалдаштырууга (изин жашырууга) каршы 
 аракеттенүү чөйрөсүндө Кыргыз Республикасынын мыйзамдарынын талаптарын  аткаруу максатында жеке маалыматтарды иштеп чыгууга макулдук берем.</span></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>8.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Мен “Жеке мүнөздөгү маалымат жөнүндө” Кыргыз Республикасынын Мыйзамынын жоболору, Кыргыз Республикасынын Өкмөтүнүн 2017-жылдын
 21-ноябрындагы №759 токтому менен бекитилген Өзүнүн жеке маалыматтарын чогултууга жана иштеп чыгууга жеке маалыматтар субъектисинин макулдугун алуунун тартиби, өздөрүнүн жеке маалыматтарын үчүнчү тарапка өткөрүп берүү жөнүндө жеке маалыматтар субъектилеринин кабарландырууларынын тартиби жана формасы менен таанышып чыккандыгымды бышыктайм.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>9.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'“Cash2U” тиркемесинде авторлоштуруу үчүн логин менен паролдун ачкычы болуп саналган 
жөнөкөй электрондук кол тамга коюлган электрондук документ формасында берилген ушул макулдук бардык жеке маалыматтарга жайылтылат жана
кандайдыр-бир чектөөлөрсүз/өзгөчөлүктөрсүз кагазга өз колум менен койгон кол тамга жеке маалыматтарды берүү, иштеп чыгуу, өткөрүп берүү 
менин макулдугума  тең (бирдей) болуп саналат.</span></p>

</div>

</body>

</html>
`