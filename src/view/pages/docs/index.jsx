import React from "react"
import { useEffect } from "react";
import { useTranslation } from 'react-i18next';
import { useLocation } from 'react-router';

import Container from "../../containers/container";
import DocsContainer from "../../containers/docs-container";


import "./style.scss"


const Docs = () => {
    const { t, i18n } = useTranslation()
    useEffect(() => {
        window.scrollTo(0, 0)
    }, [])
    const { pathname } = useLocation()
    const lang = localStorage.getItem("lang")
    return (
        <div className="docs-page">
            {
                pathname.split('/').includes('kg') || lang == "kg"
                    ?
                    <Container>
                        <div className="docs-page__block-title">
                            <h2 className="docs-page__title">{t("docs.title")}</h2>
                        </div>
                        <DocsContainer lang={'kg'} />
                    </Container>
                    :
                    <Container>
                        <div className="docs-page__block-title">
                            <h2 className="docs-page__title">{t("docs.title")}</h2>
                        </div>
                        <DocsContainer lang={'ru'} />
                    </Container>

            }

        </div>
    )
}
export default Docs