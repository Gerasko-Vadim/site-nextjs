import React, { useEffect } from "react"
import { useDispatch } from "react-redux";
import BecomePartner from "../../components/become-a-partner";
import Benefit from "../../components/benefit";
import BrendKg from "../../components/brend-kg";
import FeedbackForm from "../../components/feedback-form";
import HowItWorks from "../../components/howItWorks";
import Interest from "../../components/interest";
import "./style.scss"

const Business = ()=>{
    const dispatch = useDispatch()
    useEffect(()=>{
        dispatch({type:"SET_PARTNER"})
    },[])

    return(
        <div>
            <BecomePartner/>
            <BrendKg/>
            <HowItWorks/>
            <Interest/>
            <Benefit/>
            <FeedbackForm/>
        </div>
    )
}
export default Business;