import React from "react"
import { files } from "../../../files";
import {  filesKgList } from '../../../filesKg';
import { DocItem } from "../../components/doc-item";
import "./style.scss"

const DocsContainer = ({ items, lang }) => {
    return (
        <div className="doc-wrapper">
            {
                lang == "kg"
                    ?
                    <>
                        {
                            filesKgList.map((item) => <DocItem lang={lang} pathname={item.path} name={item.name} />)
                        }
                    </>
                    :
                    <>
                        {
                            files.map((item) => <DocItem lang={lang} pathname={item.path} name={item.name} />)
                        }
                    </>
            }



        </div>
    )
}
export default DocsContainer;