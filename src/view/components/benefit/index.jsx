import React from "react"

import "./style.scss"
import qr from "../../../assets/img/become-partner/qr-code.png"
import Container from "../../containers/container"
import { useTranslation } from 'react-i18next'




const Benefit = () => {
    const { t, i18n } = useTranslation()
    return (
        <div className="benefit" id="benefit">
            <Container>
                <div className="benefit__block-title">
                    <span className="benefit__title">
                        {t("partner-page.block5.title")}
                    </span>
                </div>
                <div className="benefit__wrapper">
                    <div className="benefit__card">
                        <img alt="qr" src={qr} />
                        <span className="benefit__card-description">
                            {t("partner-page.block5.1")}
                        </span>
                    </div>
                    <div className="benefit__card">
                        <img alt="qr" src={qr} />
                        <span className="benefit__card-description">
                            {t("partner-page.block5.2")}
                        </span>
                    </div>
                    <div className="benefit__card">
                        <img alt="qr" src={qr} />
                        <span className="benefit__card-description">
                            {t("partner-page.block5.3")}
                        </span>
                    </div>
                    <div className="benefit__card">
                        <img alt="qr" src={qr} />
                        <span className="benefit__card-description">
                            {t("partner-page.block5.4")}
                        </span>
                    </div>
                    <div className="benefit__card">
                        <img alt="qr" src={qr} />
                        <span className="benefit__card-description">
                            {t("partner-page.block5.5")}
                        </span>
                    </div>
                    <div className="benefit__card">
                        <img alt="qr" src={qr} />
                        <span className="benefit__card-description">
                            {t("partner-page.block5.6")}
                        </span>
                    </div>
                    <div className="benefit__card">
                        <img alt="qr" src={qr} />
                        <span className="benefit__card-description">
                            {t("partner-page.block5.7")}
                        </span>
                    </div>
                    <div className="benefit__card">
                        <img alt="qr" src={qr} />
                        <span className="benefit__card-description">
                            {t("partner-page.block5.8")}
                        </span>
                    </div>
                </div>

            </Container>
        </div>
    )
}

export default Benefit;