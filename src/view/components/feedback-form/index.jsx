import React, { useState } from "react"
import emailjs from 'emailjs-com';

import { Button, Form } from "react-bootstrap";
import Container from "../../containers/container";

import "./style.scss"
import { toast, ToastContainer } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import { useTranslation } from 'react-i18next';
import { useDispatch } from 'react-redux';

// "form": {
//     "title": "Заполните заявку и станьте партнером!",
//         "subtitle": "Общая информация",
//             "input1": "Контактное имя",
//                 "input2": "Мобильный телефон",
//                     "input3": "Название магазина (Бренда)",
//                         "checkbox": "Выражаю согласие на обработку персональных данных и подтверждаю,что ознакомлен с Политикой обработки персональных данных",
//                             "button": "Отправить"
// }

const FeedbackForm = () => {
    const { t, i18n } = useTranslation()
    const dispatch = useDispatch()
    const [name,setName] = useState('')
    const [phone, setPhone] = useState('')
    const [brand_name, setBrend] = useState('')


    const onSubmit = async (e) => {
        e.preventDefault()
        await fetch('https://stage.c2u.io:5005/referrals/create', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            redirect: 'follow',
            body: JSON.stringify({
                name,
                phone,
                brand_name,
                "referral_code": sessionStorage.getItem('referal') || ''
            })
        })
            .then(res => {
                toast.success("Заявка отправлена успешно !!")
                setName('')
                setPhone('')
                setBrend('')
            })
            .catch(err => toast.error("Сообщение не отправлено :("))
        // emailjs.sendForm('service_77fxlhd', 'template_rzr0psk', e.target, 'user_kVb54jMRjyVMOolEvXPco')
        //     .then((result) => {
        //         toast.success("Заявка отправлена успешно !!")
        //     }, (error) => {
        //         toast.error("Сообщение не отправлено :(")
        //     });

    }

    return (
        <div className="feedback-form" id="feedback-form">
            <ToastContainer
                position="top-right"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
            />
            <Container>
                <div className="feedback-form__wrapper">
                    <span className="feedback-form__title">{t("partner-page.form.title")}</span>
                    <span className="feedback-form__description">{t("partner-page.form.subtitle")}</span>
                    <Form style={{ width: "100%" }} onSubmit={onSubmit}>
                        <Form.Group className="mb-4 input" >
                            <Form.Control className="feedback-form__inputs" value={name} onChange={(e)=> setName(e.target.value)}  required={true} name="contact_name" size="lg" type="text" placeholder={t("partner-page.form.input1")}/>
                        </Form.Group>
                        <Form.Group className="mb-4 input" >
                            <Form.Control className="feedback-form__inputs" value={phone} onChange={(e) => setPhone(e.target.value)} required={true}  name="phone_number" size="lg" type="number" placeholder={t("partner-page.form.input2")} />
                        </Form.Group>
                        <Form.Group className="mb-4 input" >
                            <Form.Control className="feedback-form__inputs" value={brand_name} onChange={(e) => setBrend(e.target.value)} required={true} name="name_shop" size="lg" type="text" placeholder={t("partner-page.form.input3")} />
                        </Form.Group>
                        {/* <div className="questionnaire__form-checkbox-block">
                            <input type="checkbox" id="check1" required className="checkbox" />
                            <label htmlFor="check1" className="check-text">
                            </label>
                            <span> {t("partner-page.form.checkbox")}</span>
                        </div> */}
                        <div className="feedback-form__blockBtn">
                            <Button className="btn-submit" type="submit" >
                                {t("partner-page.form.button")}
                            </Button>
                        </div>
                    </Form>
                </div>
            </Container>

        </div>
    )
}
export default FeedbackForm;