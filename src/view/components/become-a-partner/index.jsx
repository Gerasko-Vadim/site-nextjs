import React from "react"

import "./style.scss"
import phones from "../../../assets/img/become-partner/phones.png"
import Container from "../../containers/container"
import { useTranslation } from 'react-i18next'

const BecomePartner = () => {
    const { t, i18n } = useTranslation();
    return (
        <div className="become-a-partner">
            <Container>
                <div className="become-a-partner__wrapper">
                    <div className="become-a-partner__content">
                        <span className="become-a-partner__title">
                            {t("partner-page.block1.title")}
                        </span>
                        <span className="become-a-partner__description">{t("partner-page.block1.subtitle")}</span>
                        <a href="#feedback-form" className="become-a-partner__btn">
                            {t("partner-page.block1.button")}
                        </a>
                    </div>
                    <div className="become-a-partner__blockImg">
                        <div className="become-a-partner__elips" />
                        <img alt="phone" src={phones} />
                    </div>
                </div>
            </Container>

        </div>
    )
}
export default BecomePartner;