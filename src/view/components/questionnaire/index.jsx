
import React from "react"

import 'bootstrap/dist/css/bootstrap.min.css';
import "./style.scss"
import { Form, Button } from "react-bootstrap";
import Container from "../../containers/container";
import { useTranslation } from 'react-i18next';

// "block10": {
//     "title": "Заполнив анкету вы сможете:",
//         "subtitle": "Получать уведомления о специальных акциях и бонусах только для пользователей приложения «cash2u». Поэтому если Вам интересно скорее заполняй анкету и получай доступ к лучшим акция и бонусам",
//             "formTitle": "Узнать о акциях и бонусах сейчас!",
//                 "placeholder": "Электронная почта",
//                     "checkbox": "Выражаю согласие на обработку персональных данных и подтверждаю,что ознакомлен с Политикой обработки персональных данных",
//                         "button": "Отправить"
// },


const Questionnaire = () => {
    const { t, i18n } = useTranslation();
    return (
        <div className="questionnaire" id="questionnaire">
            <Container>
                <div className="questionnaire__wrapper">
                    <span className="questionnaire__title">{t("block10.title")}</span>
                    <span className="questionnaire__description">
                        {t("block10.subtitle")}
                    </span>
                    <div className="questionnaire__form">
                        <span className="questionnaire__form-title">{t("block10.formTitle")}</span>
                        <Form style={{ width: "100%" }}>
                            <Form.Group className="mb-3" controlId="formBasicEmail">
                                <Form.Control style={{
                                    border: "1px solid #4F4F4F",
                                    background: "#FFF9F9"
                                }} size="lg" type="email" placeholder={t("block10.placeholder")} />
                            </Form.Group>
                            <div className="questionnaire__form-checkbox-block">
                                <input type="checkbox" id="check1" required={true} className="checkbox" />
                                <label htmlFor="check1" className="check-text">

                                </label>
                                <span> {t("block10.checkbox")}</span>
                            </div>


                            <Button className="btn-submit" type="submit" >
                                {t("block10.button")}
                            </Button>
                        </Form>

                    </div>
                </div>
            </Container>
        </div>
    )
}

export default Questionnaire;