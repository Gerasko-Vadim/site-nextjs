import React from "react"
import "./style.scss"
import img1 from "../../../../assets/img/galery/1.png"
import img2 from "../../../../assets/img/galery/2.png"
import img3 from "../../../../assets/img/galery/3.png"
import img4 from "../../../../assets/img/galery/4.png"
import img5 from "../../../../assets/img/galery/5.png"
import img6 from "../../../../assets/img/galery/6.png"

import icon1 from "../../../../assets/img/galery/icons/umbro.svg"
import icon2 from "../../../../assets/img/galery/icons/colla.svg"
import icon3 from "../../../../assets/img/galery/icons/puma.svg"
import icon4 from "../../../../assets/img/galery/icons/walmart.svg"
import icon5 from "../../../../assets/img/galery/icons/boss.svg"
import icon6 from "../../../../assets/img/galery/icons/movistar.svg"

const arrImages = [
    { image: img1, icon: icon1 },
    { image: img2, icon: icon2 },
    { image: img3, icon: icon3 },
    { image: img4, icon: icon4 },
    { image: img5, icon: icon5 },
    { image: img6, icon: icon6 },
    { image: img1, icon: icon1 },
    { image: img2, icon: icon2 },
]


const Galery = () => {
    return (
        <div className="galery">
            {
                arrImages.map((item, index) => {
                    return (
                        <div className="galery__card" key={index}>
                            <img src={item.image} className="galery__card-img"/>
                        </div>
                    )
                })
            }


        </div>
    )
}
export default Galery;