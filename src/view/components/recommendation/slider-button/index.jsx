import React from "react"
import Slider from "react-slick"
import { AiOutlineArrowLeft, AiOutlineArrowRight } from 'react-icons/ai'
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import next from "../../../../assets/img/slider/next.svg"
import prev from "../../../../assets/img/slider/prev.svg"
import "./style.scss"
import { useTranslation } from 'react-i18next';


const SampleNextArrow = (props) => {
  const { className, style, onClick } = props;
  return (
    <AiOutlineArrowRight
      className={className}
      style={{
        ...style,
        height: "32px",
        width: "32px",
        color: "black"
      }}
      onClick={onClick} />
  )
}

const SamplePrevArrow = (props) => {
  const { className, style, onClick } = props;
  return (
    <AiOutlineArrowLeft
      className={className}
      style={{
        ...style,
        height: "32px",
        width: "32px",
        color: "black"
      }}
      onClick={onClick} />

  )
}


const SliderButton = () => {
  const { t, i18n } = useTranslation();

  const settings = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 1,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 3,

        }
      },
      {
        breakpoint: 900,
        settings: {
          slidesToShow: 2,

        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
        }
      }
    ]
  };

  return (
    <div className="slider">
      <Slider {...settings}>
        <button className="slide-btn" >{t("block5.category.1")}</button>
        <button className="slide-btn">{t("block5.category.2")}</button>
        <button className="slide-btn">{t("block5.category.3")}</button>
        <button className="slide-btn">{t("block5.category.4")}</button>
        <button className="slide-btn" >{t("block5.category.5")}</button>
        <button className="slide-btn">{t("block5.category.6")}</button>
        <button className="slide-btn">{t("block5.category.7")}</button>
        <button className="slide-btn">{t("block5.category.8")}</button>
        <button className="slide-btn">{t("block5.category.9")}</button>
        <button className="slide-btn">{t("block5.category.10")}</button>
        <button className="slide-btn">{t("block5.category.11")}</button>
        <button className="slide-btn">{t("block5.category.12")}</button>
      </Slider>
    </div>
  )
}
export default SliderButton;

