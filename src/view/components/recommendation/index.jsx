import React from "react"
import { useTranslation } from 'react-i18next'
import Container from "../../containers/container"

import Galery from "./galery"
import SliderButton from "./slider-button"

import "./style.scss"

const Recommendation = () => {
    const { t, i18n } = useTranslation();
    return (
        <div className="recommendation">
            <Container>
                <div className="recommendation__block-title">
                    <span className="recommendation__title">{t("block5.title")} </span>
                    <span className="recommendation__subtitle">{t("block5.subtitle")}</span>
                </div>
                <SliderButton/>
                <Galery/>
            </Container>

        </div>
    )
}

export default Recommendation;