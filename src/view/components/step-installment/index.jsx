import React from "react"

import "./style.scss"
import img1 from "../../../assets/img/step-installment/1.png"
import img2 from "../../../assets/img/step-installment/2.png"
import img3 from "../../../assets/img/step-installment/3.png"
import Container from "../../containers/container"
import { useTranslation } from 'react-i18next'

const StepInstallment = () => {
    const { t, i18n } = useTranslation();
    return (
        <div className="step-installment" id="step-installment">
            <Container>
                <span className="step-installment__title">{t("block3.title")}</span>
                <div className="step-installment__wrapper">
                    <div className="step-installment__card">
                        <img alt="number" style={{width:"161px"}} src={img1} />
                        <span style={{ left: "73px" }}>{t("block3.card1")}</span>
                    </div>
                    <div className="step-installment__card">
                        <img alt="number" style={{width:"255px", height: "308px"}} src={img2} />
                        <span>{t("block3.card2")}</span>
                    </div>
                    <div className="step-installment__card">
                        <img alt="number" style={{width:"255px", height: "308px"}} src={img3} />
                        <span>{t("block3.card3")}</span>
                    </div>
                </div>
            </Container>
        </div>

    )
}
export default StepInstallment;