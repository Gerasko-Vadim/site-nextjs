import React from "react"
import { useTranslation } from 'react-i18next'
import pc from "../../../assets/img/about-app/pc.png"
import phone from "../../../assets/img/about-app/phone.png"
import qrCode from "../../../assets/img/about-app/qr-code.svg"
import "./style.scss"

const AboutApp = () => {
    const { t, i18n } = useTranslation();
    return (
        <div className="about-app" id="about-app">
            <div className="about-app__block-img">
                <div className="about-app__image-elips" />
                <img src={pc} className="about-app__pc" />
                <img src={phone} className="about-app__phone" />
            </div>
            <div className="about-app__block-content">
                <span className="about-app__title">{t("block8.title")}</span>
                <span className="about-app__description">
                    {t("block8.subtitle")}
                </span>
                <div className="about-app__block-activity">
                    <img src={qrCode} />
                    <span></span>
                    <a href="https://play.google.com/store/apps/details?id=kg.cash2u.client" target="_blank">
                        <button>{t("block8.button")}</button>
                    </a>
                </div>
            </div>
        </div>
    )
}
export default AboutApp;