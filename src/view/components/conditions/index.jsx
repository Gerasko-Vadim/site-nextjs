import React from "react"

import "./style.scss"
import cash from "../../../assets/img/conditions/vaadin_cash.svg"
import calendar from "../../../assets/img/conditions/topcoat_calendar.svg"
import file from "../../../assets/img/conditions/mdi_file-percent-outline.svg"
import slesh from "../../../assets/img/conditions/fa-solid_handshake-slash.svg"
import clock from "../../../assets/img/conditions/Group.svg"
import mobilephone from "../../../assets/img/conditions/emojione-monotone_mobile-phone-with-arrow.svg"
import ConditionItem from "../condition-item"
import 'animate.css/animate.css'
import RubberBand from 'react-reveal/RubberBand';
import Fade from 'react-reveal/Fade';
import Container from "../../containers/container"
import { useTranslation } from 'react-i18next'




const Conditions = () => {
    const { t, i18n } = useTranslation();
    const arrCards = [
        { id: 0, img: cash, text: t("block2.card1") },
        { id: 1, img: calendar, text: t("block2.card2") },
        { id: 2, img: file, text: t("block2.card3") },
        { id: 3, img: slesh, text: t("block2.card4") },
        { id: 4, img: clock, text: t("block2.card5") },
        { id: 5, img: mobilephone, text: t("block2.card6") },
    ]
    return (
        <div className="conditions" id="conditions">

            <span className="conditions__title ">
               { t("block2.title")}
            </span>


            <Container>
                <div className="conditions__wrapper">
                    {
                        arrCards.map((item) => {
                            return (
                                <ConditionItem className="" key={item.id} img={item.img} text={item.text} />

                            )


                        })
                    }
                </div>
            </Container>

        </div >
    )
}
export default Conditions;