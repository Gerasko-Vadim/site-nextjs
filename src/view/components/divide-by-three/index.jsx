import React from "react"

import "./style.scss"
import jacket from "../../../assets/img/divide-by-three/jacket.svg"
import sneakers from "../../../assets/img/divide-by-three/sneakers.svg"
import Container from "../../containers/container"
import { useTranslation } from 'react-i18next'


const DivideByThree = () => {
    const { t, i18n } = useTranslation();
    return (
        <div className="divide-by-three" id="divide-by-three">
            <Container>
                <span className="divide-by-three__title">
                   {t("block4.title")}
                </span>
                <span className="divide-by-three__subtitle">
                    {t("block4.subtitle")}
                </span>
                <div className="divide-by-three__grid-wrapper">
                    <div className="divide-by-three__payments">
                        <div className="divide-by-three__payments__title">
                            <span>
                                {t("block4.payments")}
                            </span>
                        </div>
                        <div className="divide-by-three__payments__math">
                            <span className="divide-by-three__payments__math__title">МАЙ</span>
                            <span className="divide-by-three__payments__math__price">1550 сом</span>
                        </div>
                        <div className="divide-by-three__payments__math">
                            <span className="divide-by-three__payments__math__title">ИЮНЬ</span>
                            <span className="divide-by-three__payments__math__price">1550 сом</span>
                        </div>
                        <div className="divide-by-three__payments__math">
                            <span className="divide-by-three__payments__math__title">ИЮЛЬ</span>
                            <span className="divide-by-three__payments__math__price">3333 сом</span>
                        </div>
                        <div className="divide-by-three__payments__math">
                            <span className="divide-by-three__payments__math__title">АВГ</span>
                            <span className="divide-by-three__payments__math__price">3333 сом</span>
                        </div>
                        <div className="divide-by-three__payments__math">
                            <span className="divide-by-three__payments__math__title">СЕН</span>
                            <span className="divide-by-three__payments__math__price">1783 сом</span>
                        </div> 
                    </div>
                    <div className="divide-by-three__example-1">
                        <div className="divide-by-three__title-example">
                            <img src={jacket} className="divide-by-three__images" />
                            <div className="divide-by-three__title-example__price">
                                <span className="product">Куртка</span><br />
                                <span className="price">4650 сом</span>
                            </div>


                        </div>
                        <div className="divide-by-three__price-row-1">
                            <div className="divide-by-three__block-price">
                                <span>1550 сом</span>
                            </div>
                            <div className="divide-by-three__block-price">
                                <span>1550 сом</span>
                            </div>
                            <div className="divide-by-three__block-price">
                                <span>1550 сом</span>
                            </div>


                        </div>

                    </div>
                    <div className="divide-by-three__example-2">
                        <div className="divide-by-three__title-example sneakers">
                            <img src={sneakers} className="divide-by-three__images" />
                            <div className="divide-by-three__title-example__price">
                                <span className="product">Кроссовки</span><br />
                                <span className="price">5350 сом</span>
                            </div>
                        </div>
                        <div className="divide-by-three__price-row-2">
                            <div className="divide-by-three__block-price">
                                <span>1783 сом</span>
                            </div>
                            <div className="divide-by-three__block-price">
                                <span>1783 сом</span>
                            </div>
                            <div className="divide-by-three__block-price">
                                <span>1783 сом</span>
                            </div>


                        </div>

                    </div>
                    <div className="divide-by-three__example-3">
                        <div className="divide-by-three__example-3__block-title">
                            <span>
                                {t("block4.remainder")}
                            </span>
                        </div>
                        <div className="divide-by-three__price-row-3">
                            <div className="divide-by-three__block-price">
                                <span>5350 сом</span>
                            </div>
                            <div className="divide-by-three__block-price">
                                <span>1550 сом</span>
                            </div>
                            <div className="divide-by-three__block-price">
                                <span>4883 сом</span>
                            </div>
                            <div className="divide-by-three__block-price">
                                <span>8217 сом</span>
                            </div>
                            <div className="divide-by-three__block-price">
                                <span>10000 сом</span>
                            </div>

                        </div>
                    </div>
                    <div className="divide-by-three__total-amount">
                        <span>{t("block4.total")}</span>
                    </div>
                </div>
            </Container>
        </div>
    )
}
export default DivideByThree;