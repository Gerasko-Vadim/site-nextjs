import React, { useState } from "react"
import { IoIosAdd } from "react-icons/io";
import icon from "../../../../assets/img/questions/_.svg"

import { CgMathPlus,  CgMathMinus} from "react-icons/cg";
import "./style.scss"



const AccordionItem = ({ title, info }) => {
    const [expanded, setExpanded] = useState(false)
    return (
        <article className='question'>
            <header >
                <div className="question__block-title">
                    <img alt="icon" style={{ marginRight: "10px" }} src={icon} />

                    <h4 onClick={() => setExpanded(!expanded)} className='question-title'>
                        {title}
                    </h4>
                </div>

                <div className='btn' onClick={() => setExpanded(!expanded)}>
                    {expanded ? <span style={{fontSize:"32px"}} >-</span> :<span style={{fontSize:"32px"}}>+</span> }
                </div>
            </header>
            {expanded && <p className="question__content">{info}</p>}
        </article>

    )
}
export default AccordionItem;