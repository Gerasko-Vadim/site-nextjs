import React from "react"
import { useTranslation } from 'react-i18next'
import Container from "../../containers/container"



import AccordionItem from "./accordion-item"

import "./style.scss"

const Quetions = () => {
    const { t, i18n } = useTranslation();
    return (
        <div className="questions" id="questions">
            <Container>
                <div className="questions__block-title">
                    Ответы на часто задаваемые вопросы
                </div>
                <div className="questions__accordion-wrapper">
                    <div>
                        <AccordionItem title={t("block7.questions.1.title")} info={<span> {t("block7.questions.1.answers")}
                        </span>} />
                        <AccordionItem title={t("block7.questions.2.title")} info={t("block7.questions.2.answers")} />
                        <AccordionItem title={t("block7.questions.3.title")} info={t("block7.questions.3.answers")} />
                        <AccordionItem
                            title={
                                <span>
                                    {t("block7.questions.4.title")}
                                </span>}
                            info={t("block7.questions.4.answers")} />
                    </div>
                    <div>
                        <AccordionItem title={t("block7.questions.5.title")} info={t("block7.questions.5.answers")} />
                        <AccordionItem title={t("block7.questions.6.title")} info={t("block7.questions.6.answers")} />
                        <AccordionItem title={t("block7.questions.7.title")} info={t("block7.questions.7.answers")} />

                    </div>

                </div>



            </Container>

        </div>
    )
}

export default Quetions;