import React from "react"

import plaza from "../../../assets/img/shoping-center/plaza.png"
import bishkekPark from "../../../assets/img/shoping-center/bishkekPark.png"
import asiaMall from "../../../assets/img/shoping-center/asiaMall.png"
import "./style.scss"
import Container from "../../containers/container"
import { useTranslation } from 'react-i18next'

const ShopingCenter = () => {
    const { t, i18n } = useTranslation();
    return (
        <div className="shoping-center" id="shoping-center">
            <Container>
                <span className="shoping-center__title">{t("block6.title")}</span>
                <div className="shoping-center__wrapper-cards">
                    <div className="shoping-center__card">
                        <img alt="img" src={plaza} />
                        <div className="shoping-center__card-text-content">
                            <span className="shoping-center__card-text-content__quantity">
                                {t("block6.card1")}
                            </span>
                        </div>
                    </div>
                    <div className="shoping-center__card">
                        <img alt="img" src={bishkekPark} style={{ marginTop: "auto" }} />
                        <div className="shoping-center__card-text-content">
                            <span className="shoping-center__card-text-content__quantity">
                                {t("block6.card2")}
                            </span>
                        </div>
                    </div>
                    <div className="shoping-center__card">
                        <img alt="img" src={asiaMall} />
                        <div className="shoping-center__card-text-content">
                            <span className="shoping-center__card-text-content__quantity">
                                {t("block6.card3")}
                            </span>
                        </div>
                    </div>
                </div>
                <div className="shoping-center__block-btn">
                    <button>
                        <span> Другие ТЦ </span>
                        <svg width="25" height="20" viewBox="0 0 25 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M0.989788 11.5752L19.9708 11.1997L13.2146 17.9937L15.0899 19.7359L24.9273 9.84326L14.7063 0.347476L12.9014 2.16248L19.921 8.68397L0.940012 9.05953L0.989788 11.5752Z" fill="#F6F6F6" fillOpacity="0.96" />
                        </svg>
                    </button>

                </div>


            </Container>
        </div>
    )
}
export default ShopingCenter;