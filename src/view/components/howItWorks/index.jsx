import React from "react"
import Container from "../../containers/container"
import phone from "../../../assets/img/how-it-works/phone.png"
import pc from "../../../assets/img/how-it-works/pc.png"
import money from "../../../assets/img/how-it-works/money.png"
import clock from "../../../assets/img/how-it-works/clock.png"
import "./style.scss"
import { useTranslation } from 'react-i18next'



const HowItWorks = () => {
    const { t, i18n } = useTranslation()
    return (
        <div className="how-it-works" id="how-it-works">
            <Container>
                <div className="how-it-works__block-title">
                    <span className="how-it-works__title">
                        {t("partner-page.block3.title")}
                    </span>
                </div>
                <div className="how-it-works__wrapper">
                    <div className="how-it-works__block-column-item">
                        <img alt="pc" src={pc} className="icon" />
                        <div className="how-it-works__content">
                            <span>
                                1. {t("partner-page.block3.1")}
                                
                            </span>
                        </div>
                    </div>
                    <div className="how-it-works__block-column-item">

                        <div className="how-it-works__content">
                            <span>
                                2. {t("partner-page.block3.2")}
                            </span>
                        </div>
                        <img alt="phone" src={phone} className="icon" />

                    </div>
                    <div className="how-it-works__block-column-item">
                        <img alt="money" src={money} className="icon" />
                        <div className="how-it-works__content">
                            <span>
                                3. {t("partner-page.block3.3")}
                            </span>
                        </div>
                    </div>
                    <div className="how-it-works__block-column-item">
                        <div className="how-it-works__content">
                            <span>
                                4. {t("partner-page.block3.4")}
                            </span>
                        </div>
                        <img alt="clock" src={clock} className="icon" />

                    </div>
                </div>
            </Container>

        </div>
    )
}

export default HowItWorks;