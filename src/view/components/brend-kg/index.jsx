import React from "react"
import "./style.scss"
import anta from "../../../assets/img/become-partner/anta.png"
import defacto from "../../../assets/img/become-partner/defacto.png"
import lc from "../../../assets/img/become-partner/lc.png"
import econica from "../../../assets/img/become-partner/эконика.png"
import next from "../../../assets/img/become-partner/next.png"
import podium from "../../../assets/img/become-partner/podium.png"
import Container from "../../containers/container"
import { useTranslation } from 'react-i18next'


const BrendKg = () => {
    const { t, i18n } = useTranslation()
    return (
        <Container>
            <div className="brend-kg" id="brend-kg">
                <span className="brend-kg__title">
                    {t("partner-page.block2.title")}
                </span>
                <div className="brend-kg__wrapper-img">
                    <img alt="brend" src={anta} />
                    <img alt="brend" src={defacto} />
                    <img alt="brend" src={lc} />
                    <img alt="brend" src={econica} />
                    <img alt="brend" src={next} />
                    <img alt="brend" src={podium} />

                </div>

            </div>
        </Container>

    )
}
export default BrendKg;