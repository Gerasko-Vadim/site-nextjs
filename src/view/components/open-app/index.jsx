import React from "react"
import Container from "../../containers/container"
import "./style.scss"
import phone from "../../../assets/img/phone.png"
import qrCode from "../../../assets/img/qr-code.svg"
import elips from "../../../assets/img/elips.png"
import { useTranslation } from 'react-i18next'

const OpenApp = () => {
    const { t, i18n } = useTranslation();
    return (
        <div className="open-app">
            <div className="open-app__content">
                <p className="open-app__title" align="left">
                    {t("block1.title")}
                </p>
                <p align="left" className="open-app__subtitle">{t("block1.subtitle")}</p>
                <div className="open-app__block-actions">
                    <a href="https://play.google.com/store/apps/details?id=kg.cash2u.client" target="_blank" className="open-app__btn">
                        { t("block1.button") }
                    </a>
                    <span></span>
                    <img className="open-app__qr-code" src={qrCode} />
                </div>
            </div>
            <div className="open-app__image-block">
            <div className="open-app__image-elips"/>
                <img className="open-app__phone" src={phone} />
            </div>

        </div>
    )
}
export default OpenApp;