import React from "react"
import { useTranslation } from 'react-i18next';
import Container from "../../containers/container";

import "./style.scss"

// "cards": {
//     "1": {
//         "title": "Платежные терминалы",
//             "subtitle": "АДРЕСА ТЕРМИНАЛОВ",
//                 "li1": "Внесение без комиссии",
//                     "li2": "Моментальное зачисление"
//     },

const RepaymentScheme = () => {
    const { t, i18n } = useTranslation();
    return (
        <div className="repayment-schema" id="repayment-schema">
            <Container>
                <span className="repayment-schema__title">{t("block9.title")}</span>
                <div className="repayment-schema__cards-wrapper">
                    <div className="repayment-schema__card">
                        <div className="repayment-schema__black-card"></div>
                        <span className="repayment-schema__card-title">{t("block9.title")}</span>
                        <button className="repayment-schema__card-btn">
                            <span>{t("block9.cards.1.subtitle")}</span>
                            <svg width="17" height="14" viewBox="0 0 17 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M0.0354374 7.81015L13.4363 7.74886L8.59484 12.4716L9.89969 13.7213L16.9492 6.84472L9.83709 0.0328569L8.54372 1.29451L13.4281 5.97275L0.0273145 6.03404L0.0354374 7.81015Z" fill="#525252" fill-opacity="0.96" />
                            </svg>
                        </button>
                        <div className="repayment-schema__card-capabilities">
                            <div className="repayment-schema__card-capabilities-item">
                                <svg width="15" height="12" viewBox="0 0 15 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M4.707 8.58597L1.414 5.29297L0 6.70697L4.707 11.414L14.414 1.70697L13 0.292969L4.707 8.58597Z" fill="#BDB8B8" />
                                </svg>
                                <span>{t("block9.cards.1.li1")}</span>
                            </div>
                            <div className="repayment-schema__card-capabilities-item">
                                <svg width="15" height="12" viewBox="0 0 15 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M4.707 8.58597L1.414 5.29297L0 6.70697L4.707 11.414L14.414 1.70697L13 0.292969L4.707 8.58597Z" fill="#BDB8B8" />
                                </svg>
                                <span>{t("block9.cards.1.li2")}</span>
                            </div>
                        </div>
                    </div>
                    <div className="repayment-schema__card">
                        <div className="repayment-schema__black-card"></div>
                        <span className="repayment-schema__card-title">{t("block9.cards.2.title")}</span>
                        <button className="repayment-schema__card-btn">
                            <span>{t("block9.cards.2.subtitle")}</span>
                            <svg width="17" height="14" viewBox="0 0 17 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M0.0354374 7.81015L13.4363 7.74886L8.59484 12.4716L9.89969 13.7213L16.9492 6.84472L9.83709 0.0328569L8.54372 1.29451L13.4281 5.97275L0.0273145 6.03404L0.0354374 7.81015Z" fill="#525252" fill-opacity="0.96" />
                            </svg>
                        </button>
                        <div className="repayment-schema__card-capabilities">
                            <div className="repayment-schema__card-capabilities-item">
                                <svg width="15" height="12" viewBox="0 0 15 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M4.707 8.58597L1.414 5.29297L0 6.70697L4.707 11.414L14.414 1.70697L13 0.292969L4.707 8.58597Z" fill="#BDB8B8" />
                                </svg>
                                <span>{t("block9.cards.2.li1")}</span>
                            </div>
                            <div className="repayment-schema__card-capabilities-item">
                                <svg width="15" height="12" viewBox="0 0 15 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M4.707 8.58597L1.414 5.29297L0 6.70697L4.707 11.414L14.414 1.70697L13 0.292969L4.707 8.58597Z" fill="#BDB8B8" />
                                </svg>
                                <span>{t("block9.cards.2.li")}</span>
                            </div>
                        </div>
                    </div>
                    <div className="repayment-schema__card">
                        <div className="repayment-schema__black-card"></div>
                        <span className="repayment-schema__card-title">{t("block9.cards.3.title")}</span>
                        <button className="repayment-schema__card-btn">
                            <span>{t("block9.cards.3.subtitle")}</span>
                            <svg width="17" height="14" viewBox="0 0 17 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M0.0354374 7.81015L13.4363 7.74886L8.59484 12.4716L9.89969 13.7213L16.9492 6.84472L9.83709 0.0328569L8.54372 1.29451L13.4281 5.97275L0.0273145 6.03404L0.0354374 7.81015Z" fill="#525252" fill-opacity="0.96" />
                            </svg>
                        </button>
                        <div className="repayment-schema__card-capabilities">
                            <div className="repayment-schema__card-capabilities-item">
                                <svg width="15" height="12" viewBox="0 0 15 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M4.707 8.58597L1.414 5.29297L0 6.70697L4.707 11.414L14.414 1.70697L13 0.292969L4.707 8.58597Z" fill="#BDB8B8" />
                                </svg>
                                <span>{t("block9.cards.3.li1")}</span>
                            </div>
                            <div className="repayment-schema__card-capabilities-item">
                                <svg width="15" height="12" viewBox="0 0 15 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M4.707 8.58597L1.414 5.29297L0 6.70697L4.707 11.414L14.414 1.70697L13 0.292969L4.707 8.58597Z" fill="#BDB8B8" />
                                </svg>
                                <span>{t("block9.cards.3.li2")}</span>
                            </div>
                        </div>
                    </div>

                </div>
            </Container>
        </div>
    )
}

export default RepaymentScheme;