import React from "react"
import logo from "../../../../assets/img/logo-for-header.svg"
import classNames from "classnames"
import { Nav, Navbar } from "react-bootstrap"
import { Link } from "react-router-dom"
import { useTranslation } from 'react-i18next'

const ClientHeader = ({
    setLanguage,
    language
}) => {
    const { t, i18n } = useTranslation();
    return (
        <Navbar expand="xxl"
        >
            <Navbar.Brand><Link to="/"><img src={logo} className="header__logo" /></Link></Navbar.Brand>
            <Navbar.Toggle aria-controls="responsive-navbar-nav" />
            <Navbar.Collapse id="responsive-navbar-nav" className="nav-content">
                <Nav className="me-auto">
                    <Nav.Link href="#conditions">{t("header-client.link1")}</Nav.Link>
                    <Nav.Link href="#shoping-center">{t("header-client.link2")}</Nav.Link>
                    <Nav.Link href="#step-installment">{t("header-client.link3")}</Nav.Link>
                    <Nav.Link href="#about-app">{t("header-client.link4")}</Nav.Link>
                    <Nav.Link href="#repayment-schema">{t("header-client.link5")}</Nav.Link>
                    <Link className="nav-link" to="/business">{t("header-client.link6")}</Link>
                </Nav>
                <Nav>
                    <Nav className="header__switch-lang">
                        <a className={classNames('header__lang', {
                            'header__lang-active': language === "ru"
                        })} onClick={() => setLanguage("ru")}>RU</a>
                        <a className={classNames('header__lang', {
                            'header__lang-active': language === "kg"
                        })} onClick={() => setLanguage("kg")}>KG</a>
                    </Nav>
                    <Nav.Link eventKey={2} href="https://play.google.com/store/apps/details?id=kg.cash2u.client" target="_blank">
                        <button className="header__get-credit">
                            {t("header-client.button")}
                        </button>
                    </Nav.Link>
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    )
}

export default ClientHeader;