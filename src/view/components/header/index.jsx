import React, { useEffect, useState } from "react"
import "./style.scss"
import logo from "../../../assets/img/logo-for-header.svg"
import classNames from "classnames"
import { Container, Nav, Navbar, NavDropdown } from "react-bootstrap"
import { Link } from "react-router-dom"
import { useSelector } from "react-redux"
import ClientHeader from "./clientHeader"
import PartnerHeader from "./partnerHeader"
import { useTranslation } from 'react-i18next'


const Header = () => {
    const [language, setLanguage] = useState(localStorage.getItem('lang') || 'ru')
    const { i18n } = useTranslation();

    const isClient = useSelector((state) => state.HandleChanheHeader.isClient)
    useEffect(()=>{
        localStorage.setItem('lang', language)
        i18n.changeLanguage(language);
    }, [language])
    return (
        <div className="header">

            {
                isClient ? <ClientHeader language={language} setLanguage={setLanguage}/> : <PartnerHeader language={language} setLanguage={setLanguage}/>
            }

        </div>
    )
}
export default Header;

