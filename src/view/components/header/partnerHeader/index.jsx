import React from "react"
import logo from "../../../../assets/img/logo-for-header.svg"
import classNames from "classnames"
import { Container, Nav, Navbar, NavDropdown } from "react-bootstrap"
import { Link } from "react-router-dom"
import { useTranslation } from 'react-i18next'


const PartnerHeader = ({
    setLanguage,
    language
}) => {

    const { t, i18n } = useTranslation()
    return (
        <Navbar expand="xxl"
        >
            <Navbar.Brand><Link to="/"><img src={logo} className="header__logo" /></Link></Navbar.Brand>
            <Navbar.Toggle aria-controls="responsive-navbar-nav" />
            <Navbar.Collapse id="responsive-navbar-nav" className="nav-content">
                <Nav className="me-auto">
                    <Nav.Link href="#brend-kg">{t("partner-page.header.link1")} </Nav.Link>
                    <Nav.Link href="#interest">{t("partner-page.header.link2")}</Nav.Link>
                    <Nav.Link href="#benefit">{t("partner-page.header.link3")}</Nav.Link>
                    <Nav.Link href="#how-it-works">{t("partner-page.header.link4")}</Nav.Link>
                    <Nav.Link href="#feedback-form">{t("partner-page.header.link5")}</Nav.Link>
                </Nav>
                <Nav>
                    <Nav className="header__switch-lang">
                        <a className={classNames('header__lang', {
                            'header__lang-active': language === "ru"
                        })} onClick={() => setLanguage("ru")}>RU</a>
                        <a className={classNames('header__lang', {
                            'header__lang-active': language === "kg"
                        })} onClick={() => setLanguage("kg")}>KG</a>
                    </Nav>
                    <Nav.Link eventKey={2} target="_blank" href="https://reverent-hopper-b3ba6d.netlify.app">
                        <button className="header__get-credit">
                            {t("partner-page.header.button")}
                        </button>
                    </Nav.Link>
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    )
}

export default PartnerHeader;