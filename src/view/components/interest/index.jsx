import React from "react"
import { useTranslation } from 'react-i18next';
import Container from "../../containers/container";

import "./style.scss"



const Interest = () => {
    const { t, i18n } = useTranslation()
    return (
        <div className="interest" id="interest">
            <Container>
                <div className="interest__block-title">
                    <span className="interest__title">
                        {t("partner-page.block4.title")}
                    </span>
                </div>

                <div className="interest__wrapper">
                    <div className="interest__card">
                        <span className="interest__card-title">45%</span>
                        <span className="interest__card-description">{t("partner-page.block4.1")}</span>
                    </div>
                    <div className="interest__card">
                        <span className="interest__card-title">2-2,5</span>
                        <span className="interest__card-description">{t("partner-page.block4.2")}</span>
                    </div>
                    <div className="interest__card">
                        <span className="interest__card-title">100 - 200 %</span>
                        <span className="interest__card-description">{t("partner-page.block4.3")}</span>
                    </div>
                </div>
            </Container>
        </div>
    )
}
export default Interest;