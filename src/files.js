import { consent } from "./view/pages/docs/files/consent-of-the-personal-data-subject";
import { UserInfo } from "./view/pages/docs/files/info-users";
import { loanAgreement } from "./view/pages/docs/files/loan-agreement";
import { oferta } from "./view/pages/docs/files/oferta";
import TableExample, { passportCreditDoc } from "./view/pages/docs/files/passport-credit-doc";
import { requisites } from './view/pages/docs/files/requisites';
import { ConfirmationForUser } from "./view/pages/docs/files/text-for-confirmation-user";
import { privacyPolicy } from './view/pages/docs/files/privacy-policy';




export const files = [
    {
        name: "Информация для клиента",
        path: "info-user",
        file: UserInfo
    },
    {
        name: "Приложение 1",
        path: "rights-and-expenses",
        file: ConfirmationForUser
    },
    {
        name: "Публичная оферта",
        path: "oferta",
        file: oferta
    },
    {
        name: "Согласие субъекта персональных данных на сбор и обработку его персональных данных",
        path: "consent-of-the-personal-data-subject",
        file: consent
    },
    {
        name: "Публичный кредитный договор",
        path: "loan-agreement",
        file: loanAgreement
    },
    {
        name: "Реквизиты МКК Келечек для МП",
        path: "requistes",
        file: requisites
    },
    {
        name: "Политика безопасности и конфиденциальности мобильного приложения",
        path: "privacy-policy",
        file: privacyPolicy
    },
    // {
    //     name:"Паспорт кредитного продукта «Онлайн»",
    //     path: "passport-credit-doc",
    //     file: passportCreditDoc
    // }
]