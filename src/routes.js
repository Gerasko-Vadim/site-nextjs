import React, { useEffect } from "react"
import {
    Switch,
    Route,
} from "react-router-dom";
import Main from './view/pages/main';
import Business from './view/pages/business';
import Docs from './view/pages/docs';
import Doc from './view/pages/doc';
import PrivacyPolicy from './view/pages/privacyPolicy';

import { useLocation } from 'react-router';
import ReactGA from 'react-ga';


ReactGA.initialize('UA-212130805-1');

const Routes = () => {

    const location = useLocation();

    // Fired on every route change
    useEffect(() => {
        ReactGA.pageview(location.pathname + location.search);
        const isReferal = (location.pathname + location.search).indexOf('agent')
        if (isReferal > 0){
            const referalLink = location.pathname + location.search
            sessionStorage.setItem('referal', referalLink.split('=').pop())
        }
        else{
            
        }
    }, [location]);


    return (
        <Switch>
            <Route exact path="/" component={Main} />
            <Route path="/business" component={Business} />
            <Route path={["/docs", "/kg/docs"]} component={Docs} />
            <Route path={["/doc/:name", "/kg/doc/:name"]} component={Doc} />
            <Route path="/privacy-policy" component={PrivacyPolicy} />
        </Switch>
    )
}

export default Routes