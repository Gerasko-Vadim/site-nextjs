import { NextPage } from 'next'
import React from "react"
import { Htag } from '../../components'
import { Container } from '../../components/Container/Container'
import { ApplicationForm } from '../../layout/ApplicationForm/ApplicationForm'
import { BusinessIsProfitable } from '../../layout/BusinessIsProfitable/BusinessIsProfitable'
import { HelpBusiness } from '../../layout/HelpBusiness/HelpBusiness'
import { HowItWork } from '../../layout/HowItWorks/HowItWorks'
import { Layout } from '../../layout/layout'
import { TrustUs } from '../../layout/TrustUs/TrustUs'
import { WelcomeBlockBusiness } from '../../layout/WelcomeBlockBusiness/WelcomeBlockBusiness'


const Business = () => {
    return (
        <Layout>
            <WelcomeBlockBusiness />
            <TrustUs />
            <HowItWork/>
            <HelpBusiness/>
            <BusinessIsProfitable/>
            <ApplicationForm/>
        </Layout>
    )
}

export default Business