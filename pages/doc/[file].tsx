import type { GetStaticPaths, GetStaticProps, GetStaticPropsContext, NextPage } from 'next'
import { useRouter } from 'next/router'
import { ParsedUrlQuery } from 'querystring';
import renderHTML from 'react-render-html';
import { ReactNode } from 'react';
import { files, filesKgList } from '../../files/files';
import { Layout } from '../../layout/layout'
import { DocWrapper } from '../../components/docWrapper/docWrapper';



const Doc = ({file}):JSX.Element => {
    
    if (!file) {
        return <></>
    }
    else {
        return (
            <Layout>
                <DocWrapper>
                    {renderHTML(file)}
                </DocWrapper>

            </Layout>
        )
    }

}

export default Doc

export const getStaticPaths: GetStaticPaths = async () => {

    return {
        paths: [
            { params: { file: 'info-user'} },
            { params: { file: 'rights-and-expenses' } },
            { params: { file: 'oferta'}},
            { params: { file: 'consent-of-the-personal-data-subject' } },
            { params: { file: 'loan-agreement'}},
            { params: { file: 'requistes'}},
            { params: { file: 'privacy-policy'}},
            { params: { file: 'rules-promotion'}}
        ],
        fallback: true
    }
}

// export const getStaticProps: GetStaticProps<FileType> = async ({ params }: GetStaticPropsContext<ParsedUrlQuery>)=> {

//     if(!params){
//         return {notFound: true}
//     }
//     const filebyFind = files.find(x => x.path === params.file);

//     return {
//         props: filebyFind
//     }
// }

export const getStaticProps = async ( {params, locale}) => {
    if (!params){
        return{
            notFound: false
        }
    }
    const file = locale == "ru" ? files.find((file) => file.path === params.file) : filesKgList.find((file) => file.path === params.file)
    
    return {
        props: {
            ...file
        }
    }

}

interface FileType extends Record<string, unknown> {
    name: string
    path: string,
    file: ReactNode

}
