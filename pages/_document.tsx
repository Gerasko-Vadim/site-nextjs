import Document, { Html, Head, Main, NextScript } from 'next/document'

export default class MyDocument extends Document {
    render() {
        return (
            <Html>
                <Head/>
                <body>
                    <noscript>
                        <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KL9DZX4"
                        height="0" width="0"></iframe>
                    </noscript>
                    <Main />
                    <NextScript />
                </body>
            </Html>
        )
    }
}