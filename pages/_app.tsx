import '../styles/globals.css'
import { AppProps } from 'next/app'
import Script from 'next/script'
import Head from "next/head"
import { useRouter } from 'next/router';
import "../styles/header.scss"
import "../styles/info-user.scss"

import 'bootstrap/dist/css/bootstrap.min.css';
import React, {  useEffect } from 'react'

import { initGA, logPageView } from "../utils/analytics"

// Router.events.on('routeChangeComplete', (url: string) => {
//   if (typeof window !== 'undefined') {
//     ReactGA.pageview(url)
//   }
// });

// Initial React Google Analytics
initGA()

function MyApp({ Component, pageProps }: AppProps) {
  const router = useRouter()

  useEffect(() => {
    if (typeof (window) === undefined) return




    const handleRouteChange = (url) => {
      logPageView(url);

    };



    router.events.on("routeChangeComplete", handleRouteChange);

    return () => {
      if (typeof (window) === undefined) return
      router.events.off("routeChangeComplete", handleRouteChange);
    };

  }, [router.events]);



  return (
    <>
      <Head>
        <title>Cash2u</title>
        <meta name="theme-color" content="#48B3AB" />
        <meta name="facebook-domain-verification" content="0z9t0enfhl9o0lcf3hqdbeiig2ayym" />
        <link rel="shortcut icon" href="/static/logo.svg" />
        <link rel="stylesheet" href="/css/video-react.css" />
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link href="dist/hamburgers.css" rel="stylesheet" />
        <script
          dangerouslySetInnerHTML={{
            __html: `
                                (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                                    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                                    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                                    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
                                    })(window,document,'script','dataLayer','GTM-KL9DZX4');
                                `,
          }}
        />
      </Head>

      <Script
      id="my-script"
        strategy="afterInteractive"
        dangerouslySetInnerHTML={{
          __html: `
            !function(f,b,e,v,n,t,s)
            {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window,document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '3170313369872268'); 
            fbq('track', 'PageView');
          `,
        }}
      />
      <Component {...pageProps} />
    </>
  )
}
export default MyApp;
