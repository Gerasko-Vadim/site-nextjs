import type { NextPage } from 'next'
import { useRouter } from 'next/router'
import { Htag } from '../../components'
import { Container } from '../../components/Container/Container'
import { Layout } from '../../layout/layout'
import { kg } from '../../locales/kg'
import { ru } from '../../locales/ru'
import { DocsWrapper } from '../../page-components/docs/docs'



const Docs: NextPage = () => {
    const router = useRouter();
    const t = router.locale === 'ru' ? ru : kg;

    return (
        <Layout>
            <Container>
                <Htag size="h1">
                    {t['footer-client'].column1.line3}
                </Htag>

                <div>
                    <DocsWrapper />
                </div>
            </Container>

        </Layout>
    )
}

export default Docs