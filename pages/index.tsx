import type { NextPage } from 'next'
import Head from 'next/head'
import Image from 'next/image'
import { useRouter } from 'next/router'

import { About } from '../layout/about/about'
import { BuyfromCash2u } from '../layout/BuyFromCash2u/BuyFromCash2u'
import { Condition } from '../layout/Condition/Condition'
import { FormBlock } from '../layout/form/form'
import { GreetBlock } from '../layout/GreetBlock/GreetBlock'
import { HowToPay } from '../layout/HowToPay/HowToPay'
import { HowToRepay } from '../layout/HowToRepay/HowToRepay'
import { Installment } from '../layout/installment/installment'
import { Layout } from '../layout/layout'
import { Questions } from '../layout/questions/questions'





const Home: NextPage = () => {
  const { locale, locales, defaultLocale, asPath } = useRouter();
  return (
    <Layout>
      <GreetBlock/>
      <Condition/>
      <Installment/>
      <HowToPay/>
      <BuyfromCash2u/>

      <Questions/>
      <About/>
      <HowToRepay />
      <FormBlock/>
    </Layout>
  )
}

export default Home
