import { UserInfo} from "./filesRu/info-users"
import { ConfirmationForUser} from "./filesRu/text-for-confirmation-user"
import { oferta} from "./filesRu/oferta"
import { consent} from "./filesRu/consent-of-the-personal-data-subject"
import { loanAgreement} from "./filesRu/loan-agreement"
import { requisites} from "./filesRu/requisites"
import { privacyPolicy } from "./filesRu/privacy-policy"
import { rulesPromotion} from "./filesRu/rules_promotion"

import { UserInfoKg } from "./filesKg/user-info"
import { ConfirmationForUserKg } from "./filesKg/text-for-confirmation-user"
import { ofertaKg } from "./filesKg/oferta"
import { consentKg } from "./filesKg/consent-of-the-personal-data-subject"
import { loanAgreementKg } from "./filesKg/loan-agreement"
import { privacyPolicyKg } from "./filesKg/privacy-policy"



const files = [
    {
        name: "Информация для клиента",
        path: "info-user",
        file: UserInfo
    },
    {
        name: "Правила проведения акции",
        path: 'rules-promotion',
        file: rulesPromotion
    },
    {
        name: "Приложение 1",
        path: "rights-and-expenses",
        file: ConfirmationForUser
    },
    {
        name: "Публичная оферта",
        path: "oferta",
        file: oferta
    },
    {
        name: "Согласие субъекта персональных данных на сбор и обработку его персональных данных",
        path: "consent-of-the-personal-data-subject",
        file: consent
    },
    {
        name: "Публичный кредитный договор",
        path: "loan-agreement",
        file: loanAgreement
    },
    {
        name: "Реквизиты МКК Келечек для МП",
        path: "requistes",
        file: requisites
    },
    {
        name: "Политика безопасности и конфиденциальности мобильного приложения",
        path: "privacy-policy",
        file: privacyPolicy
    },
    // {
    //     name:"Паспорт кредитного продукта «Онлайн»",
    //     path: "passport-credit-doc",
    //     file: passportCreditDoc
    // }
]


const filesKgList = [
    {
        name: "КАРДАР ҮЧҮН МААЛЫМАТ",
        path: "info-user",
        file: UserInfoKg
    },
    {
        name: "1-тиркеме",
        path: "rights-and-expenses",
        file: ConfirmationForUserKg
    },
    {
        name: "Коомдук сунуш",
        path: "oferta",
        file: ofertaKg
    },
    {
        name: "Жеке маалыматтар субъектинин анын жеке маалыматтарын чогултууга жана иштетүүгө макулдугу",
        path: "consent-of-the-personal-data-subject",
        file: consentKg
    },
    {
        name: "Мамлекеттик насыя келишими",
        path: "loan-agreement",
        file: loanAgreementKg
    },
    // {
    //     name: "Реквизиты МКК Келечек для МП",
    //     path: "requistes",
    //     file: requisites
    // },
    {
        name: "Мобилдик колдонмонун коопсуздугу жана купуялык саясаты",
        path: "privacy-policy",
        file: privacyPolicyKg
    },
    // {
    //     name:"Паспорт кредитного продукта «Онлайн»",
    //     path: "passport-credit-doc",
    //     file: passportCreditDoc
    // }
]

export {
    files,
    filesKgList
}