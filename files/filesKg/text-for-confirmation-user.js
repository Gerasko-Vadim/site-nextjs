export const ConfirmationForUserKg = `<html>

<head>
<meta http-equiv=Content-Type content="text/html; charset=utf-8">
<meta name=Generator content="Microsoft Word 15 (filtered)">
<style>
<!--
 /* Font Definitions */
 @font-face
	{font-family:"Cambria Math";
	panose-1:2 4 5 3 5 4 6 3 2 4;}
@font-face
	{font-family:Calibri;
	panose-1:2 15 5 2 2 2 4 3 2 4;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin-top:0in;
	margin-right:0in;
	margin-bottom:8.0pt;
	margin-left:0in;
	line-height:107%;
	font-size:11.0pt;
	font-family:"Calibri",sans-serif;}
p.MsoListParagraph, li.MsoListParagraph, div.MsoListParagraph
	{margin-top:0in;
	margin-right:0in;
	margin-bottom:8.0pt;
	margin-left:.5in;
	line-height:107%;
	font-size:11.0pt;
	font-family:"Calibri",sans-serif;}
p.MsoListParagraphCxSpFirst, li.MsoListParagraphCxSpFirst, div.MsoListParagraphCxSpFirst
	{margin-top:0in;
	margin-right:0in;
	margin-bottom:0in;
	margin-left:.5in;
	line-height:107%;
	font-size:11.0pt;
	font-family:"Calibri",sans-serif;}
p.MsoListParagraphCxSpMiddle, li.MsoListParagraphCxSpMiddle, div.MsoListParagraphCxSpMiddle
	{margin-top:0in;
	margin-right:0in;
	margin-bottom:0in;
	margin-left:.5in;
	line-height:107%;
	font-size:11.0pt;
	font-family:"Calibri",sans-serif;}
p.MsoListParagraphCxSpLast, li.MsoListParagraphCxSpLast, div.MsoListParagraphCxSpLast
	{margin-top:0in;
	margin-right:0in;
	margin-bottom:8.0pt;
	margin-left:.5in;
	line-height:107%;
	font-size:11.0pt;
	font-family:"Calibri",sans-serif;}
.MsoChpDefault
	{font-family:"Calibri",sans-serif;}
.MsoPapDefault
	{margin-bottom:8.0pt;
	line-height:107%;}
@page WordSection1
	{size:8.5in 11.0in;
	margin:56.7pt 42.5pt 56.7pt 85.05pt;}
div.WordSection1
	{page:WordSection1;}
 /* List Definitions */
 ol
	{margin-bottom:0in;}
ul
	{margin-bottom:0in;}
-->
</style>

</head>

<body lang=EN-US style='word-wrap:break-word'>

<div class=WordSection1>

<div style='display:flex;flex-direction: row; justify-content:flex-end'>
        <div>
            <p class=MsoNormal style='margin-top:0in;text-align:justify;line-height:normal'><span lang=RU
            style='font-size:12.0pt;font-family:"Times New Roman",serif'>“Келечек” МНКнын</span></p>

            <p class=MsoNormal style='margin-top:0in;text-align:justify;line-height:normal'><span lang=RU
            style='font-size:12.0pt;font-family:"Times New Roman",serif'>Айкын насыя келишимине</span></p>

            <p class=MsoNormal style='margin-top:0in;text-align:justify;line-height:normal'><span lang=RU
            style='font-size:12.0pt;font-family:"Times New Roman",serif'>№ 1 тиркеме</span></p>

        </div>
</div>

<p class=MsoNormal style='margin-bottom:0in;text-align:justify;line-height:
normal'><b><span lang=RU style='font-size:12.0pt;font-family:"Times New Roman",serif'>&nbsp;</span></b></p>

<p class=MsoNormal style='margin-bottom:0in;text-align:justify;line-height:
normal'><b><span lang=RU style='font-size:12.0pt;font-family:"Times New Roman",serif'>&nbsp;</span></b></p>

<p class=MsoNormal style='margin-bottom:0in;text-align:justify;line-height:
normal'><b><span lang=RU style='font-size:12.0pt;font-family:"Times New Roman",serif'>Укуктардын, чыгымдардын (төлөмдөрдүн) жана айып санкцияларынын тизмеги</span></b></p>

<p class=MsoNormal style='margin-bottom:0in;text-align:justify;line-height:
normal'><span lang=RU style='font-size:12.0pt;font-family:"Times New Roman",serif'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpFirst style='margin:0in;text-align:justify;
text-indent:0in;line-height:normal'><b><span lang=RU style='font-size:12.0pt;
font-family:"Times New Roman",serif'>1.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></b><b><span lang=RU style='font-size:12.0pt;font-family:"Times New Roman",serif'>Кардардын укугу</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin:0in;text-align:justify;
text-indent:0in;line-height:normal'><span lang=RU style='font-size:12.0pt;
font-family:"Times New Roman",serif'>1.1.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;font-family:"Times New Roman",serif'>Айкын насыя келишимине кол койгон учурдан тартып, келишим боюнча 
Товар үчүн төлөм ишке ашканга чейин кайтарымсыз негизде насыяны алуудан баш тартууга;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin:0in;text-align:justify;
text-indent:0in;line-height:normal'><span lang=RU style='font-size:12.0pt;
font-family:"Times New Roman",serif'>1.2.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;font-family:"Times New Roman",serif'>Айкын насыя келишимге, ага тиркелген бардык документтерге 
жетүүгө мүмкүнчүлүгү бар жана убакыт жагынан чектелбестен МНКнын чегинен тышкары юридикалык консультацияга кайрылууга;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin:0in;text-align:justify;
text-indent:0in;line-height:normal'><span lang=RU style='font-size:12.0pt;
font-family:"Times New Roman",serif'>1.3.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;font-family:"Times New Roman",serif'>Насыя, туум, айып санкциялар боюнча төлөмдөрдүн эсептеринин тартиби боюнча түшүндүрмө алууга;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin:0in;text-align:justify;
text-indent:0in;line-height:normal'><span lang=RU style='font-size:12.0pt;
font-family:"Times New Roman",serif'>1.4.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;font-family:"Times New Roman",serif'>Насыя келишими жана ушул тизмек түзүлө турган (таризделген) тилди (мамлекеттик же расмий) тандоого;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin:0in;text-align:justify;
text-indent:0in;line-height:normal'><span lang=RU style='font-size:12.0pt;
font-family:"Times New Roman",serif'>1.5.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;font-family:"Times New Roman",serif'>Насыя боюнча карызды үстөк айып же айып санкцияларынын башка түрлөрүн төлөбөстөн каалаган учурда, мөөнөтүнөн мурда толук көлөмдө же бөлүп төлөөгө.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin:0in;text-align:justify;
line-height:normal'><span lang=RU style='font-size:12.0pt;font-family:"Times New Roman",serif'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin:0in;text-align:justify;
text-indent:0in;line-height:normal'><b><span lang=RU style='font-size:12.0pt;
font-family:"Times New Roman",serif'>2.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></b><b><span lang=RU style='font-size:12.0pt;font-family:"Times New Roman",serif'>Насыя боюнча чыгымдар (төлөмдөр)</span></b></p>

<p class=MsoListParagraphCxSpLast style='margin:0in;text-align:justify;
line-height:normal'><b><span lang=RU style='font-size:12.0pt;font-family:"Times New Roman",serif'>&nbsp;</span></b></p>

<table class=MsoTableGrid border=1 cellspacing=0 cellpadding=0
 style='border-collapse:collapse;border:none'>
 <tr>
  <td width=323 valign=top style='width:241.95pt;border:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;text-align:justify;line-height:
  normal'><span lang=RU style='font-size:12.0pt;font-family:"Times New Roman",serif'>Насыянын суммасы</span></p>
  </td>
  <td width=323 valign=top style='width:242.0pt;border:solid windowtext 1.0pt;
  border-left:none;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;text-align:justify;line-height:
  normal'><span lang=RU style='font-size:12.0pt;font-family:"Times New Roman",serif'>МТда көрсөтүлөт</span></p>
  </td>
 </tr>
 <tr>
  <td width=323 valign=top style='width:241.95pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;text-align:justify;line-height:
  normal'><span lang=RU style='font-size:12.0pt;font-family:"Times New Roman",serif;
  color:black;background:#F9F9F9'>Насыя боюнча пайыздык төлөмдөр</span></p>
  </td>
  <td width=323 valign=top style='width:242.0pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;text-align:justify;line-height:
  normal'><span lang=RU style='font-size:12.0pt;font-family:"Times New Roman",serif'>0%</span></p>
  </td>
 </tr>
 <tr>
  <td width=323 valign=top style='width:241.95pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;line-height:normal'><span
  lang=RU style='font-size:12.0pt;font-family:"Times New Roman",serif;
  color:black;background:#F9F9F9'>Өтүнмөнү карагандыгы үчүн комиссия (насыяны тариздөө)</span></p>
  </td>
  <td width=323 valign=top style='width:242.0pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;text-align:justify;line-height:
  normal'><span lang=RU style='font-size:12.0pt;font-family:"Times New Roman",serif'>Каралган эмес</span></p>
  </td>
 </tr>
 <tr>
  <td width=323 valign=top style='width:241.95pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;line-height:normal'><span
  lang=RU style='font-size:12.0pt;font-family:"Times New Roman",serif;
  color:black;background:#F9F9F9'>Насыяны берүү жана башкаруу үчүн комиссия</span></p>
  </td>
  <td width=323 valign=top style='width:242.0pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;text-align:justify;line-height:
  normal'><span lang=RU style='font-size:12.0pt;font-family:"Times New Roman",serif'>Каралган эмес</span></p>
  </td>
 </tr>
 <tr>
  <td width=323 valign=top style='width:241.95pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;line-height:normal'><span
  lang=RU style='font-size:12.0pt;font-family:"Times New Roman",serif;
  color:black;background:#F9F9F9'>Ссудалык жана/же күндөлүк эсептерди ачуу жана тейлөө үчүн комиссия</span></p>
  </td>
  <td width=323 valign=top style='width:242.0pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;text-align:justify;line-height:
  normal'><span lang=RU style='font-size:12.0pt;font-family:"Times New Roman",serif'>Каралган эмес</span></p>
  </td>
 </tr>
 <tr>
  <td width=323 valign=top style='width:241.95pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;line-height:normal'><span
  lang=RU style='font-size:12.0pt;font-family:"Times New Roman",serif;
  color:black;background:#F9F9F9'>Эсептик-кассалык тейлөө үчүн комиссия</span></p>
  </td>
  <td width=323 valign=top style='width:242.0pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;text-align:justify;line-height:
  normal'><span lang=RU style='font-size:12.0pt;font-family:"Times New Roman",serif'>Каралган эмес</span></p>
  </td>
 </tr>
 <tr>
  <td width=323 valign=top style='width:241.95pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;line-height:normal'><span
  lang=RU style='font-size:12.0pt;font-family:"Times New Roman",serif;
  color:black;background:#F9F9F9'>Айып санкциялар жана туумдар</span></p>
  </td>
  <td width=323 valign=top style='width:242.0pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;text-align:justify;line-height:
  normal'><span lang=RU style='font-size:12.0pt;font-family:"Times New Roman",serif;
  color:black;background:#F9F9F9'>Карыздын негизги сумма боюнча төлөөнү узарткандыгы үчүн – туум, 
  узартылган ар бир күн үчүн негизги карыздын узартылган суммасынан 1 (бир) % өлчөмдө. Мында, насыя 
  колдонулган бардык мезгили үчүн туумдун өлчөмү, берилген насыянын суммасынан 20% (жыйырма пайыздан) 
  ашпоого тийиш.</span></p>
  </td>
 </tr>
</table>

<p class=MsoNormal style='margin-bottom:0in;text-align:justify;line-height:
normal'><span lang=RU style='font-size:12.0pt;font-family:"Times New Roman",serif'>&nbsp;</span></p>

</div>

</body>

</html>

`