export const privacyPolicyKg = `
 <html>
    <head>
        <meta content="text/html; charset=UTF-8" http-equiv="content-type">
        <style type="text/css">
          
            ol {
                margin: 0;
                padding: 0;
            }
            table td,
            table th {
                padding: 0;
            }
            .c2 {
                margin-left: 72pt;
                padding-top: 0pt;
                padding-bottom: 0pt;
                line-height: 1.15;
                orphans: 2;
                widows: 2;
                text-align: left;
                height: 11pt;
            }
            .c9 {
                margin-left: 72pt;
                padding-top: 0pt;
                padding-left: 0pt;
                padding-bottom: 0pt;
                line-height: 1.15;
                orphans: 2;
                widows: 2;
                text-align: left;
            }
            .c8 {
                margin-left: 108pt;
                padding-top: 0pt;
                padding-left: 0pt;
                padding-bottom: 0pt;
                line-height: 1.15;
                orphans: 2;
                widows: 2;
                text-align: left;
            }
            .c15 {
                margin-left: 36pt;
                padding-top: 12pt;
                padding-left: 0pt;
                padding-bottom: 12pt;
                line-height: 1.15;
                orphans: 2;
                widows: 2;
                text-align: left;
            }
            .c14 {
                margin-left: 72pt;
                padding-top: 18pt;
                padding-left: 0pt;
                padding-bottom: 4pt;
                line-height: 1.15;
                orphans: 2;
                widows: 2;
                text-align: left;
            }
            .c7 {
                margin-left: 36pt;
                padding-top: 0pt;
                padding-left: 0pt;
                padding-bottom: 0pt;
                line-height: 1.15;
                orphans: 2;
                widows: 2;
                text-align: left;
            }
            .c5 {
                margin-left: 36pt;
                padding-top: 18pt;
                padding-left: 0pt;
                padding-bottom: 4pt;
                line-height: 1.15;
                orphans: 2;
                widows: 2;
                text-align: left;
            }
            .c0 {
                color: #000000;
                font-weight: 400;
                text-decoration: none;
                vertical-align: baseline;
                font-size: 12pt;
                font-family: "Arial";
                font-style: normal;
            }
            .c3 {
                padding-top: 0pt;
                padding-bottom: 0pt;
                line-height: 1.15;
                orphans: 2;
                widows: 2;
                text-align: left;
                height: 11pt;
            }
            .c10 {
                color: #000000;
                font-weight: 700;
                text-decoration: none;
                vertical-align: baseline;
                font-size: 17pt;
                font-family: "Arial";
                font-style: normal;
            }
            .c16 {
                padding-top: 0pt;
                padding-bottom: 0pt;
                line-height: 1.15;
                orphans: 2;
                widows: 2;
                text-align: left;
            }
            .c13 {
                padding-top: 0pt;
                padding-bottom: 12pt;
                line-height: 1.15;
                orphans: 2;
                widows: 2;
                text-align: left;
            }
            .c20 {
                padding-top: 12pt;
                padding-bottom: 12pt;
                line-height: 1.15;
                orphans: 2;
                widows: 2;
                text-align: left;
            }
            .c17 {
                padding-top: 24pt;
                padding-bottom: 3pt;
                line-height: 1.15;
                orphans: 2;
                widows: 2;
                text-align: left;
            }
            .c21 {
                color: #000000;
                text-decoration: none;
                vertical-align: baseline;
                font-family: "Arial";
                font-style: normal;
            }
            .c12 {
                background-color: #ffffff;
                max-width: 451.4pt;
                padding: 72pt 72pt 72pt 72pt;
            }
            .c22 {
                font-weight: 700;
                font-size: 16pt;
            }
            .c23 {
                font-weight: 400;
                font-size: 18pt;
            }
            .c24 {
                font-weight: 400;
                font-size: 11pt;
            }
            .c18 {
                margin-left: 72pt;
                padding-left: 0pt;
            }
            .c26 {
                margin-left: 72pt;
                height: 11pt;
            }
            .c25 {
                text-indent: 36pt;
                height: 11pt;
            }
            .c11 {
                font-size: 17pt;
                font-weight: 700;
            }
            .c1 {
                font-size: 12pt;
                font-weight: 700;
            }
            .c6 {
                padding: 0;
                margin: 0;
            }
            .c19 {
                margin-left: 36pt;
            }
            .c4 {
                font-size: 12pt;
            }
            .title {
                padding-top: 0pt;
                color: #000000;
                font-size: 26pt;
                padding-bottom: 3pt;
                font-family: "Arial";
                line-height: 1.15;
                page-break-after: avoid;
                orphans: 2;
                widows: 2;
                text-align: left;
            }
            .subtitle {
                padding-top: 0pt;
                color: #666666;
                font-size: 15pt;
                padding-bottom: 16pt;
                font-family: "Arial";
                line-height: 1.15;
                page-break-after: avoid;
                orphans: 2;
                widows: 2;
                text-align: left;
            }
            li {
                color: #000000;
                font-size: 11pt;
                font-family: "Arial";
            }
            p {
                margin: 0;
                color: #000000;
                font-size: 11pt;
                font-family: "Arial";
            }
            h1 {
                padding-top: 20pt;
                color: #000000;
                font-size: 20pt;
                padding-bottom: 6pt;
                font-family: "Arial";
                line-height: 1.15;
                page-break-after: avoid;
                orphans: 2;
                widows: 2;
                text-align: left;
            }
            h2 {
                padding-top: 18pt;
                color: #000000;
                font-size: 16pt;
                padding-bottom: 6pt;
                font-family: "Arial";
                line-height: 1.15;
                page-break-after: avoid;
                orphans: 2;
                widows: 2;
                text-align: left;
            }
            h3 {
                padding-top: 16pt;
                color: #434343;
                font-size: 14pt;
                padding-bottom: 4pt;
                font-family: "Arial";
                line-height: 1.15;
                page-break-after: avoid;
                orphans: 2;
                widows: 2;
                text-align: left;
            }
            h4 {
                padding-top: 14pt;
                color: #666666;
                font-size: 12pt;
                padding-bottom: 4pt;
                font-family: "Arial";
                line-height: 1.15;
                page-break-after: avoid;
                orphans: 2;
                widows: 2;
                text-align: left;
            }
            h5 {
                padding-top: 12pt;
                color: #666666;
                font-size: 11pt;
                padding-bottom: 4pt;
                font-family: "Arial";
                line-height: 1.15;
                page-break-after: avoid;
                orphans: 2;
                widows: 2;
                text-align: left;
            }
            h6 {
                padding-top: 12pt;
                color: #666666;
                font-size: 11pt;
                padding-bottom: 4pt;
                font-family: "Arial";
                line-height: 1.15;
                page-break-after: avoid;
                font-style: italic;
                orphans: 2;
                widows: 2;
                text-align: left;
            }
        </style>
    </head>
    <body class="c12 c0">
        <p class="c17 title" id="h.alc4j8howate">
            <span class="c21 c23">
               Мобилдик тиркеменин коопсуздук жана купуялуулук саясаты
            </span>
        </p>
        <p class="c3"><span class="c21 c24"></span></p>
        <p class="c16">
            <span class="c0">
                «cash2u» мобилдик тиркемесин орнотуунун жана колдоно баштоонун алдында, пайдалануучулардын маалыматтарына
                 жана аларды биз кантип чогултабыз жана колдонорубузга карата биздин эрежени жана практиканы түшүнүү үчүн, ушул мобилдик тиркеменин коопсуздук жана купуялуулук саясаты жана Тиркемени колдонуу эрежелери менен кунт коюп таанышып чыгууну терең өтүнөбүз.
            </span>
            
        </p>
        <p class="c3"><span class="c0"></span></p>
        <p class="c13">
            <span class="c1 c21">
              Урматтуу Пайдалануучу, Сиздин көңүлүңүздү бурабыз! Мобилдик тиркеменин коопсуздук жана купуялуулук саясатын Сиздин кабыл алууңуз Сиз (Пайдалануучу) төмөнкүлөргө макул экендигиңизди билдирет:
            </span>
        </p>
        <ul class="c6 lst-kix_xl8n7jwvehjz-0 start">
            <li class="c15 li-bullet-0">
                <span class="c0">
                жеке маалымдарыңызды жана төмөндө көрсөтүлгөн башка маалыматтарды чогултуу шарттары менен макулсуз, маалыматтарды иштеп чыгууга, анын ичинде Сиздин жеке маалымдарыңызды мамлекеттик мекемеге берүүгө толук жана сөзсүз макулдук берүүгө, </span>
            </li>
            <li class="c15 li-bullet-0">
                <span class="c0">
                    ушул саясаттын шартын сактоого макулсуз жана милдеттенесиз,
                </span>
            </li>
            <li class="c15 li-bullet-0">
                <span class="c1">	Сиз 18 жашка чыккандыгыңызды, </span>
                <span class="c0">
                    Сиз ишке жарамдуу болуп эсептелериңизди тастыктайсыз, ошондой эле өзүңүздүн аракеттериңизге толук өлчөмдө отчет бересиз жана аны жетектейсиз, өз ыктыярыңыз жана өзүңүздүн кызыкчылыкта аракеттенесиз.
                </span>
            </li>
        </ul>
        <p class="c3"><span class="c0"></span></p>
        <ol class="c6 lst-kix_t85gcsts95k4-0 start" start="1">
            <li class="c5 li-bullet-0">
                <h2 id="h.p67wnmzhc98x" style="display: inline;"><span class="c10">1.Жалпы жобо</span></h2>
            </li>
        </ol>
        <ol class="c6 lst-kix_t85gcsts95k4-1 start" start="1">
            <li class="c13 c18 li-bullet-0">
            <span class="c1">cash2u</span>
                <span class="c0">
                    Тиркемесин жүктөп алуу жана колдонуу саясат жана анда көрсөтүлгөн маалыматтарды чогултуу жана иштеп чыгуу шарттары менен Пайдалануучунун сөзсүз макулдугун түшүндүрөт.
                </span>
            </li>
            <li class="c13 c18 li-bullet-0">
            <span class="c0">
                    	Тиркемеге катталуу менен Пайдалануучу жеке маалымдарын иштеп чыгуу үчүн өз ыктыяры жана өзүнүн кызыкчылыгында жеке маалымдарын жана жеке мүнөздөгү башка маалыматтарды берет.
                </span>
                
            </li>
            <li class="c13 c18 li-bullet-0">
                <span class="c0">
                    	Жеке маалымдарды иштеп чыгуу деп, жеке маалымдар, анын ичинде чогултуу, системалаштыруу, топтоо, сактоо, тактоо (жаңылоо, өзгөртүү), колдонуу жана мамлекеттик органдарга берүү менен байланышкан аракет (операциялар) түшүнүлөт. Жеке маалымдар иштеп чыгуунун аралаш методу менен Интернет тармагы боюнча берүүнү пайдалануу менен автоматташтырылган ыкмада иштелип чыгат.
                </span>
            </li>
            <li class="c13 c18 li-bullet-0">
            <span class="c0">
                	Ушул саясат <span class="c1">cash2u</span> мобилдик тиркемесине гана колдонулат.
                     Компания <span class="c1">cash2u</span> тиркемеси менен байланышкан жана жеткиликтүү
                      шилтемелер боюнча Пайдалануучу өз алдынча өтө ала турган, үчүнчү жактардын башка мобилдик 
                      тиркемелерин жана сайтын контролдобойт жана жоопкерчилик тартпайт.
                      </span>
            </li>
            <li class="c13 c18 li-bullet-0">
                <span class="c0">
                    	Компания Пайдалануучунун жеке жана башка маалыматтарын мыйзамга ылайыксыз, санкциясыз же кокус жеткиликтүүлүктөн, максатсыз пайдалануудан, жок кылуудан, өзгөртүүдөн, тосмолоодон, көчүрмөлөөдөн, жайылтуудан, ошондой эле үчүнчү жактардын башка мыйзамга ылайыксыз аракеттеринен коргоо үчүн зарыл уюштуруу жана техникалык чараларды көрөт.
                </span>
            </li>
            <li class="c18 c20 li-bullet-0">
            <span class="c0">
           
                    Саясаттын шарттары менен макул болбогон учурда Пайдалануучу «cash2u» мобилдик тиркемесин колдонууну токтотууга тийиш.
          
                <span class="c1">cash2u</span><span class="c0">&raquo;.</span>
                </span>
            </li>
        </ol>
        <p class="c20 c26"><span class="c0"></span></p>
        <ol class="c6 lst-kix_t85gcsts95k4-0" start="2">
        <li class="c5 li-bullet-0">
                <h2 id="h.xy7jc2lkos3o" style="display: inline;">
                    <span class="c10">2.Купуялуулук саясатынын предмети</span>
                </h2>
            </li>
           
        </ol>
        <ol class="c6 lst-kix_t85gcsts95k4-1 start" start="1">
            <li class="c14 li-bullet-0">
                <h2 id="h.sbamjc676op1" style="display: inline;">
                    <span class="c0">
                        Ушул саясат бөлүп төлөөнү иденттештирүү жана тариздөөнү <span class="c1">cash2u</span> мобилдик тиркемесине каттатууда, Пайдалануучу «cash2u» тиркемесине берген жеке маалымдардын купуялуулугун коргоо режимин жарыялабоо жана камсыз кылуу боюнча компаниянын милдетин аныктайт.
                    </span>
                </h2>
            </li>
            <li class="c14 li-bullet-0">
                <h2 id="h.hdtc3uzoz25" style="display: inline;">
                    <span class="c0">
                        Жогоруда айтылбаган бардык башка жеке маалымат ишенимдүү сакталууга жана жайылтылбоого тийиш.
                    </span>
                </h2>
            </li>
        </ol>
        <p class="c2"><span class="c21 c24"></span></p>
        <ol class="c6 lst-kix_t85gcsts95k4-0" start="3">
            <li class="c5 li-bullet-0">
                <h2 id="h.xy7jc2lkos3o" style="display: inline;">
                    <span class="c10">3.Биз чогултуучу маалымат</span>
                </h2>
            </li>
        </ol>
        <ol class="c6 lst-kix_t85gcsts95k4-1 start" start="1">
            <li class="c9 li-bullet-0">
            <h2 id="h.ltdr9wezmq9o" style="display: inline;">
                <span class="c0">
                   	Ушул тиркемени жүктөп алууда, каттатууда же колдонууда Пайдалануучу суралган маалыматты берет, ага төмөнкүлөр кирет:
                </span>
                </h2>
            </li>
        </ol>
        <ul class="c6 lst-kix_nwrtlbrdhawv-0 start">
            <li class="c8 li-bullet-0"><h2 id="h.ltdr9wezmq9o" style="display: inline;"><span class="c0">Мобилдик телефондун номери;</span></h2></li>
            <li class="c8 li-bullet-0">
            <h2 id="h.ltdr9wezmq9o" style="display: inline;">
                <span class="c0">
                   	Сиздин өздүгүңүздү тастыктаган документтин маалыматтары жана сүрөтү;
                </span>
                </h2>
            </li>
            <li class="c8 li-bullet-0">
            <h2 id="h.ltdr9wezmq9o" style="display: inline;">
                <span class="c0">	Башка жеке маалыматтар.</span>
                </h2>
            </li>
        </ul>
        <p class="c3"><span class="c0"></span></p>
        <p class="c3"><span class="c0"></span></p>
        <ol class="c6 lst-kix_t85gcsts95k4-0" start="4">
            <li class="c7 li-bullet-0">
                <span class="c10">
                    4.	Пайдалануучу жөнүндө маалыматтарды чогултуунун максаты
                </span>
            </li>
        </ol>
        <ol class="c6 lst-kix_t85gcsts95k4-1 start" start="1">
            <li class="c9 li-bullet-0">
            <h2 id="h.ltdr9wezmq9o" style="display: inline;">
                <span class="c0">
                    	Тиркеме, тиркеменин курамына кирген продуктыларды берүү үчүн зарыл болгон жеке маалымдарды чогултат жана сактайт.
                </span>
                </h2>
            </li>
            <li class="c9 li-bullet-0">
            <h2 id="h.ltdr9wezmq9o" style="display: inline;">
                <span class="c0">
                    	Эгерде Пайдалануучу жеке маалымдарды иштеп чыгууга макулдук берсе, аралыктан өздүгүн аныктоо үчүн «cash2u» мобилдик тиркемесинде Пайдалануучуну иденттештирүү.
                </span>
                <span class="c1">cash2u</span>
                <span class="c0">
                    	Эгерде Пайдалануучу жеке маалымдарын иштеп чыгууга макулдук берсе, сатып алууну жүргүзүү үчүн продуктуну бөлүп төлөөгө кошуу.
                </span>
                </h2>
            </li>
            <li class="c9 li-bullet-0">
            <h2 id="h.ltdr9wezmq9o" style="display: inline;">
                <span class="c0">
                   	Пайдалануучу берген жеке маалыматтардын аныктыгын жана толуктугун тастыктоо.
                </span>
                </h2>
            </li>
        </ol>
        <p class="c3"><span class="c0"></span></p>
        <p class="c3"><span class="c0"></span></p>
        <ol class="c6 lst-kix_t85gcsts95k4-0" start="5">
            <li class="c7 li-bullet-0">
                <span class="c10">
                    	Пайдалануучулардын жеке маалыматтарын иштеп чыгуу жана аны үчүнчү жактарга берүү шарты
                </span>
            </li>
        </ol>
        <ol class="c6 lst-kix_t85gcsts95k4-1 start" start="1">
            <li class="c9 li-bullet-0">
            <h2 id="h.ltdr9wezmq9o" style="display: inline;">
                <span class="c0">
                   	Пайдалануучунун жеке маалымдарын иштеп чыгуу мөөнөтүн чектебестен, баардык мыйзамдуу ыкмалар менен, анын ичинде  автоматташтыруу каражаттарын пайдалануу же мындай каражаттарды пайдаланбастан жеке маалымдардын маалыматтык тутумдарында ишке ашырылат.
                </span>
                </h2>
            </li>
        </ol>
        <p class="c2"><span class="c0"></span></p>
        <ol class="c6 lst-kix_t85gcsts95k4-0" start="6">
            <li class="c5 li-bullet-0">
                <h2 id="h.ylo5ylf0w42u" style="display: inline;"><span class="c10">6.	Маалыматтардын коопсуздугу </span></h2>
            </li>
        </ol>
        <ol class="c6 lst-kix_t85gcsts95k4-1 start" start="1">
            <li class="c14 li-bullet-0">
                <h2 id="h.ltdr9wezmq9o" style="display: inline;">
                    <span class="c0">
                        	Компания пайдалануучулардын жеке маалыматын кокустан жоготуу, санкцияланбаган жеткиликтүүлүктөн, маалыматты пайдалануу, өзгөртүү жана ачуу коопсуздугун камсыз кылууга  багытталган зарыл жана жетиштүү уюштуруу жана техникалык чараларды көрөт.
                    </span>
                </h2>
            </li>
            <li class="c9 li-bullet-0">
            <h2 id="h.ltdr9wezmq9o" style="display: inline;">
                <span class="c0">
                    	Пайдалануучулар Тиркемеге берген маалымат корголгон серверлерде сакталат.
                </span>
                </h2>
            </li>
            <li class="c9 li-bullet-0">
            <h2 id="h.ltdr9wezmq9o" style="display: inline;">
                <span class="c0">
                    Компания ошондой эле пайдалануучулардын купуялуулуктун кайсы бир тескөөлөрүн четтеп өтүүгө багытталган жана/же Биз сактоого сунуштаган коопсуздук чараларын бузган аракеттери үчүн жоопкерчилик тартпайт.
                </span>
            </h2>
            </li>
        </ol>
        <p class="c3 c19"><span class="c0"></span></p>
        <ol class="c6 lst-kix_t85gcsts95k4-0" start="7">
            <li class="c7 li-bullet-0"><span class="c10">7.	Саясатты өзгөртүү. </span></li>
        </ol>
        <ol class="c6 lst-kix_t85gcsts95k4-1 start" start="1">
            <li class="c9 li-bullet-0">
                <h2 id="h.ltdr9wezmq9o" style="display: inline;">
                <span class="c0">
                    	<span class="c1">cash2u</span> Пайдалануучунун макулдугусуз ушул Саясатка өзгөртүү киргизүүгө укуктуу. Актуалдуу редакцияга өзгөртүүлөрдү киргизген учурда акыркы жаңылоонун датасы көрсөтүлөт. Эгерде Саясаттын жаңы редакциясында башкалар каралбаса, Саясаттын жаңы редакциясы аны жайгаштырган учурдан тартып күчүнө кирет.
                </span>
                </h2>
            </li>
        </ol>
        <p class="c3"><span class="c10"></span></p>
        <ol class="c6 lst-kix_t85gcsts95k4-0" start="8">
            <li class="c5 li-bullet-0">
                <h2 id="h.ur8z0bjvcwt9" style="display: inline;"><span class="c10">Башка шарттар</span></h2>
            </li>
        </ol>
        <ol class="c6 lst-kix_t85gcsts95k4-1 start" start="1">
            <li class="c14 li-bullet-0">
                <h2 id="h.594bksmsntsj" style="display: inline;">
                    <span class="c0">
                        Катталып жана Тиркеменин маалыматтарын пайдалануу менен Пайдалануучу өзү 18 жашка чыккандыгын тастыктайт.
                </h2>
            </li>
            <li class="c14 li-bullet-0">
                <h2 id="h.pw2s5kn867r8" style="display: inline;">
                    <span class="c0">
                        	Компания 18 жашка чыга элек адамдар жөнүндө маалымдарды чогултпайт жана сактабайт. Мындай адамдарды, алардын  Тиркемени пайдалануу максатында каттоого жол берилбейт.
                    </span>
                </h2>
            </li>
            <li class="c14 li-bullet-0">
                <h2 id="h.ltdr9wezmq9o" style="display: inline;">
                    <span class="c0">
                        	Ушул Саясат боюнча бардык сунуштарды же суроолорду lalalalalala@gmail.com электрондук почтага билдирүү керек.
                    </span>
                </h2>
            </li>
        </ol>
        <p class="c20 c25"><span class="c0"></span></p>
        <p class="c3"><span class="c10"></span></p>
        <p class="c16">
            <span class="c1">
                Эгерде Сиздер жогоруда келтирилген маалыматтардын кайсы бирин биз чогултууну каалабасаңыздар, бул Тиркемени жүктөбөңүздөр жана пайдаланбаңыздар.
            </span>
        </p>
        <p class="c3"><span class="c0"></span></p>
    </body>
</html>
`
