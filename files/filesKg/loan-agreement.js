export const loanAgreementKg = `
<html>

<head>
<meta http-equiv=Content-Type content="text/html; charset=utf-8">
<meta name=Generator content="Microsoft Word 15 (filtered)">
<style>
<!--
 /* Font Definitions */
 @font-face
	{font-family:"Cambria Math";
	panose-1:2 4 5 3 5 4 6 3 2 4;}
@font-face
	{font-family:Verdana;
	panose-1:2 11 6 4 3 5 4 4 2 4;}
@font-face
	{font-family:Georgia;
	panose-1:2 4 5 2 5 4 5 2 3 3;}
@font-face
	{font-family:"Noto Sans Symbols";}
@font-face
	{font-family:Tahoma;
	panose-1:2 11 6 4 3 5 4 4 2 4;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{    margin-top: 0in;
    margin-right: 0in;
    margin-bottom: 8.0pt;
    margin-left: 0in;
    line-height: 107%;
    font-size: 11.0pt;}
.MsoChpDefault
	{font-size:12.0pt;}
 /* Page Definitions */
 @page WordSection1
	{size:595.3pt 841.9pt;
	margin:28.35pt 42.5pt 26.95pt 85.05pt;}
div.WordSection1
	{page:WordSection1;}
 /* List Definitions */
 ol
	{margin-bottom:0in;}
ul
	{margin-bottom:0in;}
-->
</style>

</head>

<body lang=EN-US style='word-wrap:break-word'>

<div class=WordSection1>

<p class=MsoNormal align=center style='text-align:center'><b><span style='font-family: "Times New Roman"' lang=RU>АЙКЫН НАСЫЯ КЕЛИШИМИ</span></b></p>

<p class=MsoNormal style='text-align: justify;'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>Керектөө насыясынын алкагында 
жеке жактар үчүн Кыргыз Республикасынын Улуттук Банкы тарабынан берилген 2021-жылдын 6-августундагы № 510 эсептик каттоо тууралуу Күбөлүктүн негизинде аракеттенген “МНК “Келечек” ЖЧКнын 
(мындан ары – МНК) ушул Айкын Насыя Келишими  www.cahs2u.kg дареги боюнча МНКнын Интернет – ресурсунда жана «Cash2U» Мобилдик тиркеменин (мындан ары – МТ) “Документтер” бөлүмүндө жарыяланган
 «Cash2U» сервисине кошулуу келишимин түзүү жөнүндө “МНК “Келечек” ЖЧКнын Айкын офертасынын (мындан ары – Оферта) алкагында иш жүргүзөт жана керектөө насыясынын алкагында жеке жактар (мындан ары – Зайымчы) менен түзүлүүчү Насыя келишиминин шартын аныктайт.</span></p>

<p class=MsoNormal style='text-align:justify'><span lang=RU>&nbsp;</span></p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in;border:none'><b><span style='font-family: "Times New Roman"'
lang=RU>1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
НАСЫЯЛООНУН ПРЕДМЕТИ ЖАНА ШАРТЫ </span> </b></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>1.1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
МНКнын ыйгарым укуктуу органы тарабынан жактырылган учурда МНКнын ушул келишими боюнча МНК кызматташуу 
жөнүндө макулдашуу түзгөн соода уюмунан (мындан ары – соода уюму) Зайымчыга товарларды/кызмат көрсөтүүнү 
сатып алууга акча каражатын насыяга (мындан ары – “насыя”) берет, ал эми Зайымчы ушул келишимде аныкталган 
шарттарда насыяны кайтарып берүүгө милдеттенет.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>1.2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ушул Келишим 
тараптардын ортосунда Офертанын зайымчысы тарабынан акцепт жолу менен түзүлөт.  Офертанын шарттарына ылайык зайымчы тарабынан жүргүзүлгөн аракеттер офертанын акцепти болуп 
саналат, алар Офертанын жана ушул Келишимдин шарттары менен толук жана сөзсүз макулдугу катары каралат. Офертанын шарттарынын акцепти менен Зайымчы «Cash2U» сервиси аркылуу 
насыяны алуу жөнүндө айкын насыя келишимдин шарттары менен сөзсүз макул болот.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>1.3.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Насыяны берүү Зайымчыга берилген Насыянын чегинин (лимиттин) алкагында МТи аркылуу жөнөтүлүүчү Зайымчынын өтүнмөсү боюнча Товарга төлөнчү эсепке соода уюмунун
 пайдасына акчаларды чегерүү жолу аркылуу жүргүзүлөт. Зайымчы тарабынан алынган товарларга төлөм МНК жана соода уюму аныктаган шарттарда жүргүзүлөт. Эгерде 
 Келишимде башкалар каралбаса, Товарга төлөнгөн учурдан тартып насыя берилди деп эсептелет.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>1.4.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Насыяны жоюу Зайымчы тарабынан Насыяны алгандан кийинки айдан тартып 3 (ай) аралыгында тең төлөмдөр менен МТда көрсөтүлгөн реквизиттер боюнча накталай эмес ыкмада 
жүргүзүлөт, мында Зайымчы айдын төмөнкү числололорунан: 5, 10, 15, 20 тандоого укуктуу. Насыяны жоюунун алгачкы төгүмү Насыяны алгандан кийинки айдын Зайымчы тарабынан
 көрсөтүлгөн числосунан кечиктирилбестен жүргүзүлөт жана Насыяны андан ары жоюуга төгүмдөр тийиштүү айдын көрсөтүлгөн числосунда жүргүзүлөт.
</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>1.5.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Насыяны жоюу графиги берилген Насыянын ар бир траншына түзүлөт жана МТнын “Жоюулар графиги” бөлүмүндө көрүү үчүн жеткиликтүү болот.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;'  lang=RU>1.6.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
МНК тарабынан Зайымчыга берилүүчү насыянын суммасы 5 000ден 15 000ге чейинки сомду түзөт жана Зайымчы тарабынан берилген маалыматка
 жараша МНКнын өзүнүн кароосуна жараша белгиленет. Насыя Зайымчы тарабынан мурунку өтүнмө боюнча карыз жоюулгандан кийин кайра 
 жаңыртылуучу насыя линиясы түрүндө берилет, жоюу суммасы насыя чегинин алкагында Зайымчы тарабынан кайталап пайдалануу үчүн жеткиликтүү. 
 Зайымчы МТ аркылуу жөнөтүлгөн өтүнмө боюнча белгиленген чектин алкагында Насыяны пайдаланууга укуктуу. Насыя чеги Зайымчыга насыя чеги 
 бекитилген учурдан тартып 24 (жыйырма төрт) айлык мөөнөттө берилет.
</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>1.7.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Насыя боюнча пайыздык коюм жылына 0% (нөл пайыз) өлчөмүндө белгиленет, натыйжалуу пайыздык коюм жылына 0% (нөл пайыз) түзөт.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>1.8.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Зайымчы тарабынан насыя 30 (отуз) ырааттуу күндүн ичинде пайдаланылбаган учурда Зайымчыга берилген чек анын өтүнмөсү боюнча жокко чыгарылат жана пайдалануу үчүн жеткиликтүү 
болбой калат. Мындай учурда Зайымчы МТ аркылуу МНКга кайрадан өтүнмө менен кайрылууга укуктуу, ал ушул Келишимде белгиленген тартипте каралат.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>1.9.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Ушул Келишимдин иштөө мөөнөтү<b> </b>– Тараптар Келишим боюнча милдеттенмелерин толук аткарганга чейин.
</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>1.10.&nbsp;&nbsp;
МНК Зайымчыга бардык зарыл болгон маалыматтарды аны МТга жайгаштыруу жолу менен берет.
</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>1.11.&nbsp;&nbsp;
Электрондук колтамганы колдонуу Зайымчынын өз колу менен койгон колтамгасынын эквиваленти, Зайымчыны иденттештирүүнүн жана электрондук документтин толуктугун текшерүүнүн куралы болуп саналат.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>1.12.&nbsp;&nbsp;
Офертага жана Келишимге электрондук колтамга менен кол коюу Насыя берүүгө МНКнын оң чечим кабыл алуусунун кепилдиги болуп саналбайт.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>1.13.&nbsp;&nbsp;Зайымчы электрондук колтамганы пайдалануу менен жасалган 
операциялардын жана бүтүмдөрдүн юридикалык маанилүүлүгүн түшүнөт жана кабардар болгон.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>1.14.&nbsp;&nbsp;
Зайымчы электрондук колтамга менен кол коюлган ушул документ (документтердин пакети) кайтарылгыс болуп эсептелерин жана кагаз түрүндөгү документке (документтердин пакетине) тең мааниде тааныларын түшүнөт.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>1.15.&nbsp;&nbsp;
Зайымчы тарабынан ушул Келишимде белгиленген жана Зайымчынын өтүнмөсүнө ылайык келген насыянын негизги 
суммасын мөөнөтүндө кайтарууну узарткандыгы жана/же кайтарбай койгондугу үчүн негизги карыздын узартылган
 суммасынан 1 (бир) % өлчөмүндө узартылган ар бир күн үчүн туум чегерилет. Мында насыянын колдонулган 
 бардык мезгилине чегерилген туумдун өлчөмү, берилген насыянын суммасынын 20%нан (жыйырма пайыз) ашпашы керек.
</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>1.16.&nbsp;&nbsp;
Насыянын максаттуу багыты – соода уюмдарынан Товарларды алуу.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>1.17.&nbsp;&nbsp;
Насыя берүүдө Зайымчы тарабынан төлөнүүчү комиссиялык жыйымдар каралган эмес. Башка комиссиялык жыйымдар МТнын “Документтер” бөлүмүндөгү «Онлайн” насыялык продуктун Паспортунда көрсөтүлгөн.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>1.18.&nbsp;&nbsp;
Зайымчы кандайдыр бир айып санкциясыз насыя боюнча карызды мөөнөтүнөн мурда толук көлөмдө же бөлүп төлөөгө укуктуу. </span></p>

<p class=MsoNormal style='text-align:justify;border:none'><span lang=RU>&nbsp;</span></p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in;border:none'><b><span style='font-family: "Times New Roman"'
lang=RU>2.НАСЫЯНЫ БЕРҮҮ ЖАНА ЖОЮУ ТАРТИБИ </span></b></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><a name="_heading=h.gjdgxs"></a><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>2.1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; МНК соода
 уюму менен МНКнын ортосунда түзүлгөн ушул Келишимдин, Офертанын жана келишимдердин шарттарында Зайымчынын өтүнмөсү боюнча соода уюмунун Товарына төлөм жүргүзөт.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>2.2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
МНК Зайымчыга насыяны Кыргыз Республикасынын улуттук валютасы болгон сом менен берет.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>2.3.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
МНК Зайымчыга ушул келишимде каралган насыяны толук же бөлүп берүүдөн төмөнкү кырдаалдарда жазуу жүзүндө кабарлоого жана баш тартууга укуктуу: </span></p>

<p class=MsoNormal style='margin-left:.5in;text-align:justify;text-indent:-.25in;
border:none'><span lang=RU style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;'>−&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
еэгерде Насыянын суммасы мөөнөтүндө кайтарылбай тургандыгы жөнүндө анык күбөлөндүрүлгөн кырдаал түзүлсө; </span></p>

<p class=MsoNormal style='margin-left:.5in;text-align:justify;text-indent:-.25in;
border:none'><span lang=RU style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;'>−&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
герде Зайымчы МНКга билип туруп өзүнүн финансылык абалы жөнүндө жалган маалымат бергендиги же МНКнын ишениминен кыянаттык менен пайдаланууга башка аракет жасагандыгы аныкталса. </span></p>

<p class=MsoNormal style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>МНК көрсөтүлгөн 
кырдаалдардын бар же жок фактысын өз алдынча аныктайт жана алардын бар экендик далилдерди Зайымчыга берүүгө милдеттүү эмес.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>2.4.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
МНК негизги карыздын суммасын кайтаруунун мөөнөтүн өткөргөндүгү үчүн пайызды чегерүүнү насыянын негизги бересеси боюнча карыз пайда болгон күндөн тартып баштайт жана
 негизги бересе боюнча мөөнөтүн өткөрүүнүн иш жүзүндөгү күндөрдүн санынан жана жылдын 360га теңдеш күндөрүнүн санынан улам эсептелет, негизги бересе боюнча
  карызды толук жойгонго чейин айдагы күндөрдүн саны боюнча ар бир календардык айдын ичинде негизги бересенин мөөнөтү өткөн карыздын суммасына жүргүзүлөт.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>2.5.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Зайымчы насыяны кайтарууну, үстөк айыпты төлөөнү, жыйымдарды жана төлөмдөрдү насыя алынган валютада жүргүзөт. </span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>2.6.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Эгерде Зайымчы жоюуга тийиш болгон сумманы толук төкпөсө, анда төгө турган акча каражатын бөлүштүрүү МНК тарабынан Кыргыз Республикасынын колдонуудагы мыйзамдарында белгиленге насыя боюнча төлөмдөрдүн кезектүүлүгүнө ылайык жүзөгө ашырылат.
</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>2.7.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
МНК мындан ары насыя берүүнү токтотууга жана/же аны мөөнөтүнөн мурда жоюуну талап кылууга төмөнкү учурларда укуктуу: </span></p>

<p class=MsoNormal style='margin-left:.5in;text-align:justify;text-indent:-.25in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU style='font-family:"Noto Sans Symbols"'>−&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Зайымчы ушул Келишимдин шарттарына ылайык төлөөгө тийиш болгон кандайдыр бир сумманы төлөй албай турган абалда болуп калганда; </span></p>

<p class=MsoNormal style='margin-left:.5in;text-align:justify;text-indent:-.25in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU style='font-family:"Noto Sans Symbols"'>−&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Зайымчы ушул Келишим боюнча өзүнүн милдеттенмелерин кандайдыр бир башка бузууларга жол бергенде; </span></p>

<p class=MsoNormal style='margin-left:.5in;text-align:justify;text-indent:-.25in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU style='font-family:"Noto Sans Symbols"'>−&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Зайымчы тарабынан ушул Келишимдин шарттарына ылайык МНКга берилген маалымдар, маалым каттар, документтер ж.б. анык эмес болуп калганда;
 </span></p>

<p class=MsoNormal style='margin-left:.5in;text-align:justify;text-indent:-.25in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU style='font-family:"Noto Sans Symbols"'>−&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Зайымчы же ага көз каранды адам төлөөгө жөндөмсүз деп таанылганда, мыйзамды бузгандыгы үчүн мүлкүн (кирешесин) конфискациялоо менен жазык же администрациялык жоопкерчиликке тартылганда, ал мүлктүн (кирешенин) өлчөмүн МНК олуттуу деп тааныганда;</span></p>

<p class=MsoNormal style='margin-left:.5in;text-align:justify;text-indent:-.25in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU style='font-family:"Noto Sans Symbols"'>−&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Зайымчыга (же ага көз каранды адамдарга) акчалай сумманы төлөө жөнүндө же мүлкүн талап кылгандыгы тууралуу доо коюлганда, анын өлчөмүн МНК олуттуу деп тааныганда.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>2.8.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Ушул Келишимдин 2.7. пунктунда көрсөтүлгөн учурларда, МНК мындан ары насыя берүүнү токтотуу жөнүндө жана/же насыянын суммасын мөөнөтүнөн мурда кайтаруу тууралуу Зайымчыга кабарландыруу жиберет.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>2.9.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Зайымчы насыяны төлөөнү, үстөк айыпты төлөөнү, жыйымдарды жана төлөмдөрдү, МТда көрсөтүлгөн методдор менен накталай эмес формада которуу жолу аркылуу жүргүзүүгө милдеттенет.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>2.10.&nbsp;&nbsp;
Тараптар ушул Келишим аркылуу Зайымчы ушул Келишим боюнча өзүнүн милдеттерин тийиштүү эмес аткарганда жана/же аткарбаганда, МНК насыяны кайтарып 
алууну камсыз кылуу максатында жалпыга маалымдоо каражаттарына маалыматтарды жайгаштырып, үчүнчү жактарга өзүнө каалагандай ыңгайлуу ыкма менен Зайымчы жөнүндө бардык маалыматты берүүгө укуктуу экендигин белгилешти.</span></p>

<p class=MsoNormal style='text-align:justify;border:none'><span lang=RU>&nbsp;</span></p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in;border:none'><b><span style='font-family: "Times New Roman"'
lang=RU>3.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
ТАРАПТАРДЫН МИЛДЕТТЕРИ ЖАНА УКУКТАРЫ</span></b></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU>3.1.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><b><span style='font-family: "Times New Roman"' lang=RU>Зайымчы ушул Келишим боюнча милдеттенет:</span></b></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>3.1.1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Төлөмдөрдүн графигине ылайык насыянын негизги суммасын кайтарууну жүргүзүүгө.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>3.1.2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Дарегин, байланыш маалымдарын (уюлдук байланыш, телефон, электрондук почта) өзгөрткөндүгү жөнүндө жана ушул Келишим боюнча өзүнүн милдеттерин тийиштүү аткаруу менен байланышкан бардык башка
 кырдаалдар жөнүндө МНКга токтоосуз кабарлоого, ошондой эле ушул Келишим боюнча шарттарды жана милдеттерди тийиштүү эмес аткарууга жана/же аткарбоого алып келе турган кандай гана болбосун себептерди четтетүүгө бардык чараларды көрүүгө.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>3.1.3.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
МНКнын биринчи талабы боюнча ушул Келишимде же Кыргыз Республикасынын мыйзамдарында каралган учурларда насыяны токтоосуз жоюуну жүргүзүүгө.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>3.1.4.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Товарды кайтарып берген учурда, эгерде соода уюму тарабынан которулган сумма займ боюнча карыздын калдыгын жоё албаган учурда, карыздын калдыгын жоюу үчүн МНКга кайрылууга.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>3.1.5.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Ушул Келишимди аткарбаган же тийиштүү эмес аткарган учурда МНКнын бардык чыгымдарын, анын ичинде транспорт чыгымдарын жана тышкы консультанттарга жумшалган, карызды өндүрүүгө байланыштуу МНК
 тарткан, ошондой эле сүйлөшүүлөрдү даярдоо, иликтөө жана жүргүзүү, документтерди каттоо, нотариалдык күбөлөндүрүү, башкаруу, түзөтүүлөрдү киргизүү, токтотуу, ушул Келишимдин жана ушул Келишимге
  тиешеси бар, башка келишимдин шарттарын жокко чыгаруу жана мажбурлап аткаруу, МНКнын укуктарын сактоо менен байланышкан чыгымдарды төлөөгө.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>3.1.6.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Офертада жана Офертанын курамына кирген документтерде каралган башка милдеттерди аткарууга.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>3.1.7.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Офертага жана (же) тарифтерге киргизилген өзгөртүүлөргө өз алдынча көз салууга.</span></p>

<p class=MsoNormal style='text-align:justify;border:none'><span lang=RU>&nbsp;</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU>3.2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><b><span style='font-family: "Times New Roman"' lang=RU>Зайымчы укуктуу</span></b></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>3.2.1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Насыялоонун алкагында мыйзамдардын талаптарына ылайык товарды кайтарууга.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>3.2.2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Насыяны бөлүп же толук көлөмдө каалаган мезгилде үстөк айыпты же башка айып санкцияларынын түрлөрүн төлөбөстөн мөөнөтүнөн мурда жоюуга.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>3.2.3.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Келишим боюнча Товар үчүн төлөмдү ишке ашырганга чейин Айкын насыялык келишимге кол койгон учурдан тартып насыяны алуудан кайтарымсыз негизде баш тартууга.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>3.2.4.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Айкын насыя келишимине, анын бардык тиркелген документтерине жетүүгө жана убакытты чектебестен МНКнын чегинен тышкары юридикалык консультация алуу үчүн кайрылууга.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>3.2.5.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
ПНасыя, туум, айып санкциялар боюнча төлөмдөрдүн эсептеринин тартиби тууралуу түшүндүрмө алууга.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>3.2.6.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Насыя келишими түзүлө турган (таризделген) тилди (мамлекеттик же расмий) тандоого.</span></p>

<p class=MsoNormal style='text-align:justify;border:none'><span lang=RU>&nbsp;</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>3.3.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
МНК ушул Келишим боюнча милдеттенет:</span></b></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>3.3.1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Зайымчынын суроо-талабы боюнча үч жумушчу күндүн ичинде Зайымчы МНКдан насыя ала тургандыгы жөнүндө жана Зайымчы ушул насыя боюнча насыялык тартипти, аны башка финансылык-насыялык мекемеге берүү
 үчүн сактоо тууралуу маалыматты берүүгө.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>3.3.2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Зайымчынын насыясы боюнча айыпты өндүрүү кайрылуусунун процедурасы башталгандыгы жөнүндө Кабарлоону жиберген учурдан тартып 15 күндүн ичинде үстөк айыпты (айыптарды, туумду) чегерүүнү токтотууга.</span></p>

<p class=MsoNormal style='text-align:justify;border:none'><span lang=RU>&nbsp;</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU>3.4.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><b><span style='font-family: "Times New Roman"' lang=RU>МНК укуктуу</span></b></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>3.4.1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Зайымчыга Насыя берүүдөн баш тартууга жана кабыл алынган чечимге тиешелүү маалыматты бербөөгө;</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>3.4.2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Офертага жана (же) тарифтерге өзгөртүүнү жана толуктоону бир тараптуу тартипте киргизүүгө. Офертаны жана (же) тарифтерди өзгөрткөндүгү жөнүндө Зайымчыны кабарландырууну МНК Офертаны жана (же)
 тарифтерди өзгөртүү/жаңы редакциясы күчүнө кире турган датага чейин 10 (он) күндөн кечиктирбестен, Офертанын жана (же) тарифтердин өзгөртүүлөрүнүн/жаңы редакциясынын текстин МНКнын сайтына жана МТга жайгаштыруу жолу менен ишке ашырылат. Офертанын жана (же) тарифтердин кандай гана болбосун өзгөртүүлөрү аларды күчүнө киргизген датадан тартып тараптар үчүн милдеттүү болот;</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>3.4.3.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Насыянын кезектеги бөлүгүн кайтаруу үчүн белгиленген мөөнөттү Зайымчы бузган учурда насыянын суммасын мөөнөтүнөн мурда талап кылууга, эгерде башкалар Кыргыз Республикасынын мыйзамдарынын талаптарында каралбаса;</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>3.4.4.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Келишимди түзүүнүн жана аткаруунун жүрүшүндө алынган Зайымчы жөнүндө маалыматты үчүнчү жактарга, анын ичинде Келишим боюнча МНК өзүнүн укуктарын үчүнчү жактарга өткөрүп берген учурда берүүгө.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>3.4.5.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Зайымчынын макулдугусуз үчүнчү жактарга Келишим боюнча талаптарды өткөрүп берүүгө.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>3.4.6.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Зайымчы менен телефондук сүйлөшүүлөрдү жаздырууга жана МНК менен Зайымчынын ортосундагы талаштарды кароо учурунда, ошондой эле башка учурларда далил катары үн жазууларды/видеожазууларды пайдаланууга.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>3.4.7.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Зайымчыга МНКнын ишмердиги, продуктулары жана кызмат көрсөтүүлөрү жөнүндө маалыматты жөнөтүүгө.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>3.4.8.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Мыйзамдарда жана Келишимде каралган башка аракеттерди жасоого.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>3.4.9.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Келишим боюнча милдеттерди аткаруунун мөөнөтүн өткөрүүгө Зайымчы жол берген учурда, өзүнүн кароосу боюнча карызды сотко чейинки өндүрүүгө жана коллектордук агенттикке же адистештирилген уюмга жөнгө салууга өткөрүп берүүгө.</span></p>

<p class=MsoNormal style='text-align:justify;border:none'><span lang=RU>&nbsp;</span></p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in;border:none'><b><span style='font-family: "Times New Roman"'
lang=RU>4.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
ФОРС-МАЖОР</span></b></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>4.1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Тараптар жеңүүгө мүмкүн болгон жагдайлар (форс-мажор) жаралган учурда ушул Келишим боюнча милдеттерин айрым же толук аткарбагандыгы үчүн жоопкерчиликтен бошотулат,
 ага төмөнкүлөр кирет: табигый кырсыктар, өрт, суу ташкыны, аскердик аракеттер, ушул Келишимде көрсөтүлгөн ишмердүүлүктүн түрүн же ушул Келишим боюнча өздөрүнүн 
 милдеттерин Тараптардын аткаруусуна тоскоолдук кылган, башка жагдайлардан улам жаралган Тараптардын акыл-эстен тышкаркы контролдоосуна түз же кыйыр тыюу салган, 
 тараптардын бири аткарууга милдеттүү болгон, мыйзам актыларынын, бийлик жана башкаруу органдарынын актыларынын күчүнө кириши.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>4.2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Форс-мажордук жагдай жаралган Тарап, мындай жагдайдын жаралгандыгы жөнүндө Форс-мажордук жагдай жаралган учурдан тартып үч күндүн ичинде кабарлоого милдеттүү.
 Форс-мажордук жагдай ыйгарым укуктуу мамлекеттик органдардын документтери менен тастыкталышы керек.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>4.3.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Форс-мажордук жагдай жаралган учурларда ушул Келишим боюнча милдеттерди аткаруу мөөнөтү, мындай жагдай жана анын кесепеттери болуп турган убакытка теңдеш жылдырылат.</span></p>

<p class=MsoNormal style='text-align:justify;border:none'><a
name="_heading=h.30j0zll"></a><span lang=RU>&nbsp;</span></p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in;border:none'><b><span style='font-family: "Times New Roman"'
lang=RU>5.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
БАШКА ЖОБОЛОР </span></b></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>5.1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Ушул Келишимди мөөнөтүнөн мурда бузуу тараптардын макулдашуусу боюнча, ошондой эле ушул Келишимде каралган учурларда же Кыргыз Республикасынын жарандык мыйзамдарында каралган башка негиздер боюнча болушу мүмкүн.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>5.2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Эгерде ушул Келишимдин кайсы бир жобосу кайсы бир мамилеге жана кайсы бир мыйзам боюнча жараксыз, мыйзамсыз же колдонууга жараксыз болуп калса, калган жоболордун
 колдонуу күчү, мыйзамдуулугу же колдонууга жарактуулугу кайсы бир мүнөздө төмөндөтүлүшү же жокко чыгарылбашы керек. Мындай учурларда зарылчылыгына жараша МНК 
 Кыргыз Республикасынын мыйзамдарына ылайык, жараксыз, мыйзамсыз, башка жобо менен жарактуу, мыйзамдуу жана мазмуну жана колдонуу багыты боюнча ушул Келишимге 
 окшош жобону алмаштыруу үчүн ушул Келишимге, Тараптардын кызыкчылыктарына жана Кыргыз Республикасынын мыйзамдарына ылайык келген өзгөртүү киргизет.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>5.3.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Ушул Келишим боюнча же ага байланыштуу Тараптардын ортосунда жаралган бардык талаштар же пикир келишпестиктер, Тараптардын ортосунда сүйлөшүү жолу менен чечилет. 
Тараптар ушул Келишим менен, пикир келишпестиктерди сүйлөшүү жолу менен чечүү мүмкүн болбогон учурда, ушул Келишим менен байланышкан жана/же жаралган бардык талаштар, 
анын ичинде ушул Келишимди түзүү, бузуу, токтотуу, жокко чыгаруу же жараксыздыгына тиешелүү талаштар Кыргыз Республикасынын мыйзамдарында белгиленген тартипте МНКнын 
жайгашкан жери боюнча сотто чечилүүгө тийиш.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>5.4.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Зайымчы жашаган дарегин өзгөрткөндүгү жөнүндө байланыш маалыматтарын кабарлабаган учурда Зайымчы көрсөткөн дарек же башка байланыш маалыматы боюнча МНК тапшырык 
почта менен жөнөткөн кабарландыруу Зайымчы алды деп эсептелинет.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
background:white;border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>5.5.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Ушул Келишимде каралбагандардын бардык учурларында, Тараптар Кыргыз Республикасынын мыйзамдарын жетекчиликке алат.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
background:white;border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>5.6.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Эгерде ушул Келишим менен Офертанын жобосунан айырмаланган башка жобо белгиленген учурда, ушул Келишимдин жобосу артыкчылыкка ээ болот, ага ылайык Офертанын жобосу бул бөлүктө Зайымчыга 
колдонулбайт же ушул Келишимде баяндалган шарттардын мазмунуна жараша өзгөрөт.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
background:white;border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>5.7.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Зайымчы Офертанын шартын негизинен кабыл алат жана төмөнкүлөрдү тастыктайт:</span></p>

<p class=MsoNormal style='margin-left:49.65pt;text-align:justify;text-indent:
-21.3pt;border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU style='font-family:"Noto Sans Symbols"'>−&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Офертанын курамына кирген бардык документтер окулду, кандайдыр бир сын-пикир жана каршылык жок толук көлөмдө Зайымчы кабыл алды, алар Зайымчынын акыл-эстүү түшүнгөн кызыкчылыктарынын негизинде кабыл алынбаса Зайымчы үчүн кандайдыр бир оор шарттарды камтыбайт, </span></p>

<p class=MsoNormal style='margin-left:49.65pt;text-align:justify;text-indent:
-21.3pt;border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU style='font-family:"Noto Sans Symbols"'>−&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
ушул Келишим жалпысынан Оферта менен бирдиктүү документ болуп эсептелет;</span></p>

<p class=MsoNormal style='margin-left:49.65pt;text-align:justify;text-indent:
-21.3pt;border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU style='font-family:"Noto Sans Symbols"'>−&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
эгерде Офертанын шарттарына ылайык жөнөкөй электрондук колтамга менен документтерге кол коюлса, Оферта жана ушул Келишим алар тарабынан окулган/түшүнүлгөн/кабыл алынган жок деп далил катары Зайымчы Офертада жана ушул Келишимде анын колтамгасы жок экендигине шылтоого укуксуз;
</span></p>

<p class=MsoNormal style='margin-left:49.65pt;text-align:justify;text-indent:
-21.3pt;background:white;border:none'><span lang=RU style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;'>−&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Келишимди түзүү жана анын шарттарын аткаруу, анын ичинде Келишим боюнча Насыяны берүү, Кыргыз Республикасынын мыйзамдарын бузбайт жана кайсы бир ченемин бузууга алып келбейт.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
background:white;border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>5.8.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
МНК Зайымчыда жаралган, анын ичинде Зайымчы Офертанын шарттары жана (же) тарифтер жана (же) Офертага жана (же) тарифтерге киргизилген өзгөртүүлөр жана толуктоолор менен таанышпагандыгы 
жана (же) өз убагында таанышпагандыгы менен байланышкан бардык жоготууларга жоопкерчилик тартпайт.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
background:white;border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>5.9.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Оферта жана Офертанын курамына кирген документтер менен Зайымчы төмөнкү шилтеме боюнча: www.cahs2u.kg МНКнын сайтынан жана МТда тааныша алат.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
background:white;border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>5.10.&nbsp;&nbsp;
Бардык болгон суроолор боюнча Зайымчы күнү-түнү иштеген Байланыш-борборуна – 0552 55 33 33, 0502 55 33 33, 0772 55 33 33 телефондору боюнча кайрылса же МНКга кайрылса болот.</span></p>

<p class=MsoNormal style='text-align:justify;background:white;border:none'><span
lang=RU>&nbsp;</span></p>

</div>

</body>

</html>

`