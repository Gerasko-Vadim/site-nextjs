export const rulesPromotion = `
<html>
    <head>
        <meta content="text/html; charset=UTF-8" http-equiv="content-type" />
        <style type="text/css">
            .lst-kix_ddndwvtx2ln1-4 > li:before {
                content: "" counter(lst-ctn-kix_ddndwvtx2ln1-4, lower-latin) ". ";
            }
            .lst-kix_ddndwvtx2ln1-6 > li:before {
                content: "" counter(lst-ctn-kix_ddndwvtx2ln1-6, decimal) ". ";
            }
            .lst-kix_ddndwvtx2ln1-1 > li:before {
                content: "" counter(lst-ctn-kix_ddndwvtx2ln1-1, lower-latin) ". ";
            }
            .lst-kix_ddndwvtx2ln1-5 > li:before {
                content: "" counter(lst-ctn-kix_ddndwvtx2ln1-5, lower-roman) ". ";
            }
            ol.lst-kix_chbwffh3ouj5-1.start {
                counter-reset: lst-ctn-kix_chbwffh3ouj5-1 0;
            }
            .lst-kix_ddndwvtx2ln1-2 > li:before {
                content: "" counter(lst-ctn-kix_ddndwvtx2ln1-2, lower-roman) ". ";
            }
            .lst-kix_aty5ge91rj4b-6 > li {
                counter-increment: lst-ctn-kix_aty5ge91rj4b-6;
            }
            .lst-kix_ddndwvtx2ln1-3 > li:before {
                content: "" counter(lst-ctn-kix_ddndwvtx2ln1-3, decimal) ". ";
            }
            ol.lst-kix_aao4at5asie2-0.start {
                counter-reset: lst-ctn-kix_aao4at5asie2-0 3;
            }
            .lst-kix_az2l5z5h56d8-5 > li {
                counter-increment: lst-ctn-kix_az2l5z5h56d8-5;
            }
            .lst-kix_chbwffh3ouj5-2 > li {
                counter-increment: lst-ctn-kix_chbwffh3ouj5-2;
            }
            ol.lst-kix_pg8i7jvff8f1-6.start {
                counter-reset: lst-ctn-kix_pg8i7jvff8f1-6 0;
            }
            .lst-kix_pg8i7jvff8f1-6 > li {
                counter-increment: lst-ctn-kix_pg8i7jvff8f1-6;
            }
            .lst-kix_aao4at5asie2-3 > li {
                counter-increment: lst-ctn-kix_aao4at5asie2-3;
            }
            .lst-kix_ddndwvtx2ln1-8 > li:before {
                content: "" counter(lst-ctn-kix_ddndwvtx2ln1-8, lower-roman) ". ";
            }
            .lst-kix_ddndwvtx2ln1-7 > li:before {
                content: "" counter(lst-ctn-kix_ddndwvtx2ln1-7, lower-latin) ". ";
            }
            ol.lst-kix_t0enf8iir7uo-4.start {
                counter-reset: lst-ctn-kix_t0enf8iir7uo-4 0;
            }
            .lst-kix_az2l5z5h56d8-5 > li:before {
                content: "" counter(lst-ctn-kix_az2l5z5h56d8-5, lower-roman) ". ";
            }
            .lst-kix_az2l5z5h56d8-6 > li:before {
                content: "" counter(lst-ctn-kix_az2l5z5h56d8-6, decimal) ". ";
            }
            .lst-kix_t0enf8iir7uo-7 > li {
                counter-increment: lst-ctn-kix_t0enf8iir7uo-7;
            }
            .lst-kix_az2l5z5h56d8-4 > li:before {
                content: "" counter(lst-ctn-kix_az2l5z5h56d8-4, lower-latin) ". ";
            }
            .lst-kix_az2l5z5h56d8-8 > li:before {
                content: "" counter(lst-ctn-kix_az2l5z5h56d8-8, lower-roman) ". ";
            }
            .lst-kix_aty5ge91rj4b-8 > li {
                counter-increment: lst-ctn-kix_aty5ge91rj4b-8;
            }
            .lst-kix_aao4at5asie2-5 > li {
                counter-increment: lst-ctn-kix_aao4at5asie2-5;
            }
            .lst-kix_az2l5z5h56d8-3 > li {
                counter-increment: lst-ctn-kix_az2l5z5h56d8-3;
            }
            .lst-kix_az2l5z5h56d8-7 > li:before {
                content: "" counter(lst-ctn-kix_az2l5z5h56d8-7, lower-latin) ". ";
            }
            .lst-kix_pg8i7jvff8f1-8 > li {
                counter-increment: lst-ctn-kix_pg8i7jvff8f1-8;
            }
            ol.lst-kix_5hs78sc92mfd-4.start {
                counter-reset: lst-ctn-kix_5hs78sc92mfd-4 0;
            }
            .lst-kix_az2l5z5h56d8-0 > li:before {
                content: "" counter(lst-ctn-kix_az2l5z5h56d8-0, decimal) ". ";
            }
            .lst-kix_az2l5z5h56d8-1 > li:before {
                content: "" counter(lst-ctn-kix_az2l5z5h56d8-1, lower-latin) ". ";
            }
            .lst-kix_az2l5z5h56d8-2 > li:before {
                content: "" counter(lst-ctn-kix_az2l5z5h56d8-2, lower-roman) ". ";
            }
            .lst-kix_az2l5z5h56d8-3 > li:before {
                content: "" counter(lst-ctn-kix_az2l5z5h56d8-3, decimal) ". ";
            }
            ol.lst-kix_pg8i7jvff8f1-0.start {
                counter-reset: lst-ctn-kix_pg8i7jvff8f1-0 5;
            }
            ol.lst-kix_t0enf8iir7uo-6 {
                list-style-type: none;
            }
            ol.lst-kix_t0enf8iir7uo-5 {
                list-style-type: none;
            }
            ol.lst-kix_az2l5z5h56d8-5.start {
                counter-reset: lst-ctn-kix_az2l5z5h56d8-5 0;
            }
            ol.lst-kix_t0enf8iir7uo-8 {
                list-style-type: none;
            }
            ol.lst-kix_t0enf8iir7uo-7 {
                list-style-type: none;
            }
            .lst-kix_chbwffh3ouj5-0 > li {
                counter-increment: lst-ctn-kix_chbwffh3ouj5-0;
            }
            ol.lst-kix_t0enf8iir7uo-2 {
                list-style-type: none;
            }
            .lst-kix_t0enf8iir7uo-3 > li {
                counter-increment: lst-ctn-kix_t0enf8iir7uo-3;
            }
            ol.lst-kix_t0enf8iir7uo-1 {
                list-style-type: none;
            }
            ol.lst-kix_pg8i7jvff8f1-1.start {
                counter-reset: lst-ctn-kix_pg8i7jvff8f1-1 0;
            }
            ol.lst-kix_t0enf8iir7uo-4 {
                list-style-type: none;
            }
            ol.lst-kix_t0enf8iir7uo-5.start {
                counter-reset: lst-ctn-kix_t0enf8iir7uo-5 0;
            }
            ol.lst-kix_chbwffh3ouj5-6.start {
                counter-reset: lst-ctn-kix_chbwffh3ouj5-6 0;
            }
            ol.lst-kix_t0enf8iir7uo-3 {
                list-style-type: none;
            }
            ol.lst-kix_aty5ge91rj4b-5.start {
                counter-reset: lst-ctn-kix_aty5ge91rj4b-5 0;
            }
            .lst-kix_chbwffh3ouj5-6 > li {
                counter-increment: lst-ctn-kix_chbwffh3ouj5-6;
            }
            ol.lst-kix_aao4at5asie2-5.start {
                counter-reset: lst-ctn-kix_aao4at5asie2-5 0;
            }
            ol.lst-kix_t0enf8iir7uo-0 {
                list-style-type: none;
            }
            .lst-kix_aty5ge91rj4b-2 > li {
                counter-increment: lst-ctn-kix_aty5ge91rj4b-2;
            }
            .lst-kix_t0enf8iir7uo-7 > li:before {
                content: "" counter(lst-ctn-kix_t0enf8iir7uo-7, lower-latin) ". ";
            }
            .lst-kix_t0enf8iir7uo-8 > li:before {
                content: "" counter(lst-ctn-kix_t0enf8iir7uo-8, lower-roman) ". ";
            }
            .lst-kix_t0enf8iir7uo-6 > li:before {
                content: "" counter(lst-ctn-kix_t0enf8iir7uo-6, decimal) ". ";
            }
            .lst-kix_t0enf8iir7uo-4 > li:before {
                content: "" counter(lst-ctn-kix_t0enf8iir7uo-4, lower-latin) ". ";
            }
            ol.lst-kix_chbwffh3ouj5-0.start {
                counter-reset: lst-ctn-kix_chbwffh3ouj5-0 6;
            }
            ol.lst-kix_5hs78sc92mfd-3.start {
                counter-reset: lst-ctn-kix_5hs78sc92mfd-3 0;
            }
            .lst-kix_ddndwvtx2ln1-1 > li {
                counter-increment: lst-ctn-kix_ddndwvtx2ln1-1;
            }
            .lst-kix_t0enf8iir7uo-5 > li:before {
                content: "" counter(lst-ctn-kix_t0enf8iir7uo-5, lower-roman) ". ";
            }
            ol.lst-kix_aao4at5asie2-6.start {
                counter-reset: lst-ctn-kix_aao4at5asie2-6 0;
            }
            ol.lst-kix_aty5ge91rj4b-4.start {
                counter-reset: lst-ctn-kix_aty5ge91rj4b-4 0;
            }
            ol.lst-kix_az2l5z5h56d8-4.start {
                counter-reset: lst-ctn-kix_az2l5z5h56d8-4 0;
            }
            .lst-kix_ddndwvtx2ln1-0 > li:before {
                content: "" counter(lst-ctn-kix_ddndwvtx2ln1-0, decimal) ". ";
            }
            .lst-kix_pg8i7jvff8f1-8 > li:before {
                content: "" counter(lst-ctn-kix_pg8i7jvff8f1-8, lower-roman) ". ";
            }
            .lst-kix_5hs78sc92mfd-0 > li {
                counter-increment: lst-ctn-kix_5hs78sc92mfd-0;
            }
            ol.lst-kix_chbwffh3ouj5-7.start {
                counter-reset: lst-ctn-kix_chbwffh3ouj5-7 0;
            }
            .lst-kix_pg8i7jvff8f1-1 > li {
                counter-increment: lst-ctn-kix_pg8i7jvff8f1-1;
            }
            ol.lst-kix_5hs78sc92mfd-2.start {
                counter-reset: lst-ctn-kix_5hs78sc92mfd-2 0;
            }
            ol.lst-kix_t0enf8iir7uo-3.start {
                counter-reset: lst-ctn-kix_t0enf8iir7uo-3 0;
            }
            .lst-kix_pg8i7jvff8f1-6 > li:before {
                content: "" counter(lst-ctn-kix_pg8i7jvff8f1-6, decimal) ". ";
            }
            ol.lst-kix_aao4at5asie2-7.start {
                counter-reset: lst-ctn-kix_aao4at5asie2-7 0;
            }
            ol.lst-kix_ddndwvtx2ln1-0.start {
                counter-reset: lst-ctn-kix_ddndwvtx2ln1-0 0;
            }
            ol.lst-kix_aty5ge91rj4b-0.start {
                counter-reset: lst-ctn-kix_aty5ge91rj4b-0 1;
            }
            ol.lst-kix_t0enf8iir7uo-0.start {
                counter-reset: lst-ctn-kix_t0enf8iir7uo-0 7;
            }
            .lst-kix_t0enf8iir7uo-0 > li:before {
                content: "" counter(lst-ctn-kix_t0enf8iir7uo-0, decimal) ". ";
            }
            ol.lst-kix_az2l5z5h56d8-3.start {
                counter-reset: lst-ctn-kix_az2l5z5h56d8-3 0;
            }
            .lst-kix_pg8i7jvff8f1-2 > li:before {
                content: "" counter(lst-ctn-kix_pg8i7jvff8f1-2, lower-roman) ". ";
            }
            .lst-kix_pg8i7jvff8f1-4 > li:before {
                content: "" counter(lst-ctn-kix_pg8i7jvff8f1-4, lower-latin) ". ";
            }
            ol.lst-kix_aty5ge91rj4b-3.start {
                counter-reset: lst-ctn-kix_aty5ge91rj4b-3 0;
            }
            .lst-kix_t0enf8iir7uo-2 > li:before {
                content: "" counter(lst-ctn-kix_t0enf8iir7uo-2, lower-roman) ". ";
            }
            .lst-kix_pg8i7jvff8f1-0 > li:before {
                content: "" counter(lst-ctn-kix_pg8i7jvff8f1-0, decimal) ". ";
            }
            ol.lst-kix_az2l5z5h56d8-0.start {
                counter-reset: lst-ctn-kix_az2l5z5h56d8-0 2;
            }
            .lst-kix_aao4at5asie2-8 > li:before {
                content: "" counter(lst-ctn-kix_aao4at5asie2-8, lower-roman) ". ";
            }
            ol.lst-kix_chbwffh3ouj5-5.start {
                counter-reset: lst-ctn-kix_chbwffh3ouj5-5 0;
            }
            .lst-kix_aao4at5asie2-6 > li:before {
                content: "" counter(lst-ctn-kix_aao4at5asie2-6, decimal) ". ";
            }
            .lst-kix_aty5ge91rj4b-5 > li:before {
                content: "" counter(lst-ctn-kix_aty5ge91rj4b-5, lower-roman) ". ";
            }
            .lst-kix_chbwffh3ouj5-8 > li:before {
                content: "" counter(lst-ctn-kix_chbwffh3ouj5-8, lower-roman) ". ";
            }
            .lst-kix_pg8i7jvff8f1-2 > li {
                counter-increment: lst-ctn-kix_pg8i7jvff8f1-2;
            }
            .lst-kix_aao4at5asie2-2 > li:before {
                content: "" counter(lst-ctn-kix_aao4at5asie2-2, lower-roman) ". ";
            }
            .lst-kix_aty5ge91rj4b-7 > li:before {
                content: "" counter(lst-ctn-kix_aty5ge91rj4b-7, lower-latin) ". ";
            }
            .lst-kix_aao4at5asie2-4 > li:before {
                content: "" counter(lst-ctn-kix_aao4at5asie2-4, lower-latin) ". ";
            }
            .lst-kix_chbwffh3ouj5-6 > li:before {
                content: "" counter(lst-ctn-kix_chbwffh3ouj5-6, decimal) ". ";
            }
            .lst-kix_pg8i7jvff8f1-0 > li {
                counter-increment: lst-ctn-kix_pg8i7jvff8f1-0;
            }
            ol.lst-kix_chbwffh3ouj5-2.start {
                counter-reset: lst-ctn-kix_chbwffh3ouj5-2 0;
            }
            .lst-kix_chbwffh3ouj5-8 > li {
                counter-increment: lst-ctn-kix_chbwffh3ouj5-8;
            }
            .lst-kix_ddndwvtx2ln1-3 > li {
                counter-increment: lst-ctn-kix_ddndwvtx2ln1-3;
            }
            .lst-kix_t0enf8iir7uo-1 > li {
                counter-increment: lst-ctn-kix_t0enf8iir7uo-1;
            }
            .lst-kix_aao4at5asie2-0 > li:before {
                content: "" counter(lst-ctn-kix_aao4at5asie2-0, decimal) ". ";
            }
            ol.lst-kix_ddndwvtx2ln1-5 {
                list-style-type: none;
            }
            ol.lst-kix_ddndwvtx2ln1-6 {
                list-style-type: none;
            }
            ol.lst-kix_ddndwvtx2ln1-7 {
                list-style-type: none;
            }
            ol.lst-kix_ddndwvtx2ln1-8 {
                list-style-type: none;
            }
            ol.lst-kix_ddndwvtx2ln1-1 {
                list-style-type: none;
            }
            ol.lst-kix_ddndwvtx2ln1-2 {
                list-style-type: none;
            }
            .lst-kix_ddndwvtx2ln1-2 > li {
                counter-increment: lst-ctn-kix_ddndwvtx2ln1-2;
            }
            ol.lst-kix_ddndwvtx2ln1-3 {
                list-style-type: none;
            }
            ol.lst-kix_5hs78sc92mfd-0.start {
                counter-reset: lst-ctn-kix_5hs78sc92mfd-0 4;
            }
            ol.lst-kix_ddndwvtx2ln1-4 {
                list-style-type: none;
            }
            .lst-kix_5hs78sc92mfd-1 > li:before {
                content: "" counter(lst-ctn-kix_5hs78sc92mfd-1, lower-latin) ". ";
            }
            .lst-kix_5hs78sc92mfd-7 > li {
                counter-increment: lst-ctn-kix_5hs78sc92mfd-7;
            }
            .lst-kix_5hs78sc92mfd-1 > li {
                counter-increment: lst-ctn-kix_5hs78sc92mfd-1;
            }
            ol.lst-kix_az2l5z5h56d8-2.start {
                counter-reset: lst-ctn-kix_az2l5z5h56d8-2 0;
            }
            ol.lst-kix_pg8i7jvff8f1-8 {
                list-style-type: none;
            }
            ol.lst-kix_pg8i7jvff8f1-7 {
                list-style-type: none;
            }
            .lst-kix_chbwffh3ouj5-0 > li:before {
                content: "" counter(lst-ctn-kix_chbwffh3ouj5-0, decimal) ". ";
            }
            .lst-kix_chbwffh3ouj5-4 > li:before {
                content: "" counter(lst-ctn-kix_chbwffh3ouj5-4, lower-latin) ". ";
            }
            ol.lst-kix_pg8i7jvff8f1-4 {
                list-style-type: none;
            }
            .lst-kix_5hs78sc92mfd-2 > li {
                counter-increment: lst-ctn-kix_5hs78sc92mfd-2;
            }
            ol.lst-kix_pg8i7jvff8f1-3 {
                list-style-type: none;
            }
            ol.lst-kix_chbwffh3ouj5-3.start {
                counter-reset: lst-ctn-kix_chbwffh3ouj5-3 0;
            }
            .lst-kix_ddndwvtx2ln1-8 > li {
                counter-increment: lst-ctn-kix_ddndwvtx2ln1-8;
            }
            ol.lst-kix_pg8i7jvff8f1-6 {
                list-style-type: none;
            }
            ol.lst-kix_ddndwvtx2ln1-0 {
                list-style-type: none;
            }
            ol.lst-kix_pg8i7jvff8f1-5 {
                list-style-type: none;
            }
            .lst-kix_5hs78sc92mfd-8 > li {
                counter-increment: lst-ctn-kix_5hs78sc92mfd-8;
            }
            ol.lst-kix_pg8i7jvff8f1-0 {
                list-style-type: none;
            }
            .lst-kix_chbwffh3ouj5-2 > li:before {
                content: "" counter(lst-ctn-kix_chbwffh3ouj5-2, lower-roman) ". ";
            }
            ol.lst-kix_pg8i7jvff8f1-2 {
                list-style-type: none;
            }
            ol.lst-kix_t0enf8iir7uo-2.start {
                counter-reset: lst-ctn-kix_t0enf8iir7uo-2 0;
            }
            ol.lst-kix_pg8i7jvff8f1-1 {
                list-style-type: none;
            }
            .lst-kix_aty5ge91rj4b-3 > li:before {
                content: "" counter(lst-ctn-kix_aty5ge91rj4b-3, decimal) ". ";
            }
            .lst-kix_chbwffh3ouj5-7 > li {
                counter-increment: lst-ctn-kix_chbwffh3ouj5-7;
            }
            ol.lst-kix_aty5ge91rj4b-1.start {
                counter-reset: lst-ctn-kix_aty5ge91rj4b-1 0;
            }
            .lst-kix_aty5ge91rj4b-1 > li:before {
                content: "" counter(lst-ctn-kix_aty5ge91rj4b-1, lower-latin) ". ";
            }
            .lst-kix_t0enf8iir7uo-2 > li {
                counter-increment: lst-ctn-kix_t0enf8iir7uo-2;
            }
            .lst-kix_5hs78sc92mfd-5 > li:before {
                content: "" counter(lst-ctn-kix_5hs78sc92mfd-5, lower-roman) ". ";
            }
            .lst-kix_aty5ge91rj4b-4 > li {
                counter-increment: lst-ctn-kix_aty5ge91rj4b-4;
            }
            .lst-kix_5hs78sc92mfd-3 > li:before {
                content: "" counter(lst-ctn-kix_5hs78sc92mfd-3, decimal) ". ";
            }
            .lst-kix_5hs78sc92mfd-7 > li:before {
                content: "" counter(lst-ctn-kix_5hs78sc92mfd-7, lower-latin) ". ";
            }
            .lst-kix_t0enf8iir7uo-8 > li {
                counter-increment: lst-ctn-kix_t0enf8iir7uo-8;
            }
            ol.lst-kix_az2l5z5h56d8-1.start {
                counter-reset: lst-ctn-kix_az2l5z5h56d8-1 0;
            }
            .lst-kix_chbwffh3ouj5-1 > li {
                counter-increment: lst-ctn-kix_chbwffh3ouj5-1;
            }
            .lst-kix_pg8i7jvff8f1-7 > li {
                counter-increment: lst-ctn-kix_pg8i7jvff8f1-7;
            }
            .lst-kix_aao4at5asie2-4 > li {
                counter-increment: lst-ctn-kix_aao4at5asie2-4;
            }
            ol.lst-kix_pg8i7jvff8f1-3.start {
                counter-reset: lst-ctn-kix_pg8i7jvff8f1-3 0;
            }
            .lst-kix_az2l5z5h56d8-4 > li {
                counter-increment: lst-ctn-kix_az2l5z5h56d8-4;
            }
            .lst-kix_chbwffh3ouj5-3 > li {
                counter-increment: lst-ctn-kix_chbwffh3ouj5-3;
            }
            ol.lst-kix_chbwffh3ouj5-4.start {
                counter-reset: lst-ctn-kix_chbwffh3ouj5-4 0;
            }
            ol.lst-kix_az2l5z5h56d8-7.start {
                counter-reset: lst-ctn-kix_az2l5z5h56d8-7 0;
            }
            ol.lst-kix_ddndwvtx2ln1-7.start {
                counter-reset: lst-ctn-kix_ddndwvtx2ln1-7 0;
            }
            ol.lst-kix_t0enf8iir7uo-7.start {
                counter-reset: lst-ctn-kix_t0enf8iir7uo-7 0;
            }
            ol.lst-kix_az2l5z5h56d8-7 {
                list-style-type: none;
            }
            ol.lst-kix_az2l5z5h56d8-8 {
                list-style-type: none;
            }
            .lst-kix_aty5ge91rj4b-5 > li {
                counter-increment: lst-ctn-kix_aty5ge91rj4b-5;
            }
            .lst-kix_t0enf8iir7uo-6 > li {
                counter-increment: lst-ctn-kix_t0enf8iir7uo-6;
            }
            .lst-kix_pg8i7jvff8f1-5 > li {
                counter-increment: lst-ctn-kix_pg8i7jvff8f1-5;
            }
            .lst-kix_t0enf8iir7uo-4 > li {
                counter-increment: lst-ctn-kix_t0enf8iir7uo-4;
            }
            .lst-kix_az2l5z5h56d8-6 > li {
                counter-increment: lst-ctn-kix_az2l5z5h56d8-6;
            }
            ol.lst-kix_aao4at5asie2-8.start {
                counter-reset: lst-ctn-kix_aao4at5asie2-8 0;
            }
            .lst-kix_chbwffh3ouj5-5 > li {
                counter-increment: lst-ctn-kix_chbwffh3ouj5-5;
            }
            ol.lst-kix_t0enf8iir7uo-1.start {
                counter-reset: lst-ctn-kix_t0enf8iir7uo-1 0;
            }
            ol.lst-kix_aty5ge91rj4b-2.start {
                counter-reset: lst-ctn-kix_aty5ge91rj4b-2 0;
            }
            .lst-kix_ddndwvtx2ln1-0 > li {
                counter-increment: lst-ctn-kix_ddndwvtx2ln1-0;
            }
            ol.lst-kix_5hs78sc92mfd-1.start {
                counter-reset: lst-ctn-kix_5hs78sc92mfd-1 0;
            }
            ol.lst-kix_pg8i7jvff8f1-8.start {
                counter-reset: lst-ctn-kix_pg8i7jvff8f1-8 0;
            }
            ol.lst-kix_ddndwvtx2ln1-8.start {
                counter-reset: lst-ctn-kix_ddndwvtx2ln1-8 0;
            }
            .lst-kix_aty5ge91rj4b-3 > li {
                counter-increment: lst-ctn-kix_aty5ge91rj4b-3;
            }
            .lst-kix_pg8i7jvff8f1-3 > li {
                counter-increment: lst-ctn-kix_pg8i7jvff8f1-3;
            }
            .lst-kix_az2l5z5h56d8-2 > li {
                counter-increment: lst-ctn-kix_az2l5z5h56d8-2;
            }
            .lst-kix_az2l5z5h56d8-8 > li {
                counter-increment: lst-ctn-kix_az2l5z5h56d8-8;
            }
            .lst-kix_aao4at5asie2-2 > li {
                counter-increment: lst-ctn-kix_aao4at5asie2-2;
            }
            .lst-kix_aao4at5asie2-6 > li {
                counter-increment: lst-ctn-kix_aao4at5asie2-6;
            }
            ol.lst-kix_ddndwvtx2ln1-2.start {
                counter-reset: lst-ctn-kix_ddndwvtx2ln1-2 0;
            }
            ol.lst-kix_5hs78sc92mfd-1 {
                list-style-type: none;
            }
            ol.lst-kix_5hs78sc92mfd-0 {
                list-style-type: none;
            }
            ol.lst-kix_pg8i7jvff8f1-2.start {
                counter-reset: lst-ctn-kix_pg8i7jvff8f1-2 0;
            }
            ol.lst-kix_5hs78sc92mfd-5 {
                list-style-type: none;
            }
            ol.lst-kix_5hs78sc92mfd-4 {
                list-style-type: none;
            }
            ol.lst-kix_5hs78sc92mfd-3 {
                list-style-type: none;
            }
            ol.lst-kix_5hs78sc92mfd-2 {
                list-style-type: none;
            }
            .lst-kix_ddndwvtx2ln1-7 > li {
                counter-increment: lst-ctn-kix_ddndwvtx2ln1-7;
            }
            .lst-kix_5hs78sc92mfd-3 > li {
                counter-increment: lst-ctn-kix_5hs78sc92mfd-3;
            }
            .lst-kix_5hs78sc92mfd-6 > li {
                counter-increment: lst-ctn-kix_5hs78sc92mfd-6;
            }
            ol.lst-kix_ddndwvtx2ln1-1.start {
                counter-reset: lst-ctn-kix_ddndwvtx2ln1-1 0;
            }
            .lst-kix_ddndwvtx2ln1-4 > li {
                counter-increment: lst-ctn-kix_ddndwvtx2ln1-4;
            }
            ol.lst-kix_t0enf8iir7uo-6.start {
                counter-reset: lst-ctn-kix_t0enf8iir7uo-6 0;
            }
            ol.lst-kix_aty5ge91rj4b-8 {
                list-style-type: none;
            }
            ol.lst-kix_aao4at5asie2-8 {
                list-style-type: none;
            }
            ol.lst-kix_aty5ge91rj4b-6 {
                list-style-type: none;
            }
            ol.lst-kix_aao4at5asie2-4.start {
                counter-reset: lst-ctn-kix_aao4at5asie2-4 0;
            }
            ol.lst-kix_aty5ge91rj4b-7 {
                list-style-type: none;
            }
            .lst-kix_t0enf8iir7uo-0 > li {
                counter-increment: lst-ctn-kix_t0enf8iir7uo-0;
            }
            .lst-kix_aty5ge91rj4b-0 > li {
                counter-increment: lst-ctn-kix_aty5ge91rj4b-0;
            }
            ol.lst-kix_aty5ge91rj4b-4 {
                list-style-type: none;
            }
            ol.lst-kix_ddndwvtx2ln1-3.start {
                counter-reset: lst-ctn-kix_ddndwvtx2ln1-3 0;
            }
            .lst-kix_pg8i7jvff8f1-7 > li:before {
                content: "" counter(lst-ctn-kix_pg8i7jvff8f1-7, lower-latin) ". ";
            }
            ol.lst-kix_aty5ge91rj4b-5 {
                list-style-type: none;
            }
            ol.lst-kix_aao4at5asie2-4 {
                list-style-type: none;
            }
            ol.lst-kix_aty5ge91rj4b-2 {
                list-style-type: none;
            }
            ol.lst-kix_aao4at5asie2-5 {
                list-style-type: none;
            }
            ol.lst-kix_aty5ge91rj4b-3 {
                list-style-type: none;
            }
            ol.lst-kix_aao4at5asie2-6 {
                list-style-type: none;
            }
            ol.lst-kix_aty5ge91rj4b-0 {
                list-style-type: none;
            }
            ol.lst-kix_aao4at5asie2-7 {
                list-style-type: none;
            }
            ol.lst-kix_aty5ge91rj4b-1 {
                list-style-type: none;
            }
            ol.lst-kix_aao4at5asie2-0 {
                list-style-type: none;
            }
            .lst-kix_5hs78sc92mfd-4 > li {
                counter-increment: lst-ctn-kix_5hs78sc92mfd-4;
            }
            ol.lst-kix_aao4at5asie2-1 {
                list-style-type: none;
            }
            ol.lst-kix_aao4at5asie2-2 {
                list-style-type: none;
            }
            .lst-kix_pg8i7jvff8f1-5 > li:before {
                content: "" counter(lst-ctn-kix_pg8i7jvff8f1-5, lower-roman) ". ";
            }
            ol.lst-kix_aao4at5asie2-3 {
                list-style-type: none;
            }
            ol.lst-kix_chbwffh3ouj5-8.start {
                counter-reset: lst-ctn-kix_chbwffh3ouj5-8 0;
            }
            .lst-kix_t0enf8iir7uo-3 > li:before {
                content: "" counter(lst-ctn-kix_t0enf8iir7uo-3, decimal) ". ";
            }
            .lst-kix_pg8i7jvff8f1-3 > li:before {
                content: "" counter(lst-ctn-kix_pg8i7jvff8f1-3, decimal) ". ";
            }
            ol.lst-kix_5hs78sc92mfd-8 {
                list-style-type: none;
            }
            ol.lst-kix_5hs78sc92mfd-5.start {
                counter-reset: lst-ctn-kix_5hs78sc92mfd-5 0;
            }
            ol.lst-kix_5hs78sc92mfd-7 {
                list-style-type: none;
            }
            ol.lst-kix_5hs78sc92mfd-6 {
                list-style-type: none;
            }
            .lst-kix_pg8i7jvff8f1-1 > li:before {
                content: "" counter(lst-ctn-kix_pg8i7jvff8f1-1, lower-latin) ". ";
            }
            .lst-kix_5hs78sc92mfd-5 > li {
                counter-increment: lst-ctn-kix_5hs78sc92mfd-5;
            }
            .lst-kix_ddndwvtx2ln1-5 > li {
                counter-increment: lst-ctn-kix_ddndwvtx2ln1-5;
            }
            .lst-kix_t0enf8iir7uo-1 > li:before {
                content: "" counter(lst-ctn-kix_t0enf8iir7uo-1, lower-latin) ". ";
            }
            .lst-kix_aao4at5asie2-7 > li:before {
                content: "" counter(lst-ctn-kix_aao4at5asie2-7, lower-latin) ". ";
            }
            .lst-kix_az2l5z5h56d8-0 > li {
                counter-increment: lst-ctn-kix_az2l5z5h56d8-0;
            }
            .lst-kix_aty5ge91rj4b-6 > li:before {
                content: "" counter(lst-ctn-kix_aty5ge91rj4b-6, decimal) ". ";
            }
            ol.lst-kix_az2l5z5h56d8-6.start {
                counter-reset: lst-ctn-kix_az2l5z5h56d8-6 0;
            }
            .lst-kix_ddndwvtx2ln1-6 > li {
                counter-increment: lst-ctn-kix_ddndwvtx2ln1-6;
            }
            .lst-kix_aao4at5asie2-5 > li:before {
                content: "" counter(lst-ctn-kix_aao4at5asie2-5, lower-roman) ". ";
            }
            ol.lst-kix_aty5ge91rj4b-6.start {
                counter-reset: lst-ctn-kix_aty5ge91rj4b-6 0;
            }
            ol.lst-kix_aty5ge91rj4b-8.start {
                counter-reset: lst-ctn-kix_aty5ge91rj4b-8 0;
            }
            .lst-kix_chbwffh3ouj5-7 > li:before {
                content: "" counter(lst-ctn-kix_chbwffh3ouj5-7, lower-latin) ". ";
            }
            .lst-kix_aty5ge91rj4b-8 > li:before {
                content: "" counter(lst-ctn-kix_aty5ge91rj4b-8, lower-roman) ". ";
            }
            ol.lst-kix_pg8i7jvff8f1-7.start {
                counter-reset: lst-ctn-kix_pg8i7jvff8f1-7 0;
            }
            .lst-kix_aao4at5asie2-3 > li:before {
                content: "" counter(lst-ctn-kix_aao4at5asie2-3, decimal) ". ";
            }
            .lst-kix_aao4at5asie2-8 > li {
                counter-increment: lst-ctn-kix_aao4at5asie2-8;
            }
            .lst-kix_aao4at5asie2-0 > li {
                counter-increment: lst-ctn-kix_aao4at5asie2-0;
            }
            ol.lst-kix_5hs78sc92mfd-8.start {
                counter-reset: lst-ctn-kix_5hs78sc92mfd-8 0;
            }
            .lst-kix_aao4at5asie2-1 > li:before {
                content: "" counter(lst-ctn-kix_aao4at5asie2-1, lower-latin) ". ";
            }
            ol.lst-kix_ddndwvtx2ln1-6.start {
                counter-reset: lst-ctn-kix_ddndwvtx2ln1-6 0;
            }
            ol.lst-kix_aao4at5asie2-1.start {
                counter-reset: lst-ctn-kix_aao4at5asie2-1 0;
            }
            ol.lst-kix_5hs78sc92mfd-7.start {
                counter-reset: lst-ctn-kix_5hs78sc92mfd-7 0;
            }
            .lst-kix_aao4at5asie2-7 > li {
                counter-increment: lst-ctn-kix_aao4at5asie2-7;
            }
            ol.lst-kix_ddndwvtx2ln1-5.start {
                counter-reset: lst-ctn-kix_ddndwvtx2ln1-5 0;
            }
            ol.lst-kix_aao4at5asie2-2.start {
                counter-reset: lst-ctn-kix_aao4at5asie2-2 0;
            }
            ol.lst-kix_chbwffh3ouj5-2 {
                list-style-type: none;
            }
            .lst-kix_az2l5z5h56d8-1 > li {
                counter-increment: lst-ctn-kix_az2l5z5h56d8-1;
            }
            ol.lst-kix_chbwffh3ouj5-1 {
                list-style-type: none;
            }
            .lst-kix_5hs78sc92mfd-0 > li:before {
                content: "" counter(lst-ctn-kix_5hs78sc92mfd-0, decimal) ". ";
            }
            ol.lst-kix_chbwffh3ouj5-4 {
                list-style-type: none;
            }
            ol.lst-kix_chbwffh3ouj5-3 {
                list-style-type: none;
            }
            ol.lst-kix_t0enf8iir7uo-8.start {
                counter-reset: lst-ctn-kix_t0enf8iir7uo-8 0;
            }
            ol.lst-kix_chbwffh3ouj5-6 {
                list-style-type: none;
            }
            ol.lst-kix_chbwffh3ouj5-5 {
                list-style-type: none;
            }
            .lst-kix_5hs78sc92mfd-2 > li:before {
                content: "" counter(lst-ctn-kix_5hs78sc92mfd-2, lower-roman) ". ";
            }
            ol.lst-kix_chbwffh3ouj5-8 {
                list-style-type: none;
            }
            ol.lst-kix_chbwffh3ouj5-7 {
                list-style-type: none;
            }
            .lst-kix_chbwffh3ouj5-3 > li:before {
                content: "" counter(lst-ctn-kix_chbwffh3ouj5-3, decimal) ". ";
            }
            .lst-kix_chbwffh3ouj5-1 > li:before {
                content: "" counter(lst-ctn-kix_chbwffh3ouj5-1, lower-latin) ". ";
            }
            .lst-kix_chbwffh3ouj5-5 > li:before {
                content: "" counter(lst-ctn-kix_chbwffh3ouj5-5, lower-roman) ". ";
            }
            ol.lst-kix_chbwffh3ouj5-0 {
                list-style-type: none;
            }
            ol.lst-kix_az2l5z5h56d8-8.start {
                counter-reset: lst-ctn-kix_az2l5z5h56d8-8 0;
            }
            ol.lst-kix_pg8i7jvff8f1-4.start {
                counter-reset: lst-ctn-kix_pg8i7jvff8f1-4 0;
            }
            ol.lst-kix_ddndwvtx2ln1-4.start {
                counter-reset: lst-ctn-kix_ddndwvtx2ln1-4 0;
            }
            ol.lst-kix_aao4at5asie2-3.start {
                counter-reset: lst-ctn-kix_aao4at5asie2-3 0;
            }
            .lst-kix_aty5ge91rj4b-4 > li:before {
                content: "" counter(lst-ctn-kix_aty5ge91rj4b-4, lower-latin) ". ";
            }
            .lst-kix_aty5ge91rj4b-2 > li:before {
                content: "" counter(lst-ctn-kix_aty5ge91rj4b-2, lower-roman) ". ";
            }
            .lst-kix_chbwffh3ouj5-4 > li {
                counter-increment: lst-ctn-kix_chbwffh3ouj5-4;
            }
            ol.lst-kix_az2l5z5h56d8-5 {
                list-style-type: none;
            }
            ol.lst-kix_az2l5z5h56d8-6 {
                list-style-type: none;
            }
            .lst-kix_5hs78sc92mfd-8 > li:before {
                content: "" counter(lst-ctn-kix_5hs78sc92mfd-8, lower-roman) ". ";
            }
            .lst-kix_aty5ge91rj4b-0 > li:before {
                content: "" counter(lst-ctn-kix_aty5ge91rj4b-0, decimal) ". ";
            }
            ol.lst-kix_az2l5z5h56d8-3 {
                list-style-type: none;
            }
            ol.lst-kix_az2l5z5h56d8-4 {
                list-style-type: none;
            }
            ol.lst-kix_az2l5z5h56d8-1 {
                list-style-type: none;
            }
            ol.lst-kix_az2l5z5h56d8-2 {
                list-style-type: none;
            }
            li.li-bullet-0:before {
                margin-left: -18pt;
                white-space: nowrap;
                display: inline-block;
                min-width: 18pt;
            }
            ol.lst-kix_az2l5z5h56d8-0 {
                list-style-type: none;
            }
            .lst-kix_aty5ge91rj4b-1 > li {
                counter-increment: lst-ctn-kix_aty5ge91rj4b-1;
            }
            .lst-kix_t0enf8iir7uo-5 > li {
                counter-increment: lst-ctn-kix_t0enf8iir7uo-5;
            }
            .lst-kix_5hs78sc92mfd-4 > li:before {
                content: "" counter(lst-ctn-kix_5hs78sc92mfd-4, lower-latin) ". ";
            }
            ol.lst-kix_pg8i7jvff8f1-5.start {
                counter-reset: lst-ctn-kix_pg8i7jvff8f1-5 0;
            }
            ol.lst-kix_5hs78sc92mfd-6.start {
                counter-reset: lst-ctn-kix_5hs78sc92mfd-6 0;
            }
            .lst-kix_az2l5z5h56d8-7 > li {
                counter-increment: lst-ctn-kix_az2l5z5h56d8-7;
            }
            .lst-kix_5hs78sc92mfd-6 > li:before {
                content: "" counter(lst-ctn-kix_5hs78sc92mfd-6, decimal) ". ";
            }
            .lst-kix_aty5ge91rj4b-7 > li {
                counter-increment: lst-ctn-kix_aty5ge91rj4b-7;
            }
            .lst-kix_pg8i7jvff8f1-4 > li {
                counter-increment: lst-ctn-kix_pg8i7jvff8f1-4;
            }
            ol.lst-kix_aty5ge91rj4b-7.start {
                counter-reset: lst-ctn-kix_aty5ge91rj4b-7 0;
            }
            .lst-kix_aao4at5asie2-1 > li {
                counter-increment: lst-ctn-kix_aao4at5asie2-1;
            }
            ol {
                margin: 0;
                padding: 0;
            }
            table td,
            table th {
                padding: 0;
            }
            .c6 {
                margin-left: 54pt;
                padding-top: 0pt;
                text-indent: -18pt;
                padding-bottom: 0pt;
                line-height: 1.15;
                orphans: 2;
                widows: 2;
                text-align: justify;
            }
            .c9 {
                margin-left: 36pt;
                padding-top: 0pt;
                padding-left: 0pt;
                padding-bottom: 12pt;
                line-height: 1.15;
                orphans: 2;
                widows: 2;
                text-align: left;
            }
            .c13 {
                margin-left: 36pt;
                padding-top: 0pt;
                padding-left: 0pt;
                padding-bottom: 12pt;
                line-height: 1.15;
                orphans: 2;
                widows: 2;
                text-align: justify;
            }
            .c10 {
                margin-left: 54pt;
                padding-top: 12pt;
                text-indent: -18pt;
                padding-bottom: 12pt;
                line-height: 1.15;
                orphans: 2;
                widows: 2;
                text-align: justify;
            }
            .c16 {
                margin-left: 90pt;
                padding-top: 0pt;
                text-indent: -36pt;
                padding-bottom: 0pt;
                line-height: 1.15;
                orphans: 2;
                widows: 2;
                text-align: justify;
            }
            .c1 {
                color: #000000;
                font-weight: 400;
                text-decoration: none;
                vertical-align: baseline;
                font-size: 12pt;
                font-family: "Times New Roman";
                font-style: normal;
            }
            .c12 {
                margin-left: 36pt;
                padding-top: 0pt;
                padding-bottom: 0pt;
                line-height: 1.15;
                orphans: 2;
                widows: 2;
                text-align: left;
            }
            .c18 {
                padding-top: 0pt;
                padding-bottom: 0pt;
                line-height: 1.15;
                orphans: 2;
                widows: 2;
                text-align: left;
                height: 11pt;
            }
            .c20 {
                color: #000000;
                font-weight: 400;
                text-decoration: none;
                vertical-align: baseline;
                font-size: 11pt;
                font-family: "Arial";
                font-style: normal;
            }
            .c15 {
                margin-left: 36pt;
                padding-top: 0pt;
                padding-bottom: 0pt;
                line-height: 1.15;
                orphans: 2;
                widows: 2;
                text-align: justify;
            }
            .c8 {
                color: #000000;
                font-weight: 700;
                text-decoration: none;
                vertical-align: baseline;
                font-size: 12pt;
                font-family: "Times New Roman";
                font-style: normal;
            }
            .c2 {
                margin-left: 326pt;
                padding-top: 0pt;
                padding-bottom: 0pt;
                line-height: 1.15;
                orphans: 2;
                widows: 2;
                text-align: left;
            }
            .c3 {
                padding-top: 12pt;
                padding-bottom: 0pt;
                line-height: 1.15;
                orphans: 2;
                widows: 2;
                text-align: center;
            }
            .c14 {
                padding-top: 12pt;
                padding-bottom: 12pt;
                line-height: 1.15;
                orphans: 2;
                widows: 2;
                text-align: center;
            }
            .c17 {
                padding-top: 12pt;
                padding-bottom: 12pt;
                line-height: 1.15;
                orphans: 2;
                widows: 2;
                text-align: justify;
            }
            .c0 {
                background-color: #ffffff;
                font-size: 12pt;
                font-family: "Times New Roman";
                font-weight: 400;
            }
            .c5 {
                font-size: 7pt;
                font-family: "Times New Roman";
                font-weight: 400;
            }
            .c7 {
                font-size: 12pt;
                font-family: "Times New Roman";
                font-weight: 400;
            }
            .c19 {
                max-width: 451.4pt;
                padding: 72pt 72pt 72pt 72pt;
            }
            .c4 {
                padding: 0;
                margin: 0;
            }
            .c11 {
                background-color: #ffffff;
            }
            .c21 {
                margin-left: 18pt;
            }
            .title {
                padding-top: 0pt;
                color: #000000;
                font-size: 26pt;
                padding-bottom: 3pt;
                font-family: "Arial";
                line-height: 1.15;
                page-break-after: avoid;
                orphans: 2;
                widows: 2;
                text-align: left;
            }
            .subtitle {
                padding-top: 0pt;
                color: #666666;
                font-size: 15pt;
                padding-bottom: 16pt;
                font-family: "Arial";
                line-height: 1.15;
                page-break-after: avoid;
                orphans: 2;
                widows: 2;
                text-align: left;
            }
            li {
                color: #000000;
                font-size: 11pt;
                font-family: "Arial";
            }
            p {
                margin: 0;
                color: #000000;
                font-size: 11pt;
                font-family: "Arial";
            }
            h1 {
                padding-top: 20pt;
                color: #000000;
                font-size: 20pt;
                padding-bottom: 6pt;
                font-family: "Arial";
                line-height: 1.15;
                page-break-after: avoid;
                orphans: 2;
                widows: 2;
                text-align: left;
            }
            h2 {
                padding-top: 18pt;
                color: #000000;
                font-size: 16pt;
                padding-bottom: 6pt;
                font-family: "Arial";
                line-height: 1.15;
                page-break-after: avoid;
                orphans: 2;
                widows: 2;
                text-align: left;
            }
            h3 {
                padding-top: 16pt;
                color: #434343;
                font-size: 14pt;
                padding-bottom: 4pt;
                font-family: "Arial";
                line-height: 1.15;
                page-break-after: avoid;
                orphans: 2;
                widows: 2;
                text-align: left;
            }
            h4 {
                padding-top: 14pt;
                color: #666666;
                font-size: 12pt;
                padding-bottom: 4pt;
                font-family: "Arial";
                line-height: 1.15;
                page-break-after: avoid;
                orphans: 2;
                widows: 2;
                text-align: left;
            }
            h5 {
                padding-top: 12pt;
                color: #666666;
                font-size: 11pt;
                padding-bottom: 4pt;
                font-family: "Arial";
                line-height: 1.15;
                page-break-after: avoid;
                orphans: 2;
                widows: 2;
                text-align: left;
            }
            h6 {
                padding-top: 12pt;
                color: #666666;
                font-size: 11pt;
                padding-bottom: 4pt;
                font-family: "Arial";
                line-height: 1.15;
                page-break-after: avoid;
                font-style: italic;
                orphans: 2;
                widows: 2;
                text-align: left;
            }
        </style>
    </head>
    <body class="c11 c19">
        <p class="c2"><span class="c1">&#1055;&#1088;&#1080;&#1083;&#1086;&#1078;&#1077;&#1085;&#1080;&#1077; &#8470;1</span></p>
        <p class="c2"><span class="c1">&#1050; &#1055;&#1091;&#1073;&#1083;&#1080;&#1095;&#1085;&#1086;&#1081; &#1054;&#1092;&#1077;&#1088;&#1090;&#1077;</span></p>
        <p class="c2">
            <span class="c1">
                &#1054; &#1079;&#1072;&#1082;&#1083;&#1102;&#1095;&#1077;&#1085;&#1080;&#1080; &#1076;&#1086;&#1075;&#1086;&#1074;&#1086;&#1088;&#1072;
                &#1087;&#1088;&#1080;&#1089;&#1086;&#1077;&#1076;&#1080;&#1085;&#1077;&#1085;&#1080;&#1103; &#1082; &#1089;&#1077;&#1088;&#1074;&#1080;&#1089;&#1091; &laquo;Cash2U&raquo;
            </span>
        </p>
        <p class="c3"><span class="c8">&nbsp;</span></p>
        <p class="c3"><span class="c8">&nbsp;</span></p>
        <p class="c3"><span class="c8">&#1055;&#1056;&#1040;&#1042;&#1048;&#1051;&#1040;</span></p>
        <p class="c3">
            <span class="c8">
                &#1087;&#1088;&#1086;&#1074;&#1077;&#1076;&#1077;&#1085;&#1080;&#1103; &#1072;&#1082;&#1094;&#1080;&#1080; &laquo;&#1055;&#1088;&#1080;&#1074;&#1077;&#1076;&#1080; &#1076;&#1088;&#1091;&#1075;&#1072;&raquo;
            </span>
        </p>
        <p class="c14"><span class="c8">&nbsp;</span></p>
        <ol class="c4 lst-kix_ddndwvtx2ln1-0 start" start="1">
            <li class="c9 li-bullet-0"><span class="c8">&#1054;&#1041;&#1065;&#1048;&#1045; &#1055;&#1054;&#1051;&#1054;&#1046;&#1045;&#1053;&#1048;&#1071;</span></li>
        </ol>
        <p class="c6">
            <span class="c1">
                1.1.&#1053;&#1072;&#1089;&#1090;&#1086;&#1103;&#1097;&#1080;&#1081; &#1076;&#1086;&#1082;&#1091;&#1084;&#1077;&#1085;&#1090; &#1103;&#1074;&#1083;&#1103;&#1077;&#1090;&#1089;&#1103;
                &#1086;&#1087;&#1080;&#1089;&#1072;&#1085;&#1080;&#1077;&#1084; &#1072;&#1082;&#1094;&#1080;&#1080; &laquo;&#1055;&#1088;&#1080;&#1074;&#1077;&#1076;&#1080; &#1076;&#1088;&#1091;&#1075;&#1072;&raquo; &#1080;
                &#1088;&#1077;&#1075;&#1083;&#1072;&#1084;&#1077;&#1085;&#1090;&#1080;&#1088;&#1091;&#1077;&#1090; &#1091;&#1089;&#1083;&#1086;&#1074;&#1080;&#1103; &#1080; &#1087;&#1086;&#1088;&#1103;&#1076;&#1086;&#1082;
                &#1088;&#1077;&#1072;&#1083;&#1080;&#1079;&#1072;&#1094;&#1080;&#1080; &#1072;&#1082;&#1094;&#1080;&#1080;.
            </span>
        </p>
        <p class="c6">
            <span class="c1">
                1.2.&#1040;&#1082;&#1094;&#1080;&#1103; &#1087;&#1088;&#1086;&#1074;&#1086;&#1076;&#1080;&#1090;&#1089;&#1103; &#1074; &#1088;&#1072;&#1084;&#1082;&#1072;&#1093; &#1076;&#1086;&#1075;&#1086;&#1074;&#1086;&#1088;&#1072;
                &#1087;&#1088;&#1080;&#1089;&#1086;&#1077;&#1076;&#1080;&#1085;&#1077;&#1085;&#1080;&#1103; &#1082; &#1089;&#1077;&#1088;&#1074;&#1080;&#1089;&#1091; &laquo;Cash2U&raquo;,
                &#1103;&#1074;&#1083;&#1103;&#1077;&#1090;&#1089;&#1103; &#1085;&#1077;&#1086;&#1090;&#1098;&#1077;&#1084;&#1083;&#1077;&#1084;&#1086;&#1081; &#1095;&#1072;&#1089;&#1090;&#1100;&#1102;
                &#1055;&#1091;&#1073;&#1083;&#1080;&#1095;&#1085;&#1086;&#1081; &#1086;&#1092;&#1077;&#1088;&#1090;&#1099;, &#1080; &#1087;&#1088;&#1077;&#1076;&#1083;&#1072;&#1075;&#1072;&#1077;&#1090;&#1089;&#1103;
                &#1087;&#1086;&#1083;&#1100;&#1079;&#1086;&#1074;&#1072;&#1090;&#1077;&#1083;&#1103;&#1084; &#1089;&#1077;&#1088;&#1074;&#1080;&#1089;&#1072; &laquo;Cash2U&raquo;
                &#1086;&#1090;&#1074;&#1077;&#1095;&#1072;&#1102;&#1097;&#1080;&#1084; &#1090;&#1088;&#1077;&#1073;&#1086;&#1074;&#1072;&#1085;&#1080;&#1103;&#1084;,
                &#1091;&#1089;&#1090;&#1072;&#1085;&#1086;&#1074;&#1083;&#1077;&#1085;&#1085;&#1099;&#1084;&#1080; &#1085;&#1072;&#1089;&#1090;&#1086;&#1103;&#1097;&#1080;&#1084;&#1080;
                &#1055;&#1088;&#1072;&#1074;&#1080;&#1083;&#1072;&#1084;&#1080;.
            </span>
        </p>
        <p class="c6">
            <span class="c1">
                1.3.&#1040;&#1082;&#1090;&#1080;&#1074;&#1080;&#1079;&#1080;&#1088;&#1091;&#1102;&#1097;&#1072;&#1103; &#1072;&#1082;&#1094;&#1080;&#1103;, &#1087;&#1086;&#1076;
                &#1085;&#1072;&#1079;&#1074;&#1072;&#1085;&#1080;&#1077;&#1084; &laquo;&#1055;&#1088;&#1080;&#1074;&#1077;&#1076;&#1080; &#1076;&#1088;&#1091;&#1075;&#1072;&raquo; (&#1076;&#1072;&#1083;&#1077;&#1077;
                &#1040;&#1082;&#1094;&#1080;&#1103;) &#1087;&#1088;&#1086;&#1074;&#1086;&#1076;&#1080;&#1090;&#1089;&#1103; &#1074; &#1094;&#1077;&#1083;&#1103;&#1093; &#1091;&#1074;&#1077;&#1083;&#1080;&#1095;&#1077;&#1085;&#1080;&#1103;
                &#1073;&#1072;&#1079;&#1099; &#1085;&#1086;&#1074;&#1099;&#1093; &#1072;&#1073;&#1086;&#1085;&#1077;&#1085;&#1090;&#1086;&#1074;,
                &#1087;&#1088;&#1080;&#1089;&#1086;&#1077;&#1076;&#1080;&#1085;&#1103;&#1102;&#1097;&#1080;&#1093;&#1089;&#1103; &#1082; &#1089;&#1077;&#1088;&#1074;&#1080;&#1089;&#1091; &laquo;Cash2U&raquo;
                &#1054;&#1073;&#1097;&#1077;&#1089;&#1090;&#1074;&#1072; &#1089; &#1086;&#1075;&#1088;&#1072;&#1085;&#1080;&#1095;&#1077;&#1085;&#1085;&#1086;&#1081;
                &#1086;&#1090;&#1074;&#1077;&#1090;&#1089;&#1090;&#1074;&#1077;&#1085;&#1085;&#1086;&#1089;&#1090;&#1100;&#1102; &laquo;&#1052;&#1050;&#1050; &laquo;&#1050;&#1077;&#1083;&#1077;&#1095;&#1077;&#1082;&raquo;
                (&#1076;&#1072;&#1083;&#1077;&#1077; - &#1052;&#1050;&#1050;).
            </span>
        </p>
        <p class="c6">
            <span class="c1">
                1.4.&#1040;&#1082;&#1094;&#1080;&#1103; &#1087;&#1088;&#1086;&#1074;&#1086;&#1076;&#1080;&#1090;&#1089;&#1103; &#1085;&#1072; &#1090;&#1077;&#1088;&#1088;&#1080;&#1090;&#1086;&#1088;&#1080;&#1080;
                &#1075;&#1086;&#1088;&#1086;&#1076;&#1072; &#1041;&#1080;&#1096;&#1082;&#1077;&#1082; &#1050;&#1099;&#1088;&#1075;&#1099;&#1079;&#1089;&#1082;&#1086;&#1081;
                &#1056;&#1077;&#1089;&#1087;&#1091;&#1073;&#1083;&#1080;&#1082;&#1080;.
            </span>
        </p>
        <p class="c6">
            <span class="c1">
                1.5.&#1040;&#1082;&#1094;&#1080;&#1103; &#1087;&#1088;&#1086;&#1074;&#1086;&#1076;&#1080;&#1090;&#1089;&#1103; &#1074; &#1089;&#1086;&#1086;&#1090;&#1074;&#1077;&#1090;&#1089;&#1090;&#1074;&#1080;&#1080; &#1089;
                &#1085;&#1072;&#1089;&#1090;&#1086;&#1103;&#1097;&#1080;&#1084;&#1080; &#1087;&#1088;&#1072;&#1074;&#1080;&#1083;&#1072;&#1084;&#1080; &#1074; &#1086;&#1076;&#1080;&#1085; &#1101;&#1090;&#1072;&#1087;,
                &#1079;&#1072;&#1082;&#1083;&#1102;&#1095;&#1072;&#1102;&#1097;&#1080;&#1081;&#1089;&#1103; &#1074; &#1087;&#1086;&#1076;&#1082;&#1083;&#1102;&#1095;&#1077;&#1085;&#1080;&#1080;
                &#1084;&#1072;&#1082;&#1089;&#1080;&#1084;&#1072;&#1083;&#1100;&#1085;&#1086;&#1075;&#1086; &#1082;&#1086;&#1083;&#1080;&#1095;&#1077;&#1089;&#1090;&#1074;&#1072; &#1076;&#1088;&#1091;&#1079;&#1077;&#1081;.
                &#1044;&#1083;&#1103; &#1091;&#1095;&#1072;&#1089;&#1090;&#1080;&#1103; &#1074; &#1040;&#1082;&#1094;&#1080;&#1080;, &#1059;&#1095;&#1072;&#1089;&#1090;&#1085;&#1080;&#1082;&#1091; &#1040;&#1082;&#1094;&#1080;&#1080;
                (&#1076;&#1072;&#1083;&#1077;&#1077; &ndash;&#1059;&#1095;&#1072;&#1089;&#1090;&#1085;&#1080;&#1082;) &#1087;&#1088;&#1077;&#1076;&#1083;&#1072;&#1075;&#1072;&#1077;&#1090;&#1089;&#1103;
                &#1086;&#1089;&#1091;&#1097;&#1077;&#1089;&#1090;&#1074;&#1080;&#1090;&#1100; &#1076;&#1077;&#1081;&#1089;&#1090;&#1074;&#1080;&#1103;, &#1091;&#1082;&#1072;&#1079;&#1072;&#1085;&#1085;&#1099;&#1077; &#1074;
                &#1087;&#1091;&#1085;&#1082;&#1090;&#1077; 5.1 &#1085;&#1072;&#1089;&#1090;&#1086;&#1103;&#1097;&#1080;&#1093; &#1055;&#1088;&#1072;&#1074;&#1080;&#1083;.
            </span>
        </p>
        <p class="c10">
            <span class="c1">
                1.6.&#1042;&#1086; &#1074;&#1089;&#1077;&#1093; &#1086;&#1089;&#1090;&#1072;&#1083;&#1100;&#1085;&#1099;&#1093; &#1089;&#1083;&#1091;&#1095;&#1072;&#1103;&#1093;, &#1085;&#1077;
                &#1086;&#1075;&#1086;&#1074;&#1086;&#1088;&#1077;&#1085;&#1085;&#1099;&#1093; &#1085;&#1072;&#1089;&#1090;&#1086;&#1103;&#1097;&#1080;&#1084; &#1055;&#1088;&#1072;&#1074;&#1080;&#1083;&#1086;&#1084;,
                &#1085;&#1077;&#1086;&#1073;&#1093;&#1086;&#1076;&#1080;&#1084;&#1086; &#1088;&#1091;&#1082;&#1086;&#1074;&#1086;&#1076;&#1089;&#1090;&#1074;&#1086;&#1074;&#1072;&#1090;&#1100;&#1089;&#1103;
                &#1091;&#1090;&#1074;&#1077;&#1088;&#1078;&#1076;&#1077;&#1085;&#1085;&#1099;&#1084;&#1080;, &#1085;&#1099;&#1085;&#1077; &#1076;&#1077;&#1081;&#1089;&#1090;&#1074;&#1091;&#1102;&#1097;&#1080;&#1084;&#1080;
                &#1085;&#1086;&#1088;&#1084;&#1072;&#1090;&#1080;&#1074;&#1085;&#1099;&#1084;&#1080; &#1076;&#1086;&#1082;&#1091;&#1084;&#1077;&#1085;&#1090;&#1072;&#1084;&#1080; &#1052;&#1050;&#1050; &#1080;
                &#1085;&#1086;&#1088;&#1084;&#1072;&#1084;&#1080; &#1079;&#1072;&#1082;&#1086;&#1085;&#1086;&#1076;&#1072;&#1090;&#1077;&#1083;&#1100;&#1089;&#1090;&#1074;&#1072; &#1050;&#1056;.
            </span>
        </p>
        <p class="c17"><span class="c8">&nbsp;</span></p>
        <ol class="c4 lst-kix_aty5ge91rj4b-0 start" start="2">
            <li class="c9 li-bullet-0">
                <span class="c8">&#1057;&#1042;&#1045;&#1044;&#1045;&#1053;&#1048;&#1071; &#1054;&#1041; &#1054;&#1056;&#1043;&#1040;&#1053;&#1048;&#1047;&#1040;&#1058;&#1054;&#1056;&#1045; &#1040;&#1050;&#1062;&#1048;&#1048;</span>
            </li>
        </ol>
        <p class="c6">
            <span class="c1">
                2.1.&#1040;&#1082;&#1094;&#1080;&#1103; &#1087;&#1088;&#1086;&#1074;&#1086;&#1076;&#1080;&#1090;&#1089;&#1103; &#1054;&#1073;&#1097;&#1077;&#1089;&#1090;&#1074;&#1086;&#1084; &#1089;
                &#1086;&#1075;&#1088;&#1072;&#1085;&#1080;&#1095;&#1077;&#1085;&#1085;&#1086;&#1081; &#1086;&#1090;&#1074;&#1077;&#1090;&#1089;&#1090;&#1074;&#1077;&#1085;&#1085;&#1086;&#1089;&#1090;&#1100;&#1102;
                &laquo;&#1052;&#1050;&#1050; &laquo;&#1050;&#1077;&#1083;&#1077;&#1095;&#1077;&#1082;&raquo;.
            </span>
        </p>
        <p class="c6">
            <span class="c7">
                2.2.&#1070;&#1088;&#1080;&#1076;&#1080;&#1095;&#1077;&#1089;&#1082;&#1080;&#1081; &#1072;&#1076;&#1088;&#1077;&#1089;: &#1050;&#1099;&#1088;&#1075;&#1099;&#1079;&#1089;&#1082;&#1072;&#1103;
                &#1056;&#1077;&#1089;&#1087;&#1091;&#1073;&#1083;&#1080;&#1082;&#1072;
            </span>
            <span class="c1 c11">, &#1075;.&#1041;&#1080;&#1096;&#1082;&#1077;&#1082;, &#1091;&#1083;. &#1051;&#1100;&#1074;&#1072; &#1058;&#1086;&#1083;&#1089;&#1090;&#1086;&#1075;&#1086;, 126/8</span>
        </p>
        <p class="c6">
            <span class="c1 c11">
                2.3.&#1060;&#1072;&#1082;&#1090;&#1080;&#1095;&#1077;&#1089;&#1082;&#1080;&#1081; &#1072;&#1076;&#1088;&#1077;&#1089;: &#1050;&#1099;&#1088;&#1075;&#1099;&#1079;&#1089;&#1082;&#1072;&#1103;
                &#1056;&#1077;&#1089;&#1087;&#1091;&#1073;&#1083;&#1080;&#1082;&#1072;, &#1075;.&#1041;&#1080;&#1096;&#1082;&#1077;&#1082;, &#1091;&#1083;. &#1051;&#1100;&#1074;&#1072;
                &#1058;&#1086;&#1083;&#1089;&#1090;&#1086;&#1075;&#1086;, 126/8
            </span>
        </p>
        <p class="c6"><span class="c1">2.4.&#1048;&#1053;&#1053; &nbsp;02707202110036, &#1054;&#1050;&#1055;&#1054; &nbsp;31158290</span></p>
        <p class="c10">
            <span class="c1">
                2.5.&#1057;&#1072;&#1081;&#1090; &#1050;&#1086;&#1084;&#1087;&#1072;&#1085;&#1080;&#1080; &#1054;&#1089;&#1054;&#1054; &laquo;&#1052;&#1050;&#1050; &laquo;&#1050;&#1077;&#1083;&#1077;&#1095;&#1077;&#1082;&raquo;,
                &#1085;&#1072; &#1082;&#1086;&#1090;&#1086;&#1088;&#1086;&#1084; &#1073;&#1091;&#1076;&#1077;&#1090; &#1088;&#1072;&#1079;&#1084;&#1077;&#1097;&#1077;&#1085;&#1072;
                &#1080;&#1085;&#1092;&#1086;&#1088;&#1084;&#1072;&#1094;&#1080;&#1103; &#1086; &#1087;&#1088;&#1086;&#1074;&#1086;&#1076;&#1080;&#1084;&#1086;&#1081; &#1040;&#1082;&#1094;&#1080;&#1080; - https://cash2u.kg/
            </span>
        </p>
        <p class="c17"><span class="c1">&nbsp;</span></p>
        <ol class="c4 lst-kix_az2l5z5h56d8-0 start" start="3">
            <li class="c9 li-bullet-0"><span class="c8">&#1057;&#1056;&#1054;&#1050;&#1048; &#1055;&#1056;&#1054;&#1042;&#1045;&#1044;&#1045;&#1053;&#1048;&#1071; &#1040;&#1050;&#1062;&#1048;&#1048;</span></li>
        </ol>
        <p class="c17 c21">
            <span class="c1">
                3.1. &#1040;&#1082;&#1094;&#1080;&#1103; &#1087;&#1088;&#1086;&#1074;&#1086;&#1076;&#1080;&#1090;&#1089;&#1103; &#1074; &#1087;&#1077;&#1088;&#1080;&#1086;&#1076; &#1089; 10 &#1103;&#1085;&#1074;&#1072;&#1088;&#1103; 2022
                &#1075;&#1086;&#1076;&#1072; &#1087;&#1086; 10 &#1072;&#1087;&#1088;&#1077;&#1083;&#1103; 2022 &#1075;&#1086;&#1076;&#1072;. &#1059;&#1082;&#1072;&#1079;&#1072;&#1085;&#1085;&#1099;&#1081; &#1089;&#1088;&#1086;&#1082;
                &#1074;&#1082;&#1083;&#1102;&#1095;&#1072;&#1077;&#1090; &#1074; &#1089;&#1077;&#1073;&#1103; &#1074;&#1099;&#1087;&#1086;&#1083;&#1085;&#1077;&#1085;&#1080;&#1077; &#1076;&#1077;&#1081;&#1089;&#1090;&#1074;&#1080;&#1081;,
                &#1091;&#1082;&#1072;&#1079;&#1072;&#1085;&#1085;&#1099;&#1093; &#1074; &#1087;.5.1 &#1085;&#1072;&#1089;&#1090;&#1086;&#1103;&#1097;&#1080;&#1093; &#1055;&#1088;&#1072;&#1074;&#1080;&#1083;,
                &#1086;&#1089;&#1091;&#1097;&#1077;&#1089;&#1090;&#1074;&#1083;&#1103;&#1077;&#1084;&#1099;&#1093; &#1074; &#1091;&#1082;&#1072;&#1079;&#1072;&#1085;&#1085;&#1099;&#1081; &#1087;&#1077;&#1088;&#1080;&#1086;&#1076;
                &#1074;&#1088;&#1077;&#1084;&#1077;&#1085;&#1080;.
            </span>
        </p>
        <p class="c17"><span class="c1">&nbsp;</span></p>
        <ol class="c4 lst-kix_aao4at5asie2-0 start" start="4">
            <li class="c9 li-bullet-0"><span class="c8">&#1059;&#1063;&#1040;&#1057;&#1058;&#1053;&#1048;&#1050;&#1048; &#1040;&#1050;&#1062;&#1048;&#1048;, &#1048;&#1061; &#1055;&#1056;&#1040;&#1042;&#1040;</span></li>
        </ol>
        <p class="c6">
            <span class="c1">
                4.1. &#1051;&#1080;&#1094;&#1072;, &#1089;&#1086;&#1086;&#1090;&#1074;&#1077;&#1090;&#1089;&#1090;&#1074;&#1091;&#1102;&#1097;&#1080;&#1077; &#1085;&#1072;&#1089;&#1090;&#1086;&#1103;&#1097;&#1080;&#1084;
                &#1055;&#1088;&#1072;&#1074;&#1080;&#1083;&#1072;&#1084; &#1080; &#1074;&#1099;&#1087;&#1086;&#1083;&#1085;&#1080;&#1074;&#1096;&#1080;&#1077;
                &#1091;&#1089;&#1090;&#1072;&#1085;&#1086;&#1074;&#1083;&#1077;&#1085;&#1085;&#1099;&#1077; &#1085;&#1072;&#1089;&#1090;&#1086;&#1103;&#1097;&#1080;&#1084;&#1080;
                &#1055;&#1088;&#1072;&#1074;&#1080;&#1083;&#1072;&#1084;&#1080; &#1090;&#1088;&#1077;&#1073;&#1086;&#1074;&#1072;&#1085;&#1080;&#1103;, &#1076;&#1072;&#1083;&#1077;&#1077; &#1087;&#1086;
                &#1090;&#1077;&#1082;&#1089;&#1090;&#1091; &#1080;&#1084;&#1077;&#1085;&#1091;&#1102;&#1090;&#1089;&#1103; &#1059;&#1095;&#1072;&#1089;&#1090;&#1085;&#1080;&#1082;&#1072;&#1084;&#1080; &#1040;&#1082;&#1094;&#1080;&#1080;.
            </span>
        </p>
        <p class="c6">
            <span class="c1">
                4.2.&#1059;&#1095;&#1072;&#1089;&#1090;&#1085;&#1080;&#1082;&#1072;&#1084;&#1080; &#1040;&#1082;&#1094;&#1080;&#1080; &#1084;&#1086;&#1075;&#1091;&#1090; &#1103;&#1074;&#1083;&#1103;&#1090;&#1100;&#1089;&#1103;
                &#1076;&#1077;&#1077;&#1089;&#1087;&#1086;&#1089;&#1086;&#1073;&#1085;&#1099;&#1077; &#1083;&#1080;&#1094;&#1072;, &#1076;&#1086;&#1089;&#1090;&#1080;&#1075;&#1096;&#1080;&#1077;
                &#1074;&#1086;&#1079;&#1088;&#1072;&#1089;&#1090;&#1072; 18 &#1083;&#1077;&#1090;, &#1075;&#1088;&#1072;&#1078;&#1076;&#1072;&#1085;&#1077; &#1050;&#1099;&#1088;&#1075;&#1099;&#1079;&#1089;&#1082;&#1086;&#1081;
                &#1056;&#1077;&#1089;&#1087;&#1091;&#1073;&#1083;&#1080;&#1082;&#1080;. &#1059;&#1095;&#1072;&#1089;&#1090;&#1085;&#1080;&#1082;&#1072;&#1084;&#1080; &#1040;&#1082;&#1094;&#1080;&#1080; &#1085;&#1077;
                &#1084;&#1086;&#1075;&#1091;&#1090; &#1073;&#1099;&#1090;&#1100; &#1089;&#1086;&#1090;&#1088;&#1091;&#1076;&#1085;&#1080;&#1082;&#1080; &#1080;
                &#1087;&#1088;&#1077;&#1076;&#1089;&#1090;&#1072;&#1074;&#1080;&#1090;&#1077;&#1083;&#1080; &#1052;&#1050;&#1050;, &#1072;&#1092;&#1092;&#1080;&#1083;&#1080;&#1088;&#1086;&#1074;&#1072;&#1085;&#1085;&#1099;&#1077; &#1089;
                &#1052;&#1050;&#1050; &#1083;&#1080;&#1094;&#1072;, &#1095;&#1083;&#1077;&#1085;&#1099; &#1080;&#1093; &#1089;&#1077;&#1084;&#1077;&#1081;, &#1072; &#1090;&#1072;&#1082;&#1078;&#1077;
                &#1088;&#1072;&#1073;&#1086;&#1090;&#1085;&#1080;&#1082;&#1080; &#1076;&#1088;&#1091;&#1075;&#1080;&#1093; &#1102;&#1088;&#1080;&#1076;&#1080;&#1095;&#1077;&#1089;&#1082;&#1080;&#1093; &#1083;&#1080;&#1094;
                &#1080;/&#1080;&#1083;&#1080; &#1080;&#1085;&#1076;&#1080;&#1074;&#1080;&#1076;&#1091;&#1072;&#1083;&#1100;&#1085;&#1099;&#1093;
                &#1087;&#1088;&#1077;&#1076;&#1087;&#1088;&#1080;&#1085;&#1080;&#1084;&#1072;&#1090;&#1077;&#1083;&#1077;&#1081;, &#1087;&#1088;&#1080;&#1095;&#1072;&#1089;&#1090;&#1085;&#1099;&#1093; &#1082;
                &#1086;&#1088;&#1075;&#1072;&#1085;&#1080;&#1079;&#1072;&#1094;&#1080;&#1080; &#1080; &#1087;&#1088;&#1086;&#1074;&#1077;&#1076;&#1077;&#1085;&#1080;&#1102; &#1040;&#1082;&#1094;&#1080;&#1080; &#1080;
                &#1095;&#1083;&#1077;&#1085;&#1099; &#1080;&#1093; &#1089;&#1077;&#1084;&#1077;&#1081;.
            </span>
        </p>
        <p class="c10">
            <span class="c1">
                4.3.&#1059;&#1095;&#1072;&#1089;&#1090;&#1085;&#1080;&#1082;&#1080; &#1080;&#1084;&#1077;&#1102;&#1090; &#1087;&#1088;&#1072;&#1074;&#1072; &#1080; &#1085;&#1077;&#1089;&#1091;&#1090;
                &#1086;&#1073;&#1103;&#1079;&#1072;&#1085;&#1085;&#1086;&#1089;&#1090;&#1080;, &#1091;&#1089;&#1090;&#1072;&#1085;&#1086;&#1074;&#1083;&#1077;&#1085;&#1085;&#1099;&#1077;
                &#1076;&#1077;&#1081;&#1089;&#1090;&#1074;&#1091;&#1102;&#1097;&#1080;&#1084; &#1079;&#1072;&#1082;&#1086;&#1085;&#1086;&#1076;&#1072;&#1090;&#1077;&#1083;&#1100;&#1089;&#1090;&#1074;&#1086;&#1084;
                &#1050;&#1099;&#1088;&#1075;&#1099;&#1079;&#1089;&#1082;&#1086;&#1081; &#1056;&#1077;&#1089;&#1087;&#1091;&#1073;&#1083;&#1080;&#1082;&#1080;, &#1072; &#1090;&#1072;&#1082;&#1078;&#1077;
                &#1085;&#1072;&#1089;&#1090;&#1086;&#1103;&#1097;&#1080;&#1084;&#1080; &#1055;&#1088;&#1072;&#1074;&#1080;&#1083;&#1072;&#1084;&#1080;.
            </span>
        </p>
        <p class="c17"><span class="c1">&nbsp;</span></p>
        <ol class="c4 lst-kix_5hs78sc92mfd-0 start" start="5">
            <li class="c9 li-bullet-0"><span class="c8">&#1054;&#1041;&#1071;&#1047;&#1040;&#1053;&#1053;&#1054;&#1057;&#1058;&#1048; &#1059;&#1063;&#1040;&#1057;&#1058;&#1053;&#1048;&#1050;&#1054;&#1042;</span></li>
        </ol>
        <p class="c6">
            <span class="c1">
                5.1.&#1044;&#1083;&#1103; &#1090;&#1086;&#1075;&#1086; &#1095;&#1090;&#1086;&#1073;&#1099; &#1089;&#1090;&#1072;&#1090;&#1100; &#1059;&#1095;&#1072;&#1089;&#1090;&#1085;&#1080;&#1082;&#1086;&#1084;
                &#1040;&#1082;&#1094;&#1080;&#1080; &#1080; &#1087;&#1088;&#1077;&#1090;&#1077;&#1085;&#1076;&#1086;&#1074;&#1072;&#1090;&#1100; &#1085;&#1072; &#1087;&#1086;&#1083;&#1091;&#1095;&#1077;&#1085;&#1080;&#1077;
                &#1053;&#1072;&#1075;&#1088;&#1072;&#1076;&#1099;, &#1083;&#1080;&#1094;&#1091;, &#1089;&#1086;&#1086;&#1090;&#1074;&#1077;&#1090;&#1089;&#1090;&#1074;&#1091;&#1102;&#1097;&#1077;&#1084;&#1091;
                &#1090;&#1088;&#1077;&#1073;&#1086;&#1074;&#1072;&#1085;&#1080;&#1103;&#1084; &#1087;.4.2 &#1085;&#1072;&#1089;&#1090;&#1086;&#1103;&#1097;&#1080;&#1093; &#1087;&#1088;&#1072;&#1074;&#1080;&#1083;,
                &#1085;&#1077;&#1086;&#1073;&#1093;&#1086;&#1076;&#1080;&#1084;&#1086; &#1089;&#1086;&#1074;&#1077;&#1088;&#1096;&#1080;&#1090;&#1100; &#1089;&#1083;&#1077;&#1076;&#1091;&#1102;&#1097;&#1080;&#1077;
                &#1076;&#1077;&#1081;&#1089;&#1090;&#1074;&#1080;&#1103;:
            </span>
        </p>
        <p class="c16">
            <span class="c7">5.1.1.</span><span class="c5">&nbsp; &nbsp; &nbsp; </span>
            <span class="c1">
                &#1041;&#1099;&#1090;&#1100; &#1080;&#1083;&#1080; &#1089;&#1090;&#1072;&#1090;&#1100; &#1087;&#1086;&#1083;&#1100;&#1079;&#1086;&#1074;&#1072;&#1090;&#1077;&#1083;&#1077;&#1084;
                &#1084;&#1086;&#1073;&#1080;&#1083;&#1100;&#1085;&#1086;&#1075;&#1086; &#1087;&#1088;&#1080;&#1083;&#1086;&#1078;&#1077;&#1085;&#1080;&#1103; cash2u;
            </span>
        </p>
        <p class="c16">
            <span class="c0">5.1.2.</span><span class="c5 c11">&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
            <span class="c1 c11">
                &#1055;&#1088;&#1086;&#1081;&#1090;&#1080; &#1091;&#1076;&#1072;&#1083;&#1077;&#1085;&#1085;&#1091;&#1102; &#1080;&#1076;&#1077;&#1085;&#1090;&#1080;&#1092;&#1080;&#1082;&#1072;&#1094;&#1080;&#1102; &#1074;
                &#1084;&#1086;&#1073;&#1080;&#1083;&#1100;&#1085;&#1086;&#1084; &#1087;&#1088;&#1080;&#1083;&#1086;&#1078;&#1077;&#1085;&#1080;&#1080;;
            </span>
        </p>
        <p class="c16">
            <span class="c7">5.1.3.</span><span class="c5">&nbsp; &nbsp; &nbsp; </span><span class="c7">&#1055;&#1086;&#1083;&#1091;&#1095;&#1080;&#1090;&#1100;</span>
            <span class="c0">&nbsp;&#1088;&#1077;&#1092;&#1077;&#1088;&#1072;&#1083;&#1100;&#1085;&#1099;&#1081; &#1082;&#1086;&#1076;</span>
            <span class="c1">
                &nbsp;&#1074; &#1088;&#1072;&#1079;&#1076;&#1077;&#1083;&#1077; &laquo;&#1056;&#1077;&#1092;&#1077;&#1088;&#1072;&#1083;&#1100;&#1085;&#1072;&#1103; &#1087;&#1088;&#1086;&#1075;&#1088;&#1072;&#1084;&#1084;&#1072;&raquo;
                &#1089;&#1074;&#1086;&#1077;&#1075;&#1086; &#1084;&#1086;&#1073;&#1080;&#1083;&#1100;&#1085;&#1086;&#1075;&#1086; &#1087;&#1088;&#1080;&#1083;&#1086;&#1078;&#1077;&#1085;&#1080;&#1103;;
            </span>
        </p>
        <p class="c16">
            <span class="c0">5.1.4.</span><span class="c5 c11">&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
            <span class="c1 c11">
                &#1056;&#1077;&#1082;&#1086;&#1084;&#1077;&#1085;&#1076;&#1086;&#1074;&#1072;&#1090;&#1100; &#1080; &#1086;&#1090;&#1087;&#1088;&#1072;&#1074;&#1083;&#1103;&#1090;&#1100; &#1089;&#1074;&#1086;&#1081;
                &#1088;&#1077;&#1092;&#1077;&#1088;&#1072;&#1083;&#1100;&#1085;&#1099;&#1081; &#1082;&#1086;&#1076; &#1076;&#1083;&#1103; &#1080;&#1089;&#1087;&#1086;&#1083;&#1100;&#1079;&#1086;&#1074;&#1072;&#1085;&#1080;&#1103;
                &nbsp;&#1087;&#1088;&#1080;&#1083;&#1086;&#1078;&#1077;&#1085;&#1080;&#1103; cash2u &#1089;&#1074;&#1086;&#1080;&#1084; &#1073;&#1083;&#1080;&#1079;&#1082;&#1080;&#1084;, &#1088;&#1086;&#1076;&#1085;&#1099;&#1084;,
                &#1076;&#1088;&#1091;&#1079;&#1100;&#1103;&#1084;, &#1082;&#1086;&#1083;&#1083;&#1077;&#1075;&#1072;&#1084;.
            </span>
        </p>
        <p class="c16">
            <span class="c7">5.1.5.</span><span class="c5">&nbsp; &nbsp; &nbsp; </span>
            <span class="c1">
                &#1050;&#1072;&#1078;&#1076;&#1099;&#1081; &#1053;&#1086;&#1074;&#1099;&#1081; &#1072;&#1073;&#1086;&#1085;&#1077;&#1085;&#1090; (&#1085;&#1086;&#1074;&#1099;&#1081; &#1072;&#1073;&#1086;&#1085;&#1077;&#1085;&#1090; &ndash;
                &#1092;&#1080;&#1079;&#1080;&#1095;&#1077;&#1089;&#1082;&#1086;&#1077; &#1083;&#1080;&#1094;&#1086;, &#1082;&#1086;&#1090;&#1086;&#1088;&#1099;&#1081; &#1077;&#1097;&#1077; &#1085;&#1077;
                &#1089;&#1082;&#1072;&#1095;&#1072;&#1083; &#1084;&#1086;&#1073;&#1080;&#1083;&#1100;&#1085;&#1086;&#1077; &#1087;&#1088;&#1080;&#1083;&#1086;&#1078;&#1077;&#1085;&#1080;&#1077; &#1080; &#1085;&#1077;
                &#1087;&#1088;&#1086;&#1096;&#1077;&#1083; &#1091;&#1076;&#1072;&#1083;&#1077;&#1085;&#1085;&#1091;&#1102; &#1080;&#1076;&#1077;&#1085;&#1090;&#1080;&#1092;&#1080;&#1082;&#1072;&#1094;&#1080;&#1102;)
                &#1076;&#1086;&#1083;&#1078;&#1077;&#1085; &#1089;&#1086;&#1074;&#1077;&#1088;&#1096;&#1080;&#1090;&#1100; &#1076;&#1077;&#1081;&#1089;&#1090;&#1074;&#1080;&#1103;
                &#1091;&#1082;&#1072;&#1079;&#1072;&#1085;&#1085;&#1099;&#1093; &#1074; &#1087;. 5.1. &#1085;&#1072;&#1089;&#1090;&#1086;&#1103;&#1097;&#1080;&#1093; &#1055;&#1088;&#1072;&#1074;&#1080;&#1083; &#1080;
                &#1087;&#1088;&#1086;&#1074;&#1077;&#1089;&#1090;&#1080; &#1082;&#1072;&#1082; &#1084;&#1080;&#1085;&#1080;&#1084;&#1091;&#1084; 1 (&#1086;&#1076;&#1085;&#1091;)
                &#1090;&#1088;&#1072;&#1085;&#1079;&#1072;&#1082;&#1094;&#1080;&#1102; &#1087;&#1086; &#1087;&#1086;&#1082;&#1091;&#1087;&#1082;&#1077; &#1090;&#1086;&#1074;&#1072;&#1088;&#1072;/&#1091;&#1089;&#1083;&#1091;&#1075;&#1080;
                &#1089; &#1080;&#1089;&#1087;&#1086;&#1083;&#1100;&#1079;&#1086;&#1074;&#1072;&#1085;&#1080;&#1077;&#1084; &#1089;&#1077;&#1088;&#1074;&#1080;&#1089;&#1072; &nbsp;cash2u.
            </span>
        </p>
        <p class="c16">
            <span class="c7">5.1.6.</span><span class="c5">&nbsp; &nbsp; &nbsp; </span>
            <span class="c1">
                &#1047;&#1072; &#1087;&#1088;&#1080;&#1074;&#1083;&#1077;&#1095;&#1077;&#1085;&#1080;&#1077; &#1085;&#1086;&#1074;&#1099;&#1093; &#1091;&#1095;&#1072;&#1089;&#1090;&#1085;&#1080;&#1082;&#1086;&#1074;
                &#1040;&#1082;&#1094;&#1080;&#1080; &#1080;&#1085;&#1080;&#1094;&#1080;&#1072;&#1090;&#1086;&#1088;&#1091; &#1079;&#1072;&#1095;&#1080;&#1089;&#1083;&#1103;&#1077;&#1090;&#1089;&#1103; &#1087;&#1086; 200
                (&#1076;&#1074;&#1077;&#1089;&#1090;&#1080;) &#1089;&#1086;&#1084; &#1079;&#1072; &#1082;&#1072;&#1078;&#1076;&#1086;&#1075;&#1086; &#1087;&#1088;&#1080;&#1074;&#1083;&#1077;&#1095;&#1077;&#1085;&#1085;&#1086;&#1075;&#1086;
                &#1091;&#1095;&#1072;&#1089;&#1090;&#1085;&#1080;&#1082;&#1072; &#1074; &#1089;&#1086;&#1086;&#1090;&#1074;&#1077;&#1090;&#1089;&#1090;&#1074;&#1080;&#1080; &#1089; &#1087;.5.1.5.
                &#1085;&#1072;&#1089;&#1090;&#1086;&#1103;&#1097;&#1080;&#1093; &#1055;&#1088;&#1072;&#1074;&#1080;&#1083;.
            </span>
        </p>
        <p class="c6">
            <span class="c1">
                5.2.&#1057;&#1086;&#1074;&#1086;&#1082;&#1091;&#1087;&#1085;&#1086;&#1089;&#1090;&#1100; &#1076;&#1077;&#1081;&#1089;&#1090;&#1074;&#1080;&#1081;, &#1091;&#1082;&#1072;&#1079;&#1072;&#1085;&#1085;&#1099;&#1093; &#1074;
                &#1087;&#1091;&#1085;&#1082;&#1090;&#1077; 5.1 &#1085;&#1072;&#1089;&#1090;&#1086;&#1103;&#1097;&#1080;&#1093; &#1055;&#1088;&#1072;&#1074;&#1080;&#1083;,
                &#1087;&#1088;&#1080;&#1079;&#1085;&#1072;&#1077;&#1090;&#1089;&#1103; &#1074;&#1099;&#1087;&#1086;&#1083;&#1085;&#1077;&#1085;&#1080;&#1077;&#1084; &#1059;&#1095;&#1072;&#1089;&#1090;&#1085;&#1080;&#1082;&#1086;&#1084;
                &#1076;&#1077;&#1081;&#1089;&#1090;&#1074;&#1080;&#1081;, &#1085;&#1077;&#1086;&#1073;&#1093;&#1086;&#1076;&#1080;&#1084;&#1099;&#1093; &#1076;&#1083;&#1103; &#1091;&#1095;&#1072;&#1089;&#1090;&#1080;&#1103; &#1074;
                &#1040;&#1082;&#1094;&#1080;&#1080;, &#1072; &#1090;&#1072;&#1082;&#1078;&#1077; &#1072;&#1082;&#1094;&#1077;&#1087;&#1090;&#1086;&#1084; &#1087;&#1091;&#1073;&#1083;&#1080;&#1095;&#1085;&#1086;&#1081;
                &#1086;&#1092;&#1077;&#1088;&#1090;&#1099; &#1074; &#1074;&#1080;&#1076;&#1077; &#1086;&#1073;&#1098;&#1103;&#1074;&#1083;&#1077;&#1085;&#1080;&#1103; &#1086;&#1073; &#1040;&#1082;&#1094;&#1080;&#1080; &#1085;&#1072;
                &#1079;&#1072;&#1082;&#1083;&#1102;&#1095;&#1077;&#1085;&#1080;&#1077; &#1076;&#1086;&#1075;&#1086;&#1074;&#1086;&#1088;&#1072; &#1085;&#1072; &#1091;&#1095;&#1072;&#1089;&#1090;&#1080;&#1077; &#1074;
                &#1040;&#1082;&#1094;&#1080;&#1080; &#1087;&#1091;&#1090;&#1077;&#1084; &#1089;&#1086;&#1074;&#1077;&#1088;&#1096;&#1077;&#1085;&#1080;&#1103;
                &#1082;&#1086;&#1085;&#1082;&#1083;&#1102;&#1076;&#1077;&#1085;&#1090;&#1085;&#1099;&#1093; &#1076;&#1077;&#1081;&#1089;&#1090;&#1074;&#1080;&#1081; (&#1076;&#1077;&#1081;&#1089;&#1090;&#1074;&#1080;&#1103;,
                &#1089;&#1074;&#1080;&#1076;&#1077;&#1090;&#1077;&#1083;&#1100;&#1089;&#1090;&#1074;&#1091;&#1102;&#1097;&#1077;&#1075;&#1086; &#1086; &#1084;&#1086;&#1083;&#1095;&#1072;&#1083;&#1080;&#1074;&#1086;&#1084;
                &#1089;&#1086;&#1075;&#1083;&#1072;&#1089;&#1080;&#1080; &#1083;&#1080;&#1094;&#1072;, &#1089;&#1086;&#1074;&#1077;&#1088;&#1096;&#1072;&#1102;&#1097;&#1077;&#1075;&#1086;
                &#1076;&#1077;&#1081;&#1089;&#1090;&#1074;&#1080;&#1103;, &#1086; &#1077;&#1075;&#1086; &#1085;&#1072;&#1084;&#1077;&#1088;&#1077;&#1085;&#1080;&#1080; &#1089;&#1086;&#1074;&#1077;&#1088;&#1096;&#1080;&#1090;&#1100;
                &#1089;&#1076;&#1077;&#1083;&#1082;&#1091;, &#1079;&#1072;&#1082;&#1083;&#1102;&#1095;&#1080;&#1090;&#1100; &#1076;&#1086;&#1075;&#1086;&#1074;&#1086;&#1088;). &#1055;&#1086; &#1080;&#1090;&#1086;&#1075;&#1072;&#1084;
                &#1089;&#1086;&#1074;&#1077;&#1088;&#1096;&#1077;&#1085;&#1080;&#1103; &#1090;&#1072;&#1082;&#1080;&#1093; &#1076;&#1077;&#1081;&#1089;&#1090;&#1074;&#1080;&#1081; &#1086;&#1092;&#1077;&#1088;&#1090;&#1072;
                &#1084;&#1077;&#1078;&#1076;&#1091; &#1085;&#1080;&#1084; &#1080; &#1052;&#1050;&#1050; &#1089;&#1095;&#1080;&#1090;&#1072;&#1077;&#1090;&#1089;&#1103;
                &#1079;&#1072;&#1082;&#1083;&#1102;&#1095;&#1077;&#1085;&#1085;&#1099;&#1084;.
            </span>
        </p>
        <p class="c12"><span class="c1">&nbsp;</span></p>
        <ol class="c4 lst-kix_pg8i7jvff8f1-0 start" start="6">
            <li class="c9 li-bullet-0"><span class="c8">&#1055;&#1054;&#1056;&#1071;&#1044;&#1054;&#1050; &#1056;&#1040;&#1057;&#1063;&#1045;&#1058;&#1054;&#1042;</span></li>
        </ol>
        <p class="c6">
            <span class="c1">
                6.1.&#1053;&#1072;&#1075;&#1088;&#1072;&#1076;&#1072;, &#1091;&#1082;&#1072;&#1079;&#1072;&#1085;&#1085;&#1072;&#1103; &#1074; 6 &#1088;&#1072;&#1079;&#1076;&#1077;&#1083;&#1077;
                &#1085;&#1072;&#1089;&#1090;&#1086;&#1103;&#1097;&#1080;&#1093; &#1055;&#1088;&#1072;&#1074;&#1080;&#1083;, &#1087;&#1086;&#1083;&#1091;&#1095;&#1072;&#1102;&#1090;
                &#1059;&#1095;&#1072;&#1089;&#1090;&#1085;&#1080;&#1082;&#1080; &#1040;&#1082;&#1094;&#1080;&#1080;, &#1082;&#1086;&#1090;&#1086;&#1088;&#1099;&#1077;
                &#1089;&#1086;&#1086;&#1090;&#1074;&#1077;&#1090;&#1089;&#1090;&#1074;&#1091;&#1102;&#1090; &#1090;&#1088;&#1077;&#1073;&#1086;&#1074;&#1072;&#1085;&#1080;&#1103;&#1084; &#1087;.4, &#1080;
                &#1087;&#1088;&#1080;&#1074;&#1083;&#1077;&#1082;&#1091;&#1090; &#1076;&#1083;&#1103; &#1087;&#1086;&#1076;&#1082;&#1083;&#1102;&#1095;&#1077;&#1085;&#1080;&#1103; &#1076;&#1088;&#1091;&#1079;&#1077;&#1081;
                (&#1085;&#1086;&#1074;&#1099;&#1093; &#1072;&#1073;&#1086;&#1085;&#1077;&#1085;&#1090;&#1086;&#1074;) &#1074; &#1087;&#1077;&#1088;&#1080;&#1086;&#1076; &#1087;&#1088;&#1086;&#1074;&#1077;&#1076;&#1077;&#1085;&#1080;&#1103;
                &#1040;&#1082;&#1094;&#1080;&#1080;.
            </span>
        </p>
        <p class="c6">
            <span class="c7">6.2.&#1055;&#1086; &#1080;&#1090;&#1086;&#1075;&#1072;&#1084;</span>
            <span class="c0">
                &nbsp;&#1082;&#1072;&#1078;&#1076;&#1086;&#1075;&#1086; &#1086;&#1090;&#1095;&#1077;&#1090;&#1085;&#1086;&#1075;&#1086; &#1087;&#1077;&#1088;&#1080;&#1086;&#1076;&#1072;, &#1079;&#1072;
                &#1087;&#1088;&#1077;&#1076;&#1099;&#1076;&#1091;&#1097;&#1091;&#1102; &#1085;&#1077;&#1076;&#1077;&#1083;&#1102; (&#1087;&#1086;&#1085;&#1077;&#1076;&#1077;&#1083;&#1100;&#1085;&#1080;&#1082; &#1089; 00:00:01 &#1087;&#1086;
                &#1074;&#1086;&#1089;&#1082;&#1088;&#1077;&#1089;&#1077;&#1085;&#1100;&#1077; 23:59:59),
            </span>
            <span class="c1">
                &#1052;&#1050;&#1050; &#1087;&#1088;&#1086;&#1080;&#1079;&#1074;&#1086;&#1076;&#1080;&#1090; &#1087;&#1086;&#1076;&#1089;&#1095;&#1077;&#1090; &#1083;&#1080;&#1094;,
                &#1087;&#1088;&#1080;&#1074;&#1083;&#1077;&#1095;&#1077;&#1085;&#1085;&#1099;&#1093; &#1074; &#1088;&#1072;&#1084;&#1082;&#1072;&#1093; &#1085;&#1072;&#1089;&#1090;&#1086;&#1103;&#1097;&#1077;&#1081;
                &#1072;&#1082;&#1094;&#1080;&#1080; &#1082;&#1072;&#1078;&#1076;&#1099;&#1084; &#1091;&#1095;&#1072;&#1089;&#1090;&#1085;&#1080;&#1082;&#1086;&#1084; &#1087;&#1086; &#1076;&#1072;&#1085;&#1085;&#1099;&#1084;
                &#1089;&#1077;&#1088;&#1074;&#1080;&#1089;&#1072; &#1052;&#1050;&#1050; &#1086; &#1082;&#1086;&#1083;&#1080;&#1095;&#1077;&#1089;&#1090;&#1074;&#1077; &#1083;&#1080;&#1094;,
                &#1089;&#1086;&#1074;&#1077;&#1088;&#1096;&#1080;&#1074;&#1096;&#1080;&#1093; &#1074;&#1089;&#1077; &#1085;&#1077;&#1086;&#1073;&#1093;&#1086;&#1076;&#1080;&#1084;&#1099;&#1077;
                &#1076;&#1077;&#1081;&#1089;&#1090;&#1074;&#1080;&#1103;, &#1087;&#1088;&#1077;&#1076;&#1091;&#1089;&#1084;&#1086;&#1090;&#1088;&#1077;&#1085;&#1085;&#1099;&#1077; &#1087;. 5.1.
                &#1085;&#1072;&#1089;&#1090;&#1086;&#1103;&#1097;&#1080;&#1093; &#1055;&#1088;&#1072;&#1074;&#1080;&#1083;.
            </span>
        </p>
        <p class="c6">
            <span class="c7">
                6.3.&#1057;&#1091;&#1084;&#1084;&#1099; &#1082; &#1079;&#1072;&#1095;&#1080;&#1089;&#1083;&#1077;&#1085;&#1080;&#1102; &#1087;&#1086;&#1076;&#1083;&#1077;&#1078;&#1072;&#1090;
                &#1087;&#1077;&#1088;&#1077;&#1095;&#1080;&#1089;&#1083;&#1077;&#1085;&#1080;&#1102; &#1091;&#1095;&#1072;&#1089;&#1090;&#1085;&#1080;&#1082;&#1072;&#1084; &#1072;&#1082;&#1094;&#1080;&#1080; &#1085;&#1072;
            </span>
            <span class="c0">
                &nbsp;&#1101;&#1083;&#1077;&#1082;&#1090;&#1088;&#1086;&#1085;&#1085;&#1099;&#1077; &#1082;&#1086;&#1096;&#1077;&#1083;&#1100;&#1082;&#1080; (&#1069;&#1083;&#1089;&#1086;&#1084;, Balance, Megapay, &nbsp;&#1054;!
                &#1044;&#1077;&#1085;&#1100;&#1075;&#1080;) &#1073;&#1077;&#1079;&#1085;&#1072;&#1083;&#1080;&#1095;&#1085;&#1099;&#1084; &#1087;&#1083;&#1072;&#1090;&#1077;
            </span>
            <span class="c1">
                &#1078;&#1086;&#1084;. &#1044;&#1072;&#1085;&#1085;&#1099;&#1077; &#1085;&#1077;&#1086;&#1073;&#1093;&#1086;&#1076;&#1080;&#1084;&#1099;&#1077; &#1076;&#1083;&#1103;
                &#1087;&#1077;&#1088;&#1077;&#1095;&#1080;&#1089;&#1083;&#1077;&#1085;&#1080;&#1103; &#1076;&#1086;&#1083;&#1078;&#1085;&#1099; &#1073;&#1099;&#1090;&#1100; &#1091;&#1082;&#1072;&#1079;&#1072;&#1085;&#1099;
                &#1091;&#1095;&#1072;&#1089;&#1090;&#1085;&#1080;&#1082;&#1086;&#1084; &#1072;&#1082;&#1094;&#1080;&#1080; &#1074; &#1084;&#1086;&#1073;&#1080;&#1083;&#1100;&#1085;&#1086;&#1084;
                &#1087;&#1088;&#1080;&#1083;&#1086;&#1078;&#1077;&#1085;&#1080;&#1080; Cash2U.
            </span>
        </p>
        <p class="c6">
            <span class="c1">
                6.4.&#1052;&#1050;&#1050; &#1085;&#1077; &#1085;&#1077;&#1089;&#1077;&#1090; &#1086;&#1090;&#1074;&#1077;&#1090;&#1089;&#1090;&#1074;&#1077;&#1085;&#1085;&#1086;&#1089;&#1090;&#1080; &#1079;&#1072;
                &#1085;&#1077;&#1074;&#1086;&#1079;&#1084;&#1086;&#1078;&#1085;&#1086;&#1089;&#1090;&#1100; &#1086;&#1089;&#1091;&#1097;&#1077;&#1089;&#1090;&#1074;&#1083;&#1077;&#1085;&#1080;&#1103;
                &#1087;&#1083;&#1072;&#1090;&#1077;&#1078;&#1077;&#1081; &#1074; &#1087;&#1086;&#1083;&#1100;&#1079;&#1091; &#1091;&#1095;&#1072;&#1089;&#1090;&#1085;&#1080;&#1082;&#1086;&#1074; &#1072;&#1082;&#1094;&#1080;&#1080;, &#1074;
                &#1090;&#1086;&#1084; &#1095;&#1080;&#1089;&#1083;&#1077; &#1074; &#1089;&#1083;&#1091;&#1095;&#1072;&#1077; &#1091;&#1082;&#1072;&#1079;&#1072;&#1085;&#1080;&#1103;
                &#1091;&#1095;&#1072;&#1089;&#1090;&#1085;&#1080;&#1082;&#1086;&#1084; &#1072;&#1082;&#1094;&#1080;&#1080; &#1085;&#1077;&#1074;&#1077;&#1088;&#1085;&#1099;&#1093;
                &#1088;&#1077;&#1082;&#1074;&#1080;&#1079;&#1080;&#1090;&#1086;&#1074; &#1101;&#1083;&#1077;&#1082;&#1090;&#1088;&#1086;&#1085;&#1085;&#1086;&#1075;&#1086; &#1082;&#1086;&#1096;&#1077;&#1083;&#1100;&#1082;&#1072;, &#1072;
                &#1088;&#1072;&#1074;&#1085;&#1086; &#1079;&#1072; &#1089;&#1073;&#1086;&#1080; &#1074; &#1089;&#1080;&#1089;&#1090;&#1077;&#1084;&#1077; &#1101;&#1083;&#1077;&#1082;&#1090;&#1088;&#1086;&#1085;&#1085;&#1099;&#1093;
                &#1082;&#1086;&#1096;&#1077;&#1083;&#1100;&#1082;&#1086;&#1074;, &#1089;&#1083;&#1077;&#1076;&#1089;&#1090;&#1074;&#1080;&#1077;&#1084; &#1082;&#1086;&#1090;&#1086;&#1088;&#1099;&#1093; &#1089;&#1090;&#1072;&#1083;&#1086;
                &#1085;&#1077;&#1074;&#1086;&#1079;&#1084;&#1086;&#1078;&#1085;&#1086; &#1087;&#1077;&#1088;&#1077;&#1095;&#1080;&#1089;&#1083;&#1080;&#1090;&#1100;
                &#1087;&#1088;&#1080;&#1095;&#1080;&#1090;&#1072;&#1102;&#1097;&#1080;&#1077;&#1089;&#1103; &#1091;&#1095;&#1072;&#1089;&#1090;&#1085;&#1080;&#1082;&#1072;&#1084; &#1072;&#1082;&#1094;&#1080;&#1080;
                &#1089;&#1091;&#1084;&#1084;&#1099; &#1074;&#1086;&#1079;&#1085;&#1072;&#1075;&#1088;&#1072;&#1078;&#1076;&#1077;&#1085;&#1080;&#1103;.
            </span>
        </p>
        <p class="c17"><span class="c1">&nbsp;</span></p>
        <ol class="c4 lst-kix_chbwffh3ouj5-0 start" start="7">
            <li class="c13 li-bullet-0">
                <span class="c8">
                    &#1057;&#1055;&#1054;&#1057;&#1054;&#1041; &#1048; &#1055;&#1054;&#1056;&#1071;&#1044;&#1054;&#1050; &#1048;&#1053;&#1060;&#1054;&#1056;&#1052;&#1048;&#1056;&#1054;&#1042;&#1040;&#1053;&#1048;&#1071; &#1054;
                    &#1057;&#1056;&#1054;&#1050;&#1040;&#1061; &#1048; &#1059;&#1057;&#1051;&#1054;&#1042;&#1048;&#1071;&#1061; &#1055;&#1056;&#1054;&#1042;&#1045;&#1044;&#1045;&#1053;&#1048;&#1071; &#1040;&#1050;&#1062;&#1048;&#1048;
                </span>
            </li>
        </ol>
        <p class="c6">
            <span class="c1">
                7.1.&#1055;&#1088;&#1072;&#1074;&#1080;&#1083;&#1072; &#1040;&#1082;&#1094;&#1080;&#1080; &#1074; &#1087;&#1086;&#1083;&#1085;&#1086;&#1084; &#1086;&#1073;&#1098;&#1077;&#1084;&#1077; &#1076;&#1083;&#1103;
                &#1086;&#1090;&#1082;&#1088;&#1099;&#1090;&#1086;&#1075;&#1086; &#1076;&#1086;&#1089;&#1090;&#1091;&#1087;&#1072; &#1088;&#1072;&#1079;&#1084;&#1077;&#1097;&#1072;&#1102;&#1090;&#1089;&#1103; &#1085;&#1072;
                &#1089;&#1072;&#1081;&#1090;&#1077; https://cash2u.kg/ &#1085;&#1072; &#1089;&#1090;&#1088;&#1072;&#1085;&#1080;&#1094;&#1077; &#1089; &#1080;&#1085;&#1092;&#1086;&#1088;&#1084;&#1072;&#1094;&#1080;&#1077;&#1081;
                &#1086;&#1073; &#1040;&#1082;&#1094;&#1080;&#1080;.
            </span>
        </p>
        <p class="c6">
            <span class="c1">
                7.2.&#1042; &#1089;&#1083;&#1091;&#1095;&#1072;&#1077; &#1080;&#1079;&#1084;&#1077;&#1085;&#1077;&#1085;&#1080;&#1103; &#1087;&#1088;&#1072;&#1074;&#1080;&#1083; &#1080;&#1083;&#1080;
                &#1086;&#1090;&#1084;&#1077;&#1085;&#1099; &#1040;&#1082;&#1094;&#1080;&#1080; &#1080;&#1085;&#1092;&#1086;&#1088;&#1084;&#1072;&#1094;&#1080;&#1103; &#1086;&#1073; &#1101;&#1090;&#1086;&#1084;
                &#1073;&#1091;&#1076;&#1077;&#1090; &#1088;&#1072;&#1079;&#1084;&#1077;&#1097;&#1077;&#1085;&#1072; &#1085;&#1072; &#1089;&#1072;&#1081;&#1090;&#1077; https://cash2u.kg/ &#1089;
                &#1080;&#1085;&#1092;&#1086;&#1088;&#1084;&#1072;&#1094;&#1080;&#1077;&#1081; &#1086;&#1073; &#1040;&#1082;&#1094;&#1080;&#1080;.
            </span>
        </p>
        <p class="c6">
            <span class="c1">
                7.3.&#1052;&#1050;&#1050; &#1074;&#1087;&#1088;&#1072;&#1074;&#1077; &#1074; &#1083;&#1102;&#1073;&#1086;&#1081; &#1084;&#1086;&#1084;&#1077;&#1085;&#1090;
                &#1080;&#1079;&#1084;&#1077;&#1085;&#1080;&#1090;&#1100;/&#1086;&#1090;&#1084;&#1077;&#1085;&#1080;&#1090;&#1100;/&#1087;&#1088;&#1077;&#1082;&#1088;&#1072;&#1090;&#1080;&#1090;&#1100; &#1072;&#1082;&#1094;&#1080;&#1102;
                &#1087;&#1086; &#1089;&#1074;&#1086;&#1077;&#1084;&#1091; &#1091;&#1089;&#1084;&#1086;&#1090;&#1088;&#1077;&#1085;&#1080;&#1102;. &#1059;&#1074;&#1077;&#1076;&#1086;&#1084;&#1083;&#1077;&#1085;&#1080;&#1077; &#1086;&#1073;
                &#1080;&#1079;&#1084;&#1077;&#1085;&#1077;&#1085;&#1080;&#1080;/&#1086;&#1090;&#1084;&#1077;&#1085;&#1077;/&#1087;&#1088;&#1077;&#1082;&#1088;&#1072;&#1097;&#1077;&#1085;&#1080;&#1080; &#1072;&#1082;&#1094;&#1080;&#1080;
                &#1088;&#1072;&#1079;&#1084;&#1077;&#1097;&#1072;&#1077;&#1090;&#1089;&#1103; &#1085;&#1072; &#1089;&#1072;&#1081;&#1090;&#1077; &#1052;&#1050;&#1050; &#1080; &#1074;
                &#1084;&#1086;&#1073;&#1080;&#1083;&#1100;&#1085;&#1086;&#1084; &#1087;&#1088;&#1080;&#1083;&#1086;&#1078;&#1077;&#1085;&#1080;&#1080; &#1059;&#1095;&#1072;&#1089;&#1090;&#1085;&#1080;&#1082;&#1080;
                &#1072;&#1082;&#1094;&#1080;&#1080; &#1086;&#1073;&#1103;&#1079;&#1072;&#1085;&#1099; &#1089;&#1072;&#1084;&#1086;&#1089;&#1090;&#1086;&#1103;&#1090;&#1077;&#1083;&#1100;&#1085;&#1086;
                &#1086;&#1090;&#1089;&#1083;&#1077;&#1078;&#1080;&#1074;&#1072;&#1090;&#1100; &#1080;&#1079;&#1084;&#1077;&#1085;&#1077;&#1085;&#1080;&#1103;, &#1074;&#1085;&#1086;&#1089;&#1080;&#1084;&#1099;&#1077; &#1074;
                &#1085;&#1072;&#1089;&#1090;&#1086;&#1103;&#1097;&#1080;&#1077; &#1055;&#1088;&#1072;&#1074;&#1080;&#1083;&#1072; &#1080; &#1085;&#1077; &#1074;&#1087;&#1088;&#1072;&#1074;&#1077;
                &#1087;&#1088;&#1077;&#1076;&#1098;&#1103;&#1074;&#1083;&#1103;&#1090;&#1100; &#1082;&#1072;&#1082;&#1080;&#1077;-&#1083;&#1080;&#1073;&#1086; &#1087;&#1088;&#1077;&#1090;&#1077;&#1085;&#1079;&#1080;&#1080;,
                &#1089;&#1074;&#1103;&#1079;&#1072;&#1085;&#1085;&#1099;&#1077; &#1089; &#1080;&#1079;&#1084;&#1077;&#1085;&#1077;&#1085;&#1080;&#1077;&#1084;, &#1086;&#1090;&#1084;&#1077;&#1085;&#1086;&#1081; &#1080;&#1083;&#1080;
                &#1087;&#1088;&#1077;&#1082;&#1088;&#1072;&#1097;&#1077;&#1085;&#1080;&#1077;&#1084; &#1072;&#1082;&#1094;&#1080;&#1080;.
            </span>
        </p>
        <p class="c15"><span class="c1">&nbsp;</span></p>
        <ol class="c4 lst-kix_t0enf8iir7uo-0 start" start="8">
            <li class="c9 li-bullet-0"><span class="c8">&#1055;&#1056;&#1054;&#1063;&#1048;&#1045; &#1059;&#1057;&#1051;&#1054;&#1042;&#1048;&#1071;</span></li>
        </ol>
        <p class="c6">
            <span class="c1">
                8.1.&#1060;&#1072;&#1082;&#1090; &#1059;&#1095;&#1072;&#1089;&#1090;&#1080;&#1103; &#1074; &#1040;&#1082;&#1094;&#1080;&#1080; &#1087;&#1086;&#1076;&#1088;&#1072;&#1079;&#1091;&#1084;&#1077;&#1074;&#1072;&#1077;&#1090;
                &#1086;&#1079;&#1085;&#1072;&#1082;&#1086;&#1084;&#1083;&#1077;&#1085;&#1080;&#1077; &#1059;&#1095;&#1072;&#1089;&#1090;&#1085;&#1080;&#1082;&#1072; &#1089;
                &#1085;&#1072;&#1089;&#1090;&#1086;&#1103;&#1097;&#1080;&#1084;&#1080; &#1055;&#1088;&#1072;&#1074;&#1080;&#1083;&#1072;&#1084;&#1080; &#1080; &#1077;&#1075;&#1086; &#1089;&#1086;&#1075;&#1083;&#1072;&#1089;&#1080;&#1077;
                &#1085;&#1072; &#1091;&#1095;&#1072;&#1089;&#1090;&#1080;&#1077; &#1074; &#1040;&#1082;&#1094;&#1080;&#1080; &#1074; &#1089;&#1086;&#1086;&#1090;&#1074;&#1077;&#1090;&#1089;&#1090;&#1074;&#1080;&#1080; &#1089;
                &#1085;&#1072;&#1089;&#1090;&#1086;&#1103;&#1097;&#1080;&#1084;&#1080; &#1055;&#1088;&#1072;&#1074;&#1080;&#1083;&#1072;&#1084;&#1080;, &#1072; &#1090;&#1072;&#1082;&#1078;&#1077;
                &#1089;&#1086;&#1075;&#1083;&#1072;&#1089;&#1080;&#1077; &#1059;&#1095;&#1072;&#1089;&#1090;&#1085;&#1080;&#1082;&#1072; &#1085;&#1072;
                &#1087;&#1088;&#1077;&#1076;&#1086;&#1089;&#1090;&#1072;&#1074;&#1083;&#1077;&#1085;&#1080;&#1077; &#1087;&#1077;&#1088;&#1089;&#1086;&#1085;&#1072;&#1083;&#1100;&#1085;&#1099;&#1093;
                &#1076;&#1072;&#1085;&#1085;&#1099;&#1093; &#1076;&#1083;&#1103; &#1086;&#1073;&#1088;&#1072;&#1073;&#1086;&#1090;&#1082;&#1080; &#1074; &#1089;&#1074;&#1103;&#1079;&#1080; &#1089; &#1077;&#1075;&#1086;
                &#1059;&#1095;&#1072;&#1089;&#1090;&#1080;&#1077;&#1084; &#1074; &#1040;&#1082;&#1094;&#1080;&#1080;, &#1074;&#1082;&#1083;&#1102;&#1095;&#1072;&#1103; &#1089;&#1073;&#1086;&#1088;,
                &#1079;&#1072;&#1087;&#1080;&#1089;&#1100;, &#1089;&#1080;&#1089;&#1090;&#1077;&#1084;&#1072;&#1090;&#1080;&#1079;&#1072;&#1094;&#1080;&#1102;, &#1085;&#1072;&#1082;&#1086;&#1087;&#1083;&#1077;&#1085;&#1080;&#1077;,
                &#1093;&#1088;&#1072;&#1085;&#1077;&#1085;&#1080;&#1077;, &#1091;&#1090;&#1086;&#1095;&#1085;&#1077;&#1085;&#1080;&#1077; (&#1086;&#1073;&#1085;&#1086;&#1074;&#1083;&#1077;&#1085;&#1080;&#1077;,
                &#1080;&#1079;&#1084;&#1077;&#1085;&#1077;&#1085;&#1080;&#1077;), &#1080;&#1079;&#1074;&#1083;&#1077;&#1095;&#1077;&#1085;&#1080;&#1077;,
                &#1080;&#1089;&#1087;&#1086;&#1083;&#1100;&#1079;&#1086;&#1074;&#1072;&#1085;&#1080;&#1077; (&#1074; &#1090;&#1086;&#1084; &#1095;&#1080;&#1089;&#1083;&#1077; &#1076;&#1083;&#1103; &#1094;&#1077;&#1083;&#1077;&#1081;
                &#1074;&#1088;&#1091;&#1095;&#1077;&#1085;&#1080;&#1103; &#1053;&#1072;&#1075;&#1088;&#1072;&#1076;, &#1080;&#1085;&#1076;&#1080;&#1074;&#1080;&#1076;&#1091;&#1072;&#1083;&#1100;&#1085;&#1086;&#1075;&#1086;
                &#1086;&#1073;&#1097;&#1077;&#1085;&#1080;&#1103; &#1089; &#1059;&#1095;&#1072;&#1089;&#1090;&#1085;&#1080;&#1082;&#1072;&#1084;&#1080; &#1074; &#1094;&#1077;&#1083;&#1103;&#1093;,
                &#1089;&#1074;&#1103;&#1079;&#1072;&#1085;&#1085;&#1099;&#1093; &#1089; &#1087;&#1088;&#1086;&#1074;&#1077;&#1076;&#1077;&#1085;&#1080;&#1077;&#1084; &#1085;&#1072;&#1089;&#1090;&#1086;&#1103;&#1097;&#1077;&#1081;
                &#1040;&#1082;&#1094;&#1080;&#1080;, &#1082;&#1072;&#1082; &#1089;&#1072;&#1084;&#1080;&#1084; &#1052;&#1050;&#1050;, &#1090;&#1072;&#1082; &#1080; &#1090;&#1088;&#1077;&#1090;&#1100;&#1080;&#1084;&#1080;
                &#1083;&#1080;&#1094;&#1072;&#1084;&#1080;, &#1087;&#1088;&#1080;&#1074;&#1083;&#1077;&#1095;&#1077;&#1085;&#1085;&#1099;&#1084;&#1080; &#1052;&#1050;&#1050;), &#1087;&#1077;&#1088;&#1077;&#1076;&#1072;&#1095;&#1091;
                (&#1088;&#1072;&#1089;&#1087;&#1088;&#1086;&#1089;&#1090;&#1088;&#1072;&#1085;&#1077;&#1085;&#1080;&#1077;, &#1087;&#1088;&#1077;&#1076;&#1086;&#1089;&#1090;&#1072;&#1074;&#1083;&#1077;&#1085;&#1080;&#1077;,
                &#1076;&#1086;&#1089;&#1090;&#1091;&#1087;), &#1086;&#1073;&#1077;&#1079;&#1083;&#1080;&#1095;&#1080;&#1074;&#1072;&#1085;&#1080;&#1077;, &#1073;&#1083;&#1086;&#1082;&#1080;&#1088;&#1086;&#1074;&#1072;&#1085;&#1080;&#1077;,
                &#1091;&#1076;&#1072;&#1083;&#1077;&#1085;&#1080;&#1077;, &#1091;&#1085;&#1080;&#1095;&#1090;&#1086;&#1078;&#1077;&#1085;&#1080;&#1077; &#1087;&#1077;&#1088;&#1089;&#1086;&#1085;&#1072;&#1083;&#1100;&#1085;&#1099;&#1093;
                &#1076;&#1072;&#1085;&#1085;&#1099;&#1093; &#1074; &#1094;&#1077;&#1083;&#1103;&#1093;, &#1089;&#1074;&#1103;&#1079;&#1072;&#1085;&#1085;&#1099;&#1093; &#1089;
                &#1087;&#1088;&#1086;&#1074;&#1077;&#1076;&#1077;&#1085;&#1080;&#1077;&#1084; &#1085;&#1072;&#1089;&#1090;&#1086;&#1103;&#1097;&#1077;&#1081; &#1040;&#1082;&#1094;&#1080;&#1080;.
                &#1054;&#1073;&#1088;&#1072;&#1073;&#1086;&#1090;&#1082;&#1072; &#1087;&#1077;&#1088;&#1089;&#1086;&#1085;&#1072;&#1083;&#1100;&#1085;&#1099;&#1093; &#1076;&#1072;&#1085;&#1085;&#1099;&#1093;
                &#1086;&#1089;&#1091;&#1097;&#1077;&#1089;&#1090;&#1074;&#1083;&#1103;&#1077;&#1090;&#1089;&#1103; &#1090;&#1086;&#1083;&#1100;&#1082;&#1086; &#1074; &#1094;&#1077;&#1083;&#1103;&#1093;
                &#1080;&#1089;&#1087;&#1086;&#1083;&#1085;&#1077;&#1085;&#1080;&#1103; &#1076;&#1086;&#1075;&#1086;&#1074;&#1086;&#1088;&#1072; &#1085;&#1072; &#1091;&#1095;&#1072;&#1089;&#1090;&#1080;&#1077; &#1074;
                &#1040;&#1082;&#1094;&#1080;&#1080;, &#1086;&#1076;&#1085;&#1086;&#1081; &#1080;&#1079; &#1089;&#1090;&#1086;&#1088;&#1086;&#1085; &#1082;&#1086;&#1090;&#1086;&#1088;&#1086;&#1075;&#1086;
                &#1103;&#1074;&#1083;&#1103;&#1077;&#1090;&#1089;&#1103; &#1059;&#1095;&#1072;&#1089;&#1090;&#1085;&#1080;&#1082;. &#1055;&#1077;&#1088;&#1089;&#1086;&#1085;&#1072;&#1083;&#1100;&#1085;&#1099;&#1077;
                &#1076;&#1072;&#1085;&#1085;&#1099;&#1077; &#1085;&#1077; &#1088;&#1072;&#1089;&#1087;&#1088;&#1086;&#1089;&#1090;&#1088;&#1072;&#1085;&#1103;&#1102;&#1090;&#1089;&#1103; &#1080; &#1085;&#1077;
                &#1087;&#1088;&#1077;&#1076;&#1086;&#1089;&#1090;&#1072;&#1074;&#1083;&#1103;&#1102;&#1090;&#1089;&#1103; &#1090;&#1088;&#1077;&#1090;&#1100;&#1080;&#1084; &#1083;&#1080;&#1094;&#1072;&#1084; &#1073;&#1077;&#1079;
                &#1089;&#1086;&#1075;&#1083;&#1072;&#1089;&#1080;&#1103; &#1089;&#1091;&#1073;&#1098;&#1077;&#1082;&#1090;&#1072; &#1087;&#1077;&#1088;&#1089;&#1086;&#1085;&#1072;&#1083;&#1100;&#1085;&#1099;&#1093;
                &#1076;&#1072;&#1085;&#1085;&#1099;&#1093; &#1080; &#1080;&#1089;&#1087;&#1086;&#1083;&#1100;&#1079;&#1091;&#1102;&#1090;&#1089;&#1103; &#1086;&#1087;&#1077;&#1088;&#1072;&#1090;&#1086;&#1088;&#1086;&#1084;
                &#1080;&#1089;&#1082;&#1083;&#1102;&#1095;&#1080;&#1090;&#1077;&#1083;&#1100;&#1085;&#1086; &#1076;&#1083;&#1103; &#1080;&#1089;&#1087;&#1086;&#1083;&#1085;&#1077;&#1085;&#1080;&#1103;
                &#1091;&#1082;&#1072;&#1079;&#1072;&#1085;&#1085;&#1086;&#1075;&#1086; &#1076;&#1086;&#1075;&#1086;&#1074;&#1086;&#1088;&#1072;.
            </span>
        </p>
        <p class="c6">
            <span class="c1">
                8.2.&#1052;&#1050;&#1050; &#1085;&#1077; &#1074;&#1089;&#1090;&#1091;&#1087;&#1072;&#1077;&#1090; &#1074; &#1087;&#1080;&#1089;&#1100;&#1084;&#1077;&#1085;&#1085;&#1099;&#1077;
                &#1087;&#1077;&#1088;&#1077;&#1075;&#1086;&#1074;&#1086;&#1088;&#1099;, &#1083;&#1080;&#1073;&#1086; &#1080;&#1085;&#1099;&#1077; &#1082;&#1086;&#1085;&#1090;&#1072;&#1082;&#1090;&#1099; &#1089;
                &#1083;&#1080;&#1094;&#1072;&#1084;&#1080;, &#1091;&#1095;&#1072;&#1089;&#1090;&#1074;&#1091;&#1102;&#1097;&#1080;&#1084;&#1080; &#1074; &#1040;&#1082;&#1094;&#1080;&#1080;, &#1082;&#1088;&#1086;&#1084;&#1077;
                &#1089;&#1083;&#1091;&#1095;&#1072;&#1077;&#1074;, &#1087;&#1088;&#1077;&#1076;&#1091;&#1089;&#1084;&#1086;&#1090;&#1088;&#1077;&#1085;&#1085;&#1099;&#1093;
                &#1085;&#1072;&#1089;&#1090;&#1086;&#1103;&#1097;&#1080;&#1084;&#1080; &#1055;&#1088;&#1072;&#1074;&#1080;&#1083;&#1072;&#1084;&#1080; &#1080; &#1076;&#1077;&#1081;&#1089;&#1090;&#1074;&#1091;&#1102;&#1097;&#1080;&#1084;
                &#1079;&#1072;&#1082;&#1086;&#1085;&#1086;&#1076;&#1072;&#1090;&#1077;&#1083;&#1100;&#1089;&#1090;&#1074;&#1086;&#1084; &#1050;&#1056;.
            </span>
        </p>
        <p class="c10">
            <span class="c1">
                8.3.&#1042;&#1089;&#1077; &#1059;&#1095;&#1072;&#1089;&#1090;&#1085;&#1080;&#1082;&#1080; &#1040;&#1082;&#1094;&#1080;&#1080; &#1089;&#1072;&#1084;&#1086;&#1089;&#1090;&#1086;&#1103;&#1090;&#1077;&#1083;&#1100;&#1085;&#1086;
                &#1086;&#1087;&#1083;&#1072;&#1095;&#1080;&#1074;&#1072;&#1102;&#1090; &#1074;&#1089;&#1077; &#1088;&#1072;&#1089;&#1093;&#1086;&#1076;&#1099;, &#1087;&#1086;&#1085;&#1077;&#1089;&#1077;&#1085;&#1085;&#1099;&#1077;
                &#1080;&#1084;&#1080; &#1074; &#1089;&#1074;&#1103;&#1079;&#1080; &#1089; &#1091;&#1095;&#1072;&#1089;&#1090;&#1080;&#1077;&#1084; &#1074; &#1040;&#1082;&#1094;&#1080;&#1080;.
            </span>
        </p>
        <p class="c17"><span class="c1">&nbsp;</span></p>
        <p class="c14"><span class="c8">&nbsp;</span></p>
        <p class="c18"><span class="c20"></span></p>
    </body>
</html>


`