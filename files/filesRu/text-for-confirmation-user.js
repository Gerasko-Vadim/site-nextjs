export const ConfirmationForUser = `<html>

<head>
<meta http-equiv=Content-Type content="text/html; charset=utf-8">
<meta name=Generator content="Microsoft Word 15 (filtered)">
<style>
<!--
 /* Font Definitions */
 @font-face
	{font-family:"Cambria Math";
	panose-1:2 4 5 3 5 4 6 3 2 4;}
@font-face
	{font-family:Calibri;
        
	panose-1:2 15 5 2 2 2 4 3 2 4;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin-top:0in;
	margin-right:0in;
	margin-bottom:8.0pt;
	margin-left:0in;
	line-height:107%;
	font-size:11.0pt;
	font-family:"Calibri",sans-serif;}
p.MsoListParagraph, li.MsoListParagraph, div.MsoListParagraph
	{margin-top:0in;
	margin-right:0in;
	margin-bottom:8.0pt;
	margin-left:.5in;
	line-height:107%;
	font-size:11.0pt;
	font-family:"Calibri",sans-serif;}
p.MsoListParagraphCxSpFirst, li.MsoListParagraphCxSpFirst, div.MsoListParagraphCxSpFirst
	{margin-top:0in;
	margin-right:0in;
	margin-bottom:0in;
	margin-left:.5in;
	line-height:107%;
	font-size:11.0pt;
	font-family:"Calibri",sans-serif;}
p.MsoListParagraphCxSpMiddle, li.MsoListParagraphCxSpMiddle, div.MsoListParagraphCxSpMiddle
	{margin-top:0in;
	margin-right:0in;
	margin-bottom:0in;
	margin-left:.5in;
	line-height:107%;
	font-size:11.0pt;
	font-family:"Calibri",sans-serif;}
p.MsoListParagraphCxSpLast, li.MsoListParagraphCxSpLast, div.MsoListParagraphCxSpLast
	{margin-top:0in;
	margin-right:0in;
	margin-bottom:8.0pt;
	margin-left:.5in;
	line-height:107%;
	font-size:11.0pt;
	font-family:"Calibri",sans-serif;}
.MsoChpDefault
	{font-family:"Calibri",sans-serif;}
.MsoPapDefault
	{margin-bottom:8.0pt;
	line-height:107%;}
@page WordSection1
	{size:8.5in 11.0in;
	margin:56.7pt 42.5pt 56.7pt 85.05pt;}
div.WordSection1
	{page:WordSection1;}
 /* List Definitions */
 ol
	{margin-bottom:0in;}
ul
	{margin-bottom:0in;}
-->
</style>

</head>

<body lang=EN-US style='word-wrap:break-word'>

<div class=WordSection1>

<div style='display:flex;flex-direction: row; justify-content:flex-end'>
        <div>
            <p class=MsoNormal style='margin-top:0in;text-align:justify;line-height:normal'><span lang=RU
            style='font-size:12.0pt;font-family:"Times New Roman",serif'>Приложение №1</span></p>

            <p class=MsoNormal style='margin-top:0in;text-align:justify;line-height:normal'><span lang=RU
            style='font-size:12.0pt;font-family:"Times New Roman",serif'>К Публичному
            кредитному договору</span></p>

            <p class=MsoNormal style='margin-top:0in;text-align:justify;line-height:normal'><span lang=RU
            style='font-size:12.0pt;font-family:"Times New Roman",serif'>МКК «Келечек»</span></p>

        </div>
</div>

<p class=MsoNormal style='margin-bottom:0in;text-align:justify;line-height:
normal'><b><span lang=RU style='font-size:12.0pt;font-family:"Times New Roman",serif'>&nbsp;</span></b></p>

<p class=MsoNormal style='margin-bottom:0in;text-align:justify;line-height:
normal'><b><span lang=RU style='font-size:12.0pt;font-family:"Times New Roman",serif'>&nbsp;</span></b></p>

<p class=MsoNormal style='margin-bottom:0in;text-align:justify;line-height:
normal'><b><span lang=RU style='font-size:12.0pt;font-family:"Times New Roman",serif'>Перечень
прав, расходов (платежей) и штрафных санкций </span></b></p>

<p class=MsoNormal style='margin-bottom:0in;text-align:justify;line-height:
normal'><span lang=RU style='font-size:12.0pt;font-family:"Times New Roman",serif'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpFirst style='margin:0in;text-align:justify;
text-indent:0in;line-height:normal'><b><span lang=RU style='font-size:12.0pt;
font-family:"Times New Roman",serif'>1.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></b><b><span lang=RU style='font-size:12.0pt;font-family:"Times New Roman",serif'>Права
клиента</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin:0in;text-align:justify;
text-indent:0in;line-height:normal'><span lang=RU style='font-size:12.0pt;
font-family:"Times New Roman",serif'>1.1.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;font-family:"Times New Roman",serif'>Отказаться
на безвозмездной основе от получения кредита с момента подписания Публичного кредитного
договора до осуществления платежа в оплату за Товар по договору;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin:0in;text-align:justify;
text-indent:0in;line-height:normal'><span lang=RU style='font-size:12.0pt;
font-family:"Times New Roman",serif'>1.2.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;font-family:"Times New Roman",serif'>Иметь
доступ к Публичному кредитному договору со всеми прилагаемыми к нему
документами и обратиться за юридической консультацией за пределами МКК, без
ограничений по времени;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin:0in;text-align:justify;
text-indent:0in;line-height:normal'><span lang=RU style='font-size:12.0pt;
font-family:"Times New Roman",serif'>1.3.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;font-family:"Times New Roman",serif'>Получить
разъяснения по порядку расчетов платежей по кредиту, пени, штрафных санкций;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin:0in;text-align:justify;
text-indent:0in;line-height:normal'><span lang=RU style='font-size:12.0pt;
font-family:"Times New Roman",serif'>1.4.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;font-family:"Times New Roman",serif'>Выбрать
язык (государственный или официальный), на котором будет составлен (оформлен) кредитный
договор и настоящий перечень;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin:0in;text-align:justify;
text-indent:0in;line-height:normal'><span lang=RU style='font-size:12.0pt;
font-family:"Times New Roman",serif'>1.5.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;font-family:"Times New Roman",serif'>Досрочно
погасить задолженность по кредиту в полном объеме либо частично в любое время
без оплаты неустойки или иных видов штрафных санкций;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin:0in;text-align:justify;
line-height:normal'><span lang=RU style='font-size:12.0pt;font-family:"Times New Roman",serif'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin:0in;text-align:justify;
text-indent:0in;line-height:normal'><b><span lang=RU style='font-size:12.0pt;
font-family:"Times New Roman",serif'>2.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></b><b><span lang=RU style='font-size:12.0pt;font-family:"Times New Roman",serif'>Расходы
(платежи) по кредиту</span></b></p>

<p class=MsoListParagraphCxSpLast style='margin:0in;text-align:justify;
line-height:normal'><b><span lang=RU style='font-size:12.0pt;font-family:"Times New Roman",serif'>&nbsp;</span></b></p>

<table class=MsoTableGrid border=1 cellspacing=0 cellpadding=0
 style='border-collapse:collapse;border:none'>
 <tr>
  <td width=323 valign=top style='width:241.95pt;border:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;text-align:justify;line-height:
  normal'><span lang=RU style='font-size:12.0pt;font-family:"Times New Roman",serif'>Сумма
  кредита</span></p>
  </td>
  <td width=323 valign=top style='width:242.0pt;border:solid windowtext 1.0pt;
  border-left:none;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;text-align:justify;line-height:
  normal'><span lang=RU style='font-size:12.0pt;font-family:"Times New Roman",serif'>Указывается
  в МП</span></p>
  </td>
 </tr>
 <tr>
  <td width=323 valign=top style='width:241.95pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;text-align:justify;line-height:
  normal'><span lang=RU style='font-size:12.0pt;font-family:"Times New Roman",serif;
  color:black;background:#F9F9F9'>Процентные платежи по кредиту</span></p>
  </td>
  <td width=323 valign=top style='width:242.0pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;text-align:justify;line-height:
  normal'><span lang=RU style='font-size:12.0pt;font-family:"Times New Roman",serif'>0%</span></p>
  </td>
 </tr>
 <tr>
  <td width=323 valign=top style='width:241.95pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;line-height:normal'><span
  lang=RU style='font-size:12.0pt;font-family:"Times New Roman",serif;
  color:black;background:#F9F9F9'>Комиссия за рассмотрение заявки (оформление
  кредита)</span></p>
  </td>
  <td width=323 valign=top style='width:242.0pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;text-align:justify;line-height:
  normal'><span lang=RU style='font-size:12.0pt;font-family:"Times New Roman",serif'>Не
  предусмотрена</span></p>
  </td>
 </tr>
 <tr>
  <td width=323 valign=top style='width:241.95pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;line-height:normal'><span
  lang=RU style='font-size:12.0pt;font-family:"Times New Roman",serif;
  color:black;background:#F9F9F9'>Комиссия за выдачу и администрирование
  кредита</span></p>
  </td>
  <td width=323 valign=top style='width:242.0pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;text-align:justify;line-height:
  normal'><span lang=RU style='font-size:12.0pt;font-family:"Times New Roman",serif'>Не
  предусмотрена</span></p>
  </td>
 </tr>
 <tr>
  <td width=323 valign=top style='width:241.95pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;line-height:normal'><span
  lang=RU style='font-size:12.0pt;font-family:"Times New Roman",serif;
  color:black;background:#F9F9F9'>Комиссия за открытие и обслуживание ссудного
  и/или текущего счетов</span></p>
  </td>
  <td width=323 valign=top style='width:242.0pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;text-align:justify;line-height:
  normal'><span lang=RU style='font-size:12.0pt;font-family:"Times New Roman",serif'>Не
  предусмотрена</span></p>
  </td>
 </tr>
 <tr>
  <td width=323 valign=top style='width:241.95pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;line-height:normal'><span
  lang=RU style='font-size:12.0pt;font-family:"Times New Roman",serif;
  color:black;background:#F9F9F9'>Комиссия за расчетно-кассовое обслуживание</span></p>
  </td>
  <td width=323 valign=top style='width:242.0pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;text-align:justify;line-height:
  normal'><span lang=RU style='font-size:12.0pt;font-family:"Times New Roman",serif'>Не
  предусмотрена</span></p>
  </td>
 </tr>
 <tr>
  <td width=323 valign=top style='width:241.95pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;line-height:normal'><span
  lang=RU style='font-size:12.0pt;font-family:"Times New Roman",serif;
  color:black;background:#F9F9F9'>Штрафные санкции и пени</span></p>
  </td>
  <td width=323 valign=top style='width:242.0pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;text-align:justify;line-height:
  normal'><span lang=RU style='font-size:12.0pt;font-family:"Times New Roman",serif;
  color:black;background:#F9F9F9'>За просрочку оплаты платежей по основной
  сумме долга - пеня в размере 1 (один) % от просроченной суммы основного долга
  за каждый день просрочки. При этом размер пени, начисленной за весь период
  действия кредита не должен превышать 20 % (Двадцать процентов) от суммы
  выданного кредита.</span></p>
  </td>
 </tr>
</table>

<p class=MsoNormal style='margin-bottom:0in;text-align:justify;line-height:
normal'><span lang=RU style='font-size:12.0pt;font-family:"Times New Roman",serif'>&nbsp;</span></p>

</div>

</body>

</html>

`