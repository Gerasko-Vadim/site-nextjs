import React, { useState } from "react"
import styles from "./switchLang.module.css"
import cn from "classnames"
import { useRouter } from 'next/router'

export const SwitchLang = (): JSX.Element => {
    const router = useRouter()
    const {asPath, locale,defaultLocale}= router

    return (
        <div className={styles.btns}>
            <button
                className={cn(styles.btn,{
                    [styles.active]: locale === 'ru'
                })}
                onClick={() => {
                    router.push(asPath, asPath,{locale:'ru'})
                }}
            >RU</button>
            <button
                className={cn(styles.btn, {
                    [styles.active]: locale === 'kg'
                })}
                onClick={() => {
                    router.push(asPath, asPath, { locale: 'kg' })
                }}
            >KG</button>
        </div>
    )
}