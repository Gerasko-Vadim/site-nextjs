export interface DocItemProps {
    name: string;
    pathname: string;
}