import React from "react"
import styles from "./docItem.module.css"
import { DocItemProps } from './docItem.props'
import Link from 'next/link'
import { FileTextOutlined } from '@ant-design/icons'
import { useRouter } from 'next/router'

export const DocItem = ({ name, pathname }: DocItemProps): JSX.Element => {

    const router = useRouter();
    return (
        <div className={styles.docItem}>
            <FileTextOutlined className={styles.docIcon} />
            <Link href={`${router.locale}/doc/${pathname}`}>
                <div style={{ overflowX: "hidden", wordWrap: "break-word" }}>
                    <span className={styles.fileName}>{name}</span>
                </div>
            </Link>
        </div>

    )
}