import { DetailedHTMLProps, HtmlHTMLAttributes, ReactChild, ReactNode } from 'react';

export interface ButtonProps extends DetailedHTMLProps<HtmlHTMLAttributes<HTMLButtonElement>, HTMLButtonElement> {
    children: ReactNode;
    color?: 'brich' | 'primary' | "yellow";
    size?: 'sm' ;
}