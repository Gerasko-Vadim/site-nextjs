import React from "react"
import cn from "classnames"
import styles from "./Button.module.css"
import { ButtonProps } from './Button.props'

export const Button = ({ children, className,size, color='brich', ...props }: ButtonProps) => {
    return (
        <button
            className={cn(styles.btn, className,{
                [styles.primary]: color === 'primary',
                [styles.yellow]: color ==='yellow',
                [styles.sm]: size === 'sm'
            })}
            {...props}
        >
            {children}
        </button>
    )
}