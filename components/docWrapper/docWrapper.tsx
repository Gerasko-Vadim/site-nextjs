import React from "react"
import styles from "./docWrapper.module.css"
import { DocWrapperProps } from './docWrapper.props'

export const DocWrapper = ({ children }: DocWrapperProps): JSX.Element => {
    return (
        <div className={styles.docPage}>
            <div className={styles.content}>
                {children}
            </div>
        </div>
    )
}