import { ReactNode } from 'react';

export interface DocWrapperProps {
    children: ReactNode
}