import React from "react"
import styles from "./HTag.module.css"
import { HTagProps } from './HTag.props'
import cn from "classnames"

export const Htag = ({ size, color = "black", className, children, style }: HTagProps): JSX.Element => {
    switch (size) {
        case 'h1':
            return <h1
                className={cn(styles.h1, className, {
                    [styles.black]: color == 'black',
                    [styles.white]: color == 'white'
                })}
                style={style}
            >{children}</h1>
        case 'h2':
            return <h2
                className={cn(styles.h2, className, {
                    [styles.black]: color == 'black',
                    [styles.white]: color == 'white'
                })}
                style={style}
            >{children}</h2>
        case 'h3':
            return <h2
                className={cn(styles.h3, className, {
                    [styles.black]: color == 'black',
                    [styles.white]: color == 'white'
                })}
                style={style}
            >{children}</h2>
    }
}