import { DetailedHTMLProps, HtmlHTMLAttributes, ReactNode } from 'react';

export interface HTagProps extends DetailedHTMLProps<HtmlHTMLAttributes<HTMLElement>, HTMLElement> {
    size: 'h1' | 'h2' | 'h3';
    color?: 'black' | 'white';
    style?: object
    children: ReactNode
}