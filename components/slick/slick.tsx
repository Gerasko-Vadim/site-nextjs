import React from "react"
import Slider from "react-slick";
import { SlickProps } from './slick.props';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

 const Slick = ({ slidesToShow = 5, children }: SlickProps)=>{

    const settings = {
        dots: false,
        infinite: false,
        arrows:false,
        className: "slider variable-width",
        swipe:true,
        speed: 500,
        slidesToShow,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 1300,
                settings: {
                    slidesToShow: 3,
                }
            },
            {
                breakpoint: 800,
                settings: {
                    slidesToShow: 4,
                }
            },
            {
                breakpoint: 500,
                settings: {
                    slidesToShow: 3,
                }
            },
            {
                breakpoint: 400,
                settings: {
                    slidesToShow: 2,
                }
            },
        ]
    };
    return(
        <div>
            <Slider {...settings}>
                {children}
            </Slider>
        </div>
    )
}
export default Slick;