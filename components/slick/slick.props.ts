import { ReactNode } from 'react';


export interface SlickProps {
    slidesToShow: number;
    children: ReactNode
}