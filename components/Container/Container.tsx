import React from "react"
import styles from "./Container.module.css"
import { ContainerProps } from './Container.props'

export const Container = ({ children, ...rest }: ContainerProps): JSX.Element => {
    return (
        <div className={styles.container} {...rest}>
            {children}
        </div>
    )
}