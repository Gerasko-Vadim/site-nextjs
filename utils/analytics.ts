import ReactGA from 'react-ga'

const GA_TRACKING_ID = 'UA-212130805-1'

export const initGA = () => {
    ReactGA.initialize(GA_TRACKING_ID)
}

export const logPageView = (url) => {
    console.log(`Logging pageview for ${url}`)
    ReactGA.set({ page: url })
    ReactGA.pageview(url)

}

export const logEvent = (category = '', action = '') => {
    if (category && action) {
        ReactGA.event({ category, action })
    }
}

export const logException = (description = '', fatal = false) => {
    if (description) {
        ReactGA.exception({ description, fatal })
    }
}

export const DownloadAppTrigger = () => {
    ReactGA.event({
        category: 'Download App',
        action: 'Download App',
    })
}

export const GetAloanTrigger = () => {
    ReactGA.event({
        category: 'Get a loan',
        action: 'Get a loan',
    })
}

export const TransitionBySocialLinks = (link: string) => {
    ReactGA.event({
        category: 'Transition by social links',
        action: `Follow a social link ${link}`,
    })
}